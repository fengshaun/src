public class RectangleDemo {
    public static void main (String [] args)
    {
        Rectangle rect = new Rectangle();
        rect.setWidth(20);
        rect.setLength(15);

        System.out.println("My rectangle has area " + rect.area());
        System.out.println("Perimeter is " + rect.perimeter());
        System.out.println("Width is " + rect.width());
        System.out.println("Length is " + rect.length());
    }
}

class Rectangle {
    private double width, length;

    public void setLength(double l) { this.length = l; }
    public void setWidth(double w) { this.width = w; }
    
    public double width() { return this.width; }
    public double length() { return this.length; }

    public double area() { return width * length; }
    public double perimeter() { return 2 * (length + width); }
}
