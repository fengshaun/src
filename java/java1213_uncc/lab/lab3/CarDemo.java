public class CarDemo {
    public static void main (String [] args)
    {
        Car c = new Car();
        c.setYearModel(2006);
        c.setMake("Honda");
        c.setSpeed(10);

        for (int i = 0; i < 5; i++) {
            accCar(c);
        }

        for (int i = 0; i < 5; i++) {
            brakeCar(c);
        }

        System.out.println("Year model: " + c.getYearModel());
        System.out.println("Make: " + c.getMake());
    }

    private static void accCar(Car c) {
        c.accelerate();
        reportSpeed(c);
    }

    private static void brakeCar(Car c) {
        c.brake();
        reportSpeed(c);
    }

    private static void reportSpeed(Car c) {
        System.out.println("Car's current speed: " + c.getSpeed());
    }
}
