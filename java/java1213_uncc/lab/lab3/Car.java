public class Car {
    private int yearModel;
    private String make;
    private int speed;

    public void setYearModel(int y) { this.yearModel = y; }
    public void setMake(String m) { this.make = m; }
    public void setSpeed(int s) { this.speed = s; }

    public int getYearModel() { return this.yearModel; }
    public String getMake() { return this.make; }
    public int getSpeed() { return this.speed; }

    public void accelerate() { this.speed += 5; }
    public void brake() { this.speed -= 5; }
}
