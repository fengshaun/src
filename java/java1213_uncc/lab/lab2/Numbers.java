// Yo dawg!  I thought you liked numbers, so put a number in your number so you can add while you add.
// The above statement is more appropriate for recursive functions, but oh well!
// Armin Moradi ITCS 1213

public class Numbers {
    public static void main (String [] args)
    {
        System.out.println(8.0 + 3); // 11, 11.0, 11.0
        System.out.println(8.0 - 3); // 5, 5.0, 5.0
        System.out.println(8.0 * 3); // 24, 24.0, 24.0
        System.out.println(8.0 / 3); // 2, 2.666666, 2.666666

        System.out.println(5 / 2); // 2
        System.out.println(5.0 / 2); // 2.5
        System.out.println(5 / 2.5); // 2.0
        System.out.println(0.5 / 2.5); // 0.2
    }
}
