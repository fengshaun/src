// Super cool java TV simulator!
// Armin Moradi ITCS 1213

class Television {
    public static void main (String [] args) // 'main' method not found: Exception
    {
        System.out.print("What is your favourite\n"); // 'system' not found
        System.out.print("television program?\n"); // ';' expected

        // using print: the output is all on one line!
        // using \n: same output as using println
        
        /* output:
         * 1:CSI,
         * American Idol, Survivor?
         */
        System.out.println("1:CSI,\nAmerican Idol, Survivor?");

        /* output:
         * 2:CSI,
         * American Idol,
         * Survivor?
         */
        System.out.println("2:CSI,\nAmerican Idol,\nSurvivor?");

        /* output:
         * 3:CSI,
         *
         * American Idol,
         *
         * Survivor?
         */
        System.out.println("3:CSI,\n\nAmerican Idol,\n\nSurvivor?");

        // \n is the newline character

        /* Exercise 3 */
        System.out.println("Oh, I love to \"program\" in Java");
        // backslash: making things special since the dawn of time!
        // \": literal " character
        // \t: tab character

        /* Exercise 4 */
        System.out.println("1:Send money quick!"); // To Mom!
        System.out.println("2:Send money quick!// To Mom!");
        // To Mom! System.out.println("3:Send money quick!");
        /* I'm going to comment out the following so that this file can compile:
         * System.out.println("4:Send money quick!" // To Mom!);
         */

        /* comments should not be placed inside strings, and inside functions either! */
    }
} // EOF while parsing
