// Mileage: Y U APPEAR IN *EVERY* CS CLASS ASSIGNMENT?
// Armin Moradi ITCS 1213

import java.util.Scanner;

public class Mileage {
    public static void main (String [] args)
    {
        System.out.println("This program is very awesome and calculates mileage.\nYour mileage, however, may vary.\n\n");
        Scanner kbd = new Scanner(System.in);

        System.out.print("miles driven? ");
        double miles = kbd.nextDouble();

        System.out.print("gallons used? ");
        double gallons = kbd.nextDouble();

        if (gallons == 0) {
            System.out.println("You divided by zero and a black hole was created.  Run for your life.\n" +
                               "Your car also has super-powers!");
        }

        double mpg = miles / gallons;
        System.out.println("Miles per gallon: " + mpg);
    }
}
