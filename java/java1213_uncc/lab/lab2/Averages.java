// Averages: Y U NO ACCURATE?
// Armin Moradi ITCS 1213

public  class Averages {
    public static void main (String [] args)
    {
        int x = 5, y = 8, z = 4;
        double result = (x + y) / 3.0; // 2.0: making averages accurate since the beginning.
        System.out.println("Average of " + x + " and " + y + " and " + z + " is " + result); // result = 6 at the beginning
    }
}
