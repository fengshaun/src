// They see me callin', they hatin'
// Armin Moradi ITCS 1213

import java.util.Scanner;

public class Calling {
    public static void main (String [] args)
    {
        Scanner kbd = new Scanner(System.in);

        // declarations.
        // This coding practice is *highly* frowned upon IRL.  I even saw it on TheDailyWTF a couple of times
        String name, address, city, state;
        int zip;

        System.out.print("name? ");
        name = kbd.nextLine();

        System.out.print("address? ");
        address = kbd.nextLine();

        System.out.print("city? ");
        city = kbd.nextLine();

        System.out.print("state? ");
        state = kbd.nextLine();

        System.out.print("zip? ");
        zip = kbd.nextInt();

        System.out.println(name + "\n" + address + "\n" + city + ", " + state + " " + zip);

        char firstInitial = name.toUpperCase().charAt(0);
        System.out.println("firstInitial: " + firstInitial);

        name = name.toUpperCase();
        System.out.println("name: " + name + "\n" + "length: " + name.length());
    }
}
