/* Armin Moradi ITCS1213
 * Numbers driver class
 */

import java.util.Scanner;

public class Guess {
    public static void main (String[] args) {
        Scanner kbd = new Scanner(System.in);
        Numbers nums = new Numbers();

        System.out.print("Enter guess (-1 to exit): ");
        int guess;
        while ((guess = kbd.nextInt()) != -1) {
            if (nums.checkNumbers(guess)) { System.out.println("Congratulations!  You guessed the number!"); break; }
            else { System.out.println("Incorrect.  Guess again."); }

            System.out.print("Enter guess (-1 to exit): ");
        }
    
        System.exit(0);
    }
}
