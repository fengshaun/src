/* Armin Moradi ITCS1213
 * Numbers utility class
 */

public class Numbers {
    private int[] nums = new int[6];

    public Numbers() { nums[0] = 4; nums[1] = 8; nums[2] = 15; nums[3] = 16; nums[4] = 23; nums[5] = 42; }

    public boolean checkNumbers(int guess) {
        for (int i : nums) if (guess == i) return true;
        return false;
    }
}
