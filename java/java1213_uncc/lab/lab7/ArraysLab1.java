/* Armin Moradi ITCS1213
 * Arrays lab exercise #1
 */

import java.util.Scanner;
public class ArraysLab1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int[] numbers = new int[4];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print("Enter an integer: ");
             numbers[i] = keyboard.nextInt();
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.println("numbers[" + i + "] = " + numbers[i]);
        }

        System.out.println("The sum of the numbers is " + sumOfArrays(numbers));
        System.out.println("The average of the numbers is " + averageOfArray(numbers));
      	System.out.println("The minimum of the numbers is " + minOfArray(numbers));
        System.out.println("The maximum of the numbers is " + maxOfArray(numbers));
    }

    public static int sumOfArrays(int[] numbers) {
        int sum = 0;
        for (int elem : numbers) { sum += elem; }
        return sum;
    }

    public static double averageOfArray(int[] numbers) {
        return (double)sumOfArrays(numbers) / numbers.length;
    }

    public static int minOfArray(int[] numbers) {
        int min = numbers[0];
        for (int elem : numbers) min = (elem < min) ? elem : min;
        return min;
    }

    public static int maxOfArray(int[] numbers) {
        int max = numbers[0];
        for (int elem : numbers) max = (elem > max) ? elem : max;
        return max;
    }
}
