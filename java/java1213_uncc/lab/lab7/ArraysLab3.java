/* Armin Moradi ITCS1213
 * Arrays lab exercise #3
 */

public class ArraysLab3 {
     public static void main(String[] args) {
        /* in main: j, arrayA[0] are unaffected. arrayB[0] is affected */
        int[] arrayA = {2,3,4};
        int[] arrayB = {20, 30, 40};
        int j = 5;
        Data3 tester = new Data3();
        System.out.println( "Before tester.fun(numbers):" + "\n j = "
                            + j + "\n arrayA[0] = " +   arrayA[0] + "\n arrayB[0] = " + arrayB[0]);
        tester.fun(j, arrayA[0], arrayB);
        System.out.println( "After tester.fun(numbers):" + "\n j = "
		            	 	+ j + "\n arrayA[0] = " +        arrayA[0] + "\n arrayB[0] = "
                            + arrayB[0]);
     }
}

class Data3 {
     public void fun(int x, int y, int[] a) {
          x = 100;
          y = 200;
          a[0] = 300;
     }
}
