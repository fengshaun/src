/* Armin Moradi ITCS1213
 * Arrays lab exercise #2
 */

public class ArraysLab2
{
     public static void main(String[] args)
     {
          double[] fatGrams = {12.6, 32.0, 2.0, 11.2, 0.5, 3.99};
          for(int j = fatGrams.length - 1; j >= 0; j--) { System.out.println(fatGrams[j]); }
     }
}
