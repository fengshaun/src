/* Armin Moradi ITCS1213L
 * Lisenced under GPLv3
 */

import javax.swing.JOptionPane;

public class WordGame {
    public static void main (String[] args)
    {
        String name = JOptionPane.showInputDialog("Yer name be?");
        String age = JOptionPane.showInputDialog("Yer age be?");
        String city = JOptionPane.showInputDialog("Yer city of living be?");
        String college = JOptionPane.showInputDialog("Yer university be?");
        String profession = JOptionPane.showInputDialog("Yer profession be?");
        String animal = JOptionPane.showInputDialog("Yer type de animal be?");
        String petName = JOptionPane.showInputDialog("Yer pet name be?");

        JOptionPane.showMessageDialog(null, "There once was a person named " + name + " who lived in " + city + ".  At the age of " + age + ",\n" +
                                            name + " went to college at " + college + ".  " + name + " graduated and went to work as a\n" + 
                                            profession + ".  Then " + name + " adopted a(n) " + animal + " name " + petName + ".  They both lived\n" +
                                            "happily ever after.");
    
        System.exit(0);
    }
}
