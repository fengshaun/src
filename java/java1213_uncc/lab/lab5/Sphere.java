/* Armin Moradi ITCS1213L */

import javax.swing.JOptionPane;

public class Sphere {
    public static void main (String[] args)
    {
        String strDiameter = JOptionPane.showInputDialog("Diameter?");
        double numDiameter = Double.parseDouble(strDiameter);

        double radius = numDiameter / 2;
        double volume = (4.0/3) * Math.PI * Math.pow(radius, 3.0);

        JOptionPane.showMessageDialog(null, "volume: " + volume);
    
        System.exit(0);
    }

}
