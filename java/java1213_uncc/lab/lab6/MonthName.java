/* Armin Moradi ITCS1213
 * Month driver class using name as input
 */

import java.util.Scanner;
import javax.swing.JOptionPane;

public class MonthName {
    public static void main (String[] args)  {
        Month first =  new Month(JOptionPane.showInputDialog("Enter the first month name:"));
        Month second = new Month(JOptionPane.showInputDialog("Enter the second month name:"));
        JOptionPane.showMessageDialog(null, "first: " + first + "\nsecond: " + second + "\nThey are " + (first.equals(second) ? "equal" : "not equal"));

        System.exit(0);
    }
}
