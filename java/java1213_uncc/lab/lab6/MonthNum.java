/* Armin Moradi ITCS1213
 * Month driver class using int as input
 */

import java.util.Scanner;
import javax.swing.JOptionPane;

public class MonthNum {
    public static void main (String[] args) {
        Month first =  new Month(Integer.parseInt(JOptionPane.showInputDialog("Enter the first month number:")));
        Month second = new Month(Integer.parseInt(JOptionPane.showInputDialog("Enter the second month number:")));
        JOptionPane.showMessageDialog(null, "first: " + first + "\nsecond: " + second + "\nThey are " + (first.equals(second) ? "equal" : "not equal"));

        System.exit(0);
    }
}
