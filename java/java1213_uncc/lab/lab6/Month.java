/* Armin Moradi ITCS1213
 * Month class
 * represents a month and makes handling months easier
 */

import org.apache.commons.collections.bidimap.DualHashBidiMap;

public class Month {
    private int monthNumber;
    private DualHashBidiMap monthMap;

    private void initializeMap() {
        monthMap = new DualHashBidiMap();
        monthMap.put(new Integer(1),"january");
        monthMap.put(new Integer(2), "february");
        monthMap.put(new Integer(3), "march");
        monthMap.put(new Integer(4), "april");
        monthMap.put(new Integer(5), "may");
        monthMap.put(new Integer(6), "june");
        monthMap.put(new Integer(7), "july");
        monthMap.put(new Integer(8), "august");
        monthMap.put(new Integer(9), "september");
        monthMap.put(new Integer(10),"october");
        monthMap.put(new Integer(11),"november");
        monthMap.put(new Integer(12),"december");
    }

    public Month() { initializeMap(); monthNumber = 1; }
    public Month(int m) { initializeMap(); setMonthNumber(m); }
    public Month(String m) { initializeMap(); setMonthNumber(m); }

    // the lab said case sensitive, but I'll go with case insensitive since it's
    // more robust
    public void setMonthNumber(String m) { monthNumber = monthMap.containsValue(m.toLowerCase()) ? (Integer)monthMap.getKey(m) : 1; }
    public void setMonthNumber(int m) { monthNumber = (m > 12 || m < 1) ? 1 : m; }

    public int getMonthNumber() { return monthNumber; }
    public String getMonthName() { return (String)monthMap.get(monthNumber); } // safely assume monthNumber is valid
    public String toString() { return getMonthNumber() + " - " + getMonthName(); }
    public boolean equals(Month m2) { return this.getMonthNumber() == m2.getMonthNumber(); }
}
