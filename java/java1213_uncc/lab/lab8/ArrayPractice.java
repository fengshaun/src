/* Armin Moradi ITCS1213
 * Practice with arrays
 */

import java.util.Scanner;

public class ArrayPractice {
    public static void main (String[] args) {
        int[][] table = new int[3][4];
        Scanner kbd = new Scanner(System.in);

        int sum = 0;
        String t = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                // doing everything in one pass: efficiency.
                System.out.print("Enter number: ");
                table[i][j] = kbd.nextInt();
                sum += table[i][j];
                t += table[i][j] + " ";
            }
            t += "\n";
        }

        System.out.println("Total of third column of the first row and first column of the second row: " + (table[0][2] + table[1][0]));
        System.out.println("Total of table: " + sum);
        System.out.print("table:\n" + t);

        System.exit(0);
    }
}
