// First Java program in CS1213 UNCC lab.  Someday this will be good old times!

public class Hello {
    public static void main (String[] args) {
        System.out.println("Hello world");
        System.out.println("Armin Moradi");
        System.out.println("\nI love java");

        System.out.println("I have now written my first Java program named Hello World" +
                           " so I am a Java programmer and can continue to write more programs " +
                           "during this semester while in my ITCS1213 class.");
    }
}
