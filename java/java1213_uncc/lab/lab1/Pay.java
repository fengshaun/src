// Yet another simple program!

import java.util.Scanner;

public class Pay {
    public static void main(String[] args) { // MethodDoesNotExist Exception for 'main'
        System.out.print("How many hours did you work? "); // 'system' class does not exist

        Scanner kbd = new Scanner(System.in); // ';' expected
        double hours = kbd.nextDouble();

        System.out.print("How much are you paid per hour? ");
        double rate = kbd.nextDouble();

        double salary;
        if (hours <= 40) {
            salary = hours * rate;
        } else {
            salary = (hours - 40) * (1.5 * rate) + 40 * rate;
        }

        System.out.println("You earned $" + salary);
    }
} // reached EOF while parsing
