// Prints a diamond
// and that's it :)
// Probably the most typical "first-chapter" excercise in every possible CS text book!

public class Diamond {
    public static void main(String[] args) {
        System.out.println("   *   \n" +
                           "  ***  \n" +
                           " ***** \n" +
                           "*******\n" +
                           " ***** \n" +
                           "  ***  \n" +
                           "   *   ");
    }
}
