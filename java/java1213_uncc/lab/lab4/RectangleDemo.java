public class RectangleDemo {
    public static void main (String [] args)
    {
        Rectangle rect = new Rectangle();
        rect.setWidth(20);
        rect.setLength(15);

        System.out.println("My rectangle has area " + rect.area());
        System.out.println("Perimeter is " + rect.perimeter());
        System.out.println("Width is " + rect.width());
        System.out.println("Length is " + rect.length());

        System.out.println("------------------------");
        
        Rectangle rect2 = new Rectangle();
        System.out.println("Second Rect:");
        System.out.println("perimeter is " + rect2.perimeter());
        System.out.println("area is " + rect2.area());

        System.out.println("------------------------");

        Rectangle rect3 = new Rectangle(12);
        System.out.println("Square:");
        System.out.println("perimeter is " + rect3.perimeter());
        System.out.println("area is " + rect3.area());

        System.out.println("------------------------");

        Rectangle rect4 = new Rectangle(10, 20);
        System.out.println("Rectangle 10 20:");
        System.out.println("perimeter is " + rect4.perimeter());
        System.out.println("area is " + rect4.area());
    }
}

class Rectangle {
    private double width, length;

    public Rectangle() {
        setWidth(1);
        setLength(1);
    }

    public Rectangle(double s) {
        // creates a square
        setWidth(s);
        setLength(s);
    }

    public Rectangle(double l, double w) {
        setWidth(w);
        setLength(l);
    }

    public void setLength(double l) { this.length = l; }
    public void setWidth(double w) { this.width = w; }
    
    public double width() { return this.width; }
    public double length() { return this.length; }

    public double area() { return width * length; }
    public double perimeter() { return 2 * (length + width); }
}
