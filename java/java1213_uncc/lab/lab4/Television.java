/* Armin Moradi (c) 2011
 * ITCS 1213L
 *
 * Licensed under GPLv3
 *
 * The purpose of this class is to model a television
 */

public class Television {
    // name of the manufacturer
    private String manufacturer = "";

    // the size of the screen
    private int screenSize = 0;

    // whether it's powered on
    private Boolean powerOn = false;

    // channel number it's currently on
    private int channel = 0;

    // the volume of the TV
    private int volume = 0;

    // this is the constructor
    // it initializes some of the attributes
    public Television(String m, int s) {
        this.manufacturer = m;
        this.screenSize = s;

        this.powerOn = false;
        this.channel = 2;
        this.volume = 20;
    }

    // this method returns the manufacturer name
    public String getManufacturer() { return this.manufacturer; }
    // this method returns the screen size
    public int getScreenSize() { return this.screenSize; }

    // this method sets the channel
    public void setChannel(int c) { this.channel = c; }
    // this method return the channel number
    public int getChannel() { return this.channel; }

    // this method toggles the power on television
    public void power() { this.powerOn = !this.powerOn; }

    // this method increases the volume by 1
    public void increaseVolume() { volume = volume >= 25 ? volume : volume + 1; }
    // this method decreases the volume by 1
    public void decreaseVolume() { volume = volume <= 0  ? volume = 0 : volume - 1; }
    // this method returns the volume of the television
    public int getVolume() { return this.volume; }
}

