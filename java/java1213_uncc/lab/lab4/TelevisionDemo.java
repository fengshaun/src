/* Armin Moradi (c) 2011
 * ITCS 1213L
 *
 * Licensed under GPLv3
 *
 * The driver class for Television.java
 */

import java.util.Scanner;

public class TelevisionDemo {
    public static void main (String [] args)
    {
        Scanner kbd = new Scanner(System.in);
        System.out.print("brand? ");
        String brand = kbd.nextLine();
        System.out.print("screen size? ");
        int size = kbd.nextInt();

        Television tv = new Television(brand, size);
        tv.power();
        showState(tv);

        System.out.println("--------------------------");
        System.out.print("what channel would you like? ");
        tv.setChannel(kbd.nextInt());
        showState(tv);

        System.out.println("--------------------------");
        System.out.println("increasing volume by 6...");
        for (int i = 0; i < 6; i++) { tv.increaseVolume(); }
        showState(tv);

        System.out.println("--------------------------");
        System.out.println("volume is too high :(");
        for (int i = 0; i < 2; i++) { tv.decreaseVolume(); }
        showState(tv);
    }

    public static void showState(Television tv) {
        System.out.println("channel: " + tv.getChannel());
        System.out.println("volume: " + tv.getVolume());
    }
}
