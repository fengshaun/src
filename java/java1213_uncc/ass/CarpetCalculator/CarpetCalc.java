/* Armin Moradi ITCS1213
 * calculates the cost of a given carpet
 */

import java.util.Scanner;

public class CarpetCalc {
    public static void main (String[] args) {
        Scanner kbd = new Scanner(System.in);

        System.out.print("Please enter the length of the room: ");
        double l = kbd.nextDouble();

        System.out.print("Please enter the width of the room: ");
        double w = kbd.nextDouble();

        System.out.print("Please enter the price per square meter of the carpet: ");
        double p = kbd.nextDouble();

        RoomCarpet carpet = new RoomCarpet(new RoomDimension(l, w), p);
        System.out.println("The total cost of carpeting will be: " + carpet.getTotalCost());
    }
}
