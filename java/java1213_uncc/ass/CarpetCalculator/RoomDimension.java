/* Armin Moradi ITCS1213
 * RoomDimension class
 * calculates the area of a square room
 */

public class RoomDimension {
    private double length;
    private double width;

    public RoomDimension(double len, double w) {
        this.length = len;
        this.width = w;
    }

    public double getArea() {
        return this.length * this.width;
    }

    public String toString() {
        return "Carpet Dimensions: length = " + this.length + ", width = " + this.width;
    }
}
