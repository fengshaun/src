/* Armin Moradi ITCS1213
 * RoomCarpet
 * calculates the total cost of the room's carpet
 */

public class RoomCarpet {
    private RoomDimension size;
    private double carpetCost;

    public RoomCarpet(RoomDimension dim, double cost) {
        this.size = dim;
        this.carpetCost = cost;
    }

    public double getTotalCost() {
        return this.size.getArea() * this.carpetCost;
    }

    public String toString() {
        return "Carpet: area = " + this.size.getArea() + ", total cost = " + getTotalCost();
    }
}
