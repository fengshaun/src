/* Armin Moradi ITCS1213L
 * Lisenced under GPLv3
 *
 * This class simulates the rolling of a die
 */

import java.util.Random;

public class Dice {
    private int face;
    private Random rand;

    public Dice() { 
        this.face = 0;
        this.rand = new Random();
    }

    public int diceRoll() {
        this.face = this.rand.nextInt(6) + 1;
        System.out.print(this);
        return this.face;
    }

    public String toString() {
        String out = "";

        switch (this.face) {
            case 1:
                out = "   \n * \n   \n";
                break;

            case 2:
                out = "*  \n   \n  *\n";
                break;

            case 3:
                out = "*  \n * \n  *\n";
                break;

            case 4:
                out = "* *\n   \n* *\n";
                break;

            case 5:
                out = "* *\n * \n* *\n";
                break;

            case 6:
                out = "* *\n* *\n* *\n";
                break;
            
            default: 
                out = "";
                break;
        }

        return out;
    }
}
