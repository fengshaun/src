/* Armin Moradi ITCS1213
 * Lisenced under GPLv3
 *
 * Simulates a game of craps for those who need to
 * play it even at home on a Monday morning!
 */

import java.util.Scanner;

public class Craps {
    public static void main (String[] args)
    {
        // What is game without a player?
        Player player = new Player(0, 100);

        // initial instructions
        System.out.println("This is a simulation of the game of craps.\n" +
                           "You are given 100 chips initially.  Now, make\n" +
                           "your bet and have fun!");

        // play as much as your want!  Pseudo-infinite loop of craps awesomeness!
        // each loop represents one game
        while(true) {
            System.out.println("Chips: " + player.chips);
            int w = askWager();

            // if the player won the game,
            if (playRound(player)) {
                // make the player rich
                player.chips += 2 * w;
            } else {
                // or make the player pay!
                player.chips -= w;
            }

            // Lost all chips?  Don't want to play?  Time to quit gambling!
            if (!playAnother() || player.chips <= 0) {
                System.out.println("Chips: " + player.chips);
                System.out.println("Bye!");

                // a 0 exit code to make the shell happy!
                System.exit(0);
            }
        }
    }

    // rolls a pair of dice
    public static DicePair rollPair() {
        Dice d = new Dice();
        return new DicePair(d.diceRoll(), d.diceRoll());
    }

    // utility function to calculate the sum
    public static int sumOfPair(DicePair p) {
        return p.fst + p.snd;
    }

    // one game of craps!  All the fun happens here!
    // true signifies win, false signifies loss
    public static boolean playRound(Player player) {
        DicePair pair = rollPair();
        System.out.println("------");

        // Did we win?  Did we lose?  Neither?
        if (sumOfPair(pair) == 7 ||
            sumOfPair(pair) == 11) {
            System.out.println("You Won! The sum of pair was " + sumOfPair(pair));
            return true;
        } else if (sumOfPair(pair) == 2 ||
                   sumOfPair(pair) == 3 ||
                   sumOfPair(pair) == 12) {
            System.out.println("You Lost! The sum of pair was " + sumOfPair(pair));
            return false;
        } else {
            player.points = sumOfPair(pair);
            System.out.println("Points: " + pair.fst + " + " + pair.snd + " = " + player.points);
        }

        // a hijacked for-loop.  Should have been while-loop, but they are pretty much
        // interchangeale for this pair.  For-loop seems more consice!
        for(pair = rollPair(); sumOfPair(pair) != player.points; pair = rollPair()) {
            System.out.println("------");
            // a pseudo-infinite loop that loops until sum of dice pair equals player's points
            if (pair.fst == 7 ||
                pair.snd == 7) {
                // unless the player got a 7, in which case he loses
                System.out.println("You Lost: got 7!");
                return false;
            }
        }

        // the only way to get here is if the for-loop failed, which is
        // when the player actually wins by making the point!
        System.out.println("You Won: made your point: " + pair.fst + " " + pair.snd);
        player.points = 0;
        return true;
    }

    // utility function for maximum laziness
    public static int askWager() {
        Scanner kbd = new Scanner(System.in);
        System.out.print("Wager? ");
        return kbd.nextInt();
    }

    // yet another utility function for even more laziness
    public static boolean playAnother() {
        Scanner kbd = new Scanner(System.in);
        System.out.print("Play another game (y/n)? ");
        return kbd.nextLine().equals("y");
    }
}

/* These classes are mostly used like Haskell datatypes.
 * Please excuse the lack of regard toward the principles of encapsulation.
 */

class DicePair {
    // represents a pair of dice
    public int fst = 0;
    public int snd = 0;

    public DicePair(int x, int y) {
        this.fst = x;
        this.snd = y;
    }
}

class Player {
    // represents a player of craps
    public int points = 0;
    public int chips = 0;

    public Player(int p, int c) {
        this.points = p;
        this.chips = c;
    }
}
