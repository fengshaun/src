/*
 * Copyright (C) 2011 Armin Moradi <amoradi@fedoraproject.org>
 * Course: ITCS 1213 
 * 
 * License: GPLv3 or higher
 *
 * This class is designed to make working with circle objects easier by
 * encapsulating the appropriate behaviour/data.
 */

public class Circle {
    private double radius = 0; /* uninitialized */

    /* The assignment asked for a PI field which equals 3.14159.
     * While this might make it easier to get through this assignment,
     * I prefer to stick to a slightly better solution (which is using
     * java's PI constant) and avoid "reinventing the wheel"
     *
     * For the purpose of following the assignment, here is the declaration
     * that I would have written for PI:
     *
     * private final double PI = 3.14159;
     */

    public Circle() {
        /* Again, the assignment didn't ask for it, but it's often a good
         * idea to include a default constructor
         *
         * I'm not going to re-initialize this.radius since it's already
         * set to 0.
         */
    }

    public Circle(double r) {
        // set the radius to appropriate amount. 
        setRadius(r);
    }

    public void setRadius(double r) {
        /* make sure radius is not a negative number
         * that wouldn't make sense at all!
         */
        this.radius = r <= 0 ? 0 : r;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getDiameter() {
        return 2 * radius;
    }

    public double getCircumference() {
        return 2 * Math.PI * radius;
    }
}
