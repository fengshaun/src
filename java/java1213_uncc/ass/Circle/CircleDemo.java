/*
 * Copyright (C) 2011 Armin Moradi <amoradi@fedoraproject.org>
 * Course: ITCS 1213 
 * 
 * License: GPLv3 or higher
 *
 * This program demonstrates how to use the Circle class.
 * this is often called a "driver" class.
 */

import java.util.Scanner;

public class CircleDemo {
    public static void main (String [] args)
    {
        // get input from user: radius
        System.out.print("What is the radius, my friend? ");

        Scanner kbd = new Scanner(System.in);
        double r = kbd.nextDouble();

        // create a circle object with appropriate radius
        Circle c = new Circle(r);

        // PRINT ALL YOU HAVE!!!
        // Print all the properties of the circle
        System.out.print("\nThe circle's properties are as follows:\n\n" +
                         "area (PI * radius * radius): " + c.getArea() + "\n" +
                         "diameter (radius * 2): " + c.getDiameter() + "\n" +
                         "circumference (PI * 2 * radius): " + c.getCircumference() + "\n");
    }
}
