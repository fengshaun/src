/*
 * Copyright (C) 2011 Armin Moradi <amoradi@fedoraproject.org>
 * Course: ITCS 1213 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This program is designed to calculate weights on different planets
 * based on the user's weight on Earth.  Currently, it only works for
 * the following planets:
 *
 * Mercury
 * Venus
 * Jupiter
 * Saturn
 */

import java.util.Scanner;

public class Planets {
    public static void main(String[] args) {

        // give user prompt instruction
        System.out.print("Please enter your weight (lb) on Earth: ");

        // get the user's weight on Earth from standard input as double
        Scanner kbd = new Scanner(System.in);
        double weightOnEarth = kbd.nextDouble();

        // this one is to separate the question from the answer
        System.out.println();

        /* calculate and print all the weights one by one
         * the extra spacing between the name and the word 'is'
         * is so the output is aligned well when a monospaced
         * font is used (most probable in a terminal setting.
         */
        System.out.println("Your weight on Mercury is " +
                           weightOnEarth + "lb * 0.4 = " +
                           weightOnEarth * 0.4 + "lb");

        System.out.println("Your weight on Venus   is " +
                           weightOnEarth + "lb * 0.9 = " +
                           weightOnEarth * 0.9 + "lb");

        System.out.println("Your weight on Jupiter is " +
                           weightOnEarth + "lb * 2.5 = " +
                           weightOnEarth * 2.5 + "lb");

        System.out.println("Your weight on Saturn  is " +
                           weightOnEarth + "lb * 1.1 = " +
                           weightOnEarth * 1.1 + "lb");

    }
}
