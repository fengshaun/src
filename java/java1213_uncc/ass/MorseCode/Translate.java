/* Armin Moradi ITCS1213
 * Gets a string of characters and translates it into morse code
 *
 * requires:
 *     Morse.txt containing a key/value list of morse codes like so
 *         1 .----
 *         2 ..---
 *         3 ...--
 *         .
 *         .
 *         .
 *         Y -.--
 *         Z --..
 */

import java.io.*; // IOException, File
import java.util.*; // Scanner, HashMap

public class Translate {
    private String line = "";
    private HashMap<String, String> map;

    public Translate(String l) throws IOException {
        Scanner f = new Scanner(new File("Morse.txt"));
        map = new HashMap<String, String>();

        // putting the file into a HashMap for further processing
        while (f.hasNext()) { map.put(f.next(), f.next()); }
        this.line = l.toUpperCase();
    }

    public String translate() {
        String res = "";

        for (int i = 0; i < line.length(); i++) {
            String c = Character.toString(line.charAt(i));

            if (map.containsKey(c)) {
                res += (String)map.get(c) + " ";
            } else {
                res += "   ";
            }
        }

        return res;
    }
}

