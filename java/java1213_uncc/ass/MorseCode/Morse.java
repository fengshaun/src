/* Armin Moradi ITCS1213
 * Reads a sentence and translates it to morse code
 * 
 * requires:
 *     Translate.java
 */

import java.util.Scanner;
import java.io.IOException;

public class Morse {
    public static void main (String[] args) throws IOException {
        // just read stuff from keyboard and print the morse
        Scanner kbd = new Scanner(System.in);
        System.out.println("Latin to Morse code converter");

        while (true) {
            System.out.print("Enter sentence: ");
            String line = kbd.nextLine();

            if (line.equals("0")) { break; }

            System.out.println("Sentence: " + line);
            System.out.println("Morse code: " + (new Translate(line)).translate());
        }

        System.out.println("Exiting...bye!");
        System.exit(0);
    }

}
