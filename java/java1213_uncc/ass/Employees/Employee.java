/* Armin Moradi ITCS1213
 * Employee.java
 *
 * representation of an employee
 */

import java.util.StringTokenizer;

public class Employee {
    // class attribute name with type String
    private String name;
    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }

    // class attribute number with type String
    private String number;
    public String getNumber() { return this.number; }
    public void setNumber(String number) { this.number = number; }

    public String getDepartment() {
        StringTokenizer t = new StringTokenizer(this.number, "-\n");
        t.nextToken();
        String n = "";
        String v = t.nextToken();
        if (v.equals("H")) {
            n += "Human resources";
        } else if (v.equals("A")) {
            n += "Accounting";
        } else if (v.equals("P")) {
            n += "Production";
        } else if (v.equals("S")) {
            n += "Shipping";
        }

        return n;
    }

    // class attribute hireDate with type String
    private String hireDate;
    public String getHireDate() { return this.hireDate; }
    public void setHireDate(String hireDate) { this.hireDate = hireDate; }

    public Employee(String name, String num, String date) {
        setName(name);
        setNumber(num);
        setHireDate(date);
    }
}
