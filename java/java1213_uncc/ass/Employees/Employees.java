/* Armin Moradi ITCS1213
 * Employees.java
 *
 * reads and parses information about a set of employees from a txt file
 */

import java.io.*;
import java.util.*;

public class Employees {
    public static void main (String[] args) throws IOException {
        // setup IO
        Scanner iFile = new Scanner(new File("Information.txt"));
        PrintWriter oFile = new PrintWriter("Department.txt");
        
        // process input file
        while (iFile.hasNextLine()) {
            try {
                StringTokenizer t = new StringTokenizer(iFile.nextLine());
                ProductionWorker p = new ProductionWorker(t.nextToken() + " " + t.nextToken(),
                                                        t.nextToken(),
                                                        t.nextToken(),
                                                        Integer.parseInt(t.nextToken()),
                                                        Double.parseDouble(t.nextToken()));

                System.out.println(p);
                oFile.println(p);
            } catch (NoSuchElementException e) {
                // blank line.  Move along...
            }
        }
    
        // finish IO
        oFile.close();
        iFile.close();
        System.exit(0);
    }
}
