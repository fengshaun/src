/* Armin Moradi ITCS1213
 * ProductionWorker.java
 *
 * representation of a production worker
 */

public class ProductionWorker extends Employee {
    // class attribute shiftNumber with type int
    private int shiftNumber;
    public int getShiftNumber() { return this.shiftNumber; }
    public void setShiftNumber(int s) { this.shiftNumber = s; }
    public String getShift() { 
        if (shiftNumber == 1) {
            return "Morning";
        } else if (shiftNumber == 2) {
            return "Swing";
        } else if (shiftNumber == 3) {
            return "Night";
        }

        return "";
    }

    // class attribute payRate with type int
    private double payRate;
    public double getPayRate() { return this.payRate; }
    public void setPayRate(double payRate) { this.payRate = payRate; }
    
    // constructor
    public ProductionWorker(String name, String number, String date, int shift, double pay) {
        super(name, number, date);
        setShiftNumber(shift);
        setPayRate(pay);
    }

    // convert to string
    public String toString() {
        return "Employee\n--------\nname: " + getName() + "\nnumber: " + getNumber() + "\ndepartment: " + getDepartment() + "\nhire date: " + getHireDate() + "\nshift: " + getShift() + "\nhourly pay rate: $" + getPayRate() + "\n";
    }
}
