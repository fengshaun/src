/* Armin Moradi ITCS 1213L Test #1
 * Lisenced under GPLv3
 */

import java.util.Scanner;

public class Pet {
    public static void main (String[] args)
    {
        Scanner kbd = new Scanner(System.in);
        System.out.print("Enter the name of the pet: ");
        String pet = kbd.nextLine();

        System.out.println("# of chars: " + pet.length());
        System.out.println("lowercase: " + pet.toLowerCase());
        System.out.println("uppercase: " + pet.toUpperCase());
        System.out.println("first char: " + pet.charAt(0));
    
        System.exit(0);
    }
}
