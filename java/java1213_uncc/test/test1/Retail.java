/* Armin Moradi ITCS1213L Test #1
 * Lisenced under GPLv3
 */

public class Retail {
    // class attribute description with type String
    private String description;
    public String getDescription() { return this.description; }
    public void setDescription(String description) { this.description = description; }

    // class attribute unitsOnHand with type int
    private int unitsOnHand;
    public int getUnitsOnHand() { return this.unitsOnHand; }
    public void setUnitsOnHand(int unitsOnHand) { this.unitsOnHand = unitsOnHand; }

    // class attribute price with type double
    private double price;
    public double getPrice() { return this.price; }
    public void setPrice(double price) { this.price = price; }

    public Retail(String desc, int units, double price) {
        this.description = desc;
        this.unitsOnHand = units;
        this.price = price;
    }

    public void increasePrice() {
        this.price += this.price * 0.05;
    }
}
