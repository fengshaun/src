/* Armin Moradi ITCS 1213L Test #1
 * Lisenced under GPLv3
 */

public class RetailItem {
    public static void main (String[] args)
    {
        Retail jacket = new Retail("Jacket", 12, 60.00);
        Retail jeans = new Retail("Designer Jeans", 40, 100.00);
        Retail shirt = new Retail("Shirt", 20, 25.00);

        printDetails(jacket);
        printDetails(jeans);
        printDetails(shirt);
    
        System.exit(0);
    }

    public static void printDetails(Retail r) {
        System.out.println("Description: " + r.getDescription());
        System.out.println("Units on hand: " + r.getUnitsOnHand());
        System.out.println("Original price: " + r.getPrice());
        r.increasePrice();
        System.out.println("Increased price: " + r.getPrice());
    }
}
