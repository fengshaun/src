/* Armin Moradi ITCS1213
 * Test #3
 */
 
import java.util.Scanner;
import java.util.StringTokenizer;

public class SSN {
	public static void main(String[] args) {
		Scanner kbd = new Scanner(System.in);
		System.out.print("Enter SSN (XXX-XX-XXXX): ");
		String ssn = kbd.nextLine();
		
		StringTokenizer t = new StringTokenizer(ssn, "-");

		// skip the first two tokens
		t.nextToken(); t.nextToken();
		
		System.out.println(t.nextToken());
	}
}