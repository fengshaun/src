/* Armin Moradi ITCS1213
 * Test #3
 */

import java.util.Scanner;

public class Password {
	public static void main(String[] args) {
		Scanner kbd = new Scanner(System.in);
		System.out.print("Enter password: ");
		String pass = kbd.nextLine();
		
		if (Verify.isValidPassword(pass)) {
			System.out.println("It's valid");
		} else {
			System.out.println("It's not valid");
		}
	}
}