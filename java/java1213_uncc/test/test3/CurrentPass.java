/* Armin Moradi ITCS1213
 * Test #3
 */
 
import java.io.*;
import java.util.Scanner;
 
public class CurrentPass {
 	public static void main(String[] args) throws IOException {
		// read ALL the passwords
		Scanner file = new Scanner(new File("Passwords.txt"));
		String[] passwords = new String[30];
		int c = 0;
		
		while(file.hasNextLine()) {
			passwords[c] = file.nextLine();
			c++; // lol
		}
		
		Scanner kbd = new Scanner(System.in);
		String input = "";

		System.out.print("Enter password: ");
		String p = kbd.nextLine();
		
		while (!p.equals("0")) {
			boolean valid = false;
			for (int i = 0; i < 30; i++) {
				if (passwords[i].equals(p)) {
					valid = true;
				}
			}
			
			if (valid) {
				System.out.println("Password is current.  You are allowed in the system.");
			} else {
				System.out.println("Password is not current.  Please try again later.");
			}
			
			System.out.print("Enter password: ");
			p = kbd.nextLine();
		}	
	}
}