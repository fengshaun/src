/* Armin Moradi ITCS1213
 * Test #3
 */

public class Verify {
	public static boolean isValidPassword(String passwd) {
		// requirements
		int length;
		int digits = 0;
		boolean uppercase = false;
		boolean lowercase = false;

		length = passwd.length();
		
		for (int i = 0; i < passwd.length(); i++) {
			if (Character.isDigit(passwd.charAt(i))) {
				digits++;
			} else if (Character.isUpperCase(passwd.charAt(i))) {
				uppercase = true;
			} else if (Character.isLowerCase(passwd.charAt(i))) {
				lowercase = true;
			}
		}			
		
		// check all
		if (length >= 6 &&
			 digits >= 2 &&
			 uppercase &&
			 lowercase) {
			 return true;
		} else {
			return false;
		}			
	}
}