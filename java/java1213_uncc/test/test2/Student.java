/* Armin Moradi ITCS1213
 * Nov. 4, 2011
 * A student representation class
 * This class represents a student and makes
 * working with an student object easier.
 */
 
import java.text.DecimalFormat;

public class Student {
    // class attribute studentName with type String
    private String studentName;
    public String getStudentName() { return this.studentName; }
    public void setStudentName(String studentName) { this.studentName = studentName; }

    // class attribute GPA with type double
    private double GPA;
    public double getGPA() { return this.GPA; }
    public void setGPA(double GPA) { this.GPA = GPA; }
    
    public Student() {
        setGPA(0.0);
    }

    public Student(String studentName, double GPA) {
        setStudentName(studentName);
        setGPA(GPA);
    }

    public Student(String studentName) {
        setStudentName(studentName);
    }

    // determines whether the student's GPA is above C/2.0
    public boolean isAboveC() {
        if (getGPA() > 2.0) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
	     DecimalFormat f = new DecimalFormat("0.00");
        return "Name: " + getStudentName() + "\n" + "GPA: " + f.format(getGPA()) + "\n" + "This student's GPA " + (isAboveC() ? "is" : "is not") + " above C (2.0)";
    }
}
