/* Armin Moradi ITCS1213
 * Nov. 4, 2011
 * A driver class for Student.java
 * This class uses and tests the Student.java class
 */

import javax.swing.*;

public class StudentGPA {
    public static void main (String[] args) {
        // create a student class with needed fields taken as input from the user
        Student stu = new Student(JOptionPane.showInputDialog("What is the student's name?"), Double.parseDouble(JOptionPane.showInputDialog("What is the student's GPA?")));

        // show the student's name and GPA to the user and whether the student got a C or above
        JOptionPane.showMessageDialog(null, stu);
		      
        System.exit(0);
    }
}
