/* Armin Moradi ITCS1213
 * Nov. 4, 2011
 * 'Increases' program
 * Calculates a price increase for items sold
 */

import java.io.*;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Increases {
    public static void main (String[] args) throws IOException {
        Scanner iFile = new Scanner(new File("oldPrices.txt")); // to read from oldPrices.txt
        PrintWriter oFile = new PrintWriter("newPrices.txt"); // to write to newPrices.txt

        DecimalFormat formatter = new DecimalFormat("##0.00");
        
        // transform all the prices into new prices (25% increase) and print as you go
        while (iFile.hasNext()) {
            double oldPrice = iFile.nextDouble();
            double newPrice = oldPrice + oldPrice * 0.25;

            System.out.println("Old price: " + formatter.format(oldPrice) + "\tNew price: " + formatter.format(newPrice));
            oFile.println(formatter.format(newPrice));
        } 

        // cleanup time!
        iFile.close();
        oFile.close();
        System.exit(0);
    }
}
