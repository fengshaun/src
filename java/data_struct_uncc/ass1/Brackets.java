/* Armin Moradi ITCS2214 
 * checks for balanced brackets
 */

import java.util.Scanner;

public class Brackets {
    public static void main (String[] args) {
        Stack<Character> brackets = new Stack<Character>();

        Scanner kbd = new Scanner(System.in);
        System.out.print("Please enter a string: ");
        String testStr = kbd.nextLine();

        for (int i = 0; i < testStr.length(); i++) {
            char c = testStr.charAt(i);
            if (isLeftBracket(c)) {
                brackets.push(c);
            } else if (isRightBracket(c)) {
                char r = c;
                char l = brackets.pop();
                if (!isMatchingBracket(l, r)) {
                    System.out.println("Error: unbalanced brackets");
                    break;
                }
            }

        }

        if (brackets.size() == 0) {
            System.out.println("Brackets are balanced");
        } else {
            System.out.println("Error: unbalanced brackets");
        }
    }

    public static boolean isLeftBracket(char c) {
        switch (c) {
            case '(': case '[': case '{':
                return true;
            
            default: 
                break;
        }
        return false;
    }

    public static boolean isRightBracket(char c) {
        switch (c) {
            case ')': case ']': case '}':
                return true;
            
            default: 
                break;
        }
        return false;
    }

    public static boolean isMatchingBracket(char l, char r) {
        if ((l == '(' && r == ')') ||
            (l == '[' && r == ']') ||
            (l == '{' && r == '}')) {
            return true;
        } 
        return false;
    }
}
