/* Armin Moradi ITCS1213 */
public class DLItem<E> {
    public DLItem(E elem) {
        setElem(elem);
    }

    // class attribute elem with type E
    private E elem;
    public E getElem() { return this.elem; }
    public void setElem(E elem) { this.elem = elem; }
    
    // class attribute next with type DLItem<E>
    private DLItem<E> next;
    public DLItem<E> getNext() { return this.next; }
    public void setNext(DLItem<E> next) { this.next = next; }

    // class attribute prev with type DLItem<E>
    private DLItem<E> prev;
    public DLItem<E> getPrev() { return this.prev; }
    public void setPrev(DLItem<E> prev) { this.prev = prev; }
}
