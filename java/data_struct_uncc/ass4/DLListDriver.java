/* Armin Moradi ITCS1214
 * Doubly-linked list assignment
 */

import java.util.Scanner;
import java.io.*;

public class DLListDriver {
    public static void main (String[] args) throws IOException {
        DLList<String> list = new DLList<String>();
        
        Scanner kbd = new Scanner(System.in);
        System.out.print("Enter filename: ");
        String fn = kbd.nextLine();

        Scanner iFile = new Scanner(new File(fn));
        
        while(iFile.hasNextLine()) {
            list.add(iFile.nextLine());
        }

        DLIterator<String> it = list.iterator(); 
        while(it.hasNext()) { System.out.println(it.next()); }

        iFile.close();
    }
}
