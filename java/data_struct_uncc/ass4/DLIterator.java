/* Armin Moradi ITCS1213 */
public class DLIterator<I> {
    // private attribute 'item' with type 'DLItem<I>'
    private DLItem<I> item = null;

    public DLIterator(DLItem<I> item) { this.item = item; }

    public boolean hasNext() { return this.item != null; } 

    public I next() {
        I res = this.item.getElem();
        this.item = this.item.getNext();
        return res;
    }
}
