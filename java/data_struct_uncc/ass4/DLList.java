/* Armin Moradi ITCS 1214 assignment #4
 * Doubly Linked List 
 */

public class DLList<T> {
    private DLItem<T> front;
    private DLItem<T> rear;
    private int count;
    
    public DLList() {
        front = null;
        rear = null;
        count = 0;
    }

    @SuppressWarnings("unchecked")
    public void add(T elem) {
        Comparable<T> item = (Comparable<T>) elem;
        DLItem<T> temp = new DLItem<T>(elem);
        DLItem<T> c = this.front;

        if (isEmpty()) {
            this.front = this.rear = temp;
            count++;
        } else if (size() == 1) {
            if (item.compareTo(this.front.getElem()) >= 0) {
                this.front.setNext(temp);
                temp.setPrev(this.front);
                this.rear = temp;
                count++;
            } else {
                this.front.setPrev(temp);
                temp.setNext(this.front);
                this.front = temp;
                count++;
            }
        } else {
            while(item.compareTo(c.getElem()) >= 0 && c != this.rear) { c = c.getNext(); }

            if (c == this.front) {
                this.front.setPrev(temp);
                temp.setNext(this.front);
                this.front = temp;
                count++;
            } else if (c == this.rear && item.compareTo(c.getElem()) > 0) {
                this.rear.setNext(temp);
                temp.setPrev(this.rear);
                this.rear = this.rear.getNext();
                count++;
            } else {
                temp.setNext(c);
                temp.setPrev(c.getPrev());

                temp.getNext().setPrev(temp);
                temp.getPrev().setNext(temp);
                count++;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void remove(T elem) {
        Comparable<T> item = (Comparable<T>) elem;
        DLItem<T> temp = new DLItem<T>(elem);
        DLItem<T> c = this.front;

        while(item.compareTo(c.getElem()) != 0 && c != this.rear) { c = c.getNext(); }
        if (c == this.rear) {
            if (item.compareTo(c.getElem()) == 0) {
                this.rear = this.rear.getPrev();
                this.rear.setNext(null);
                count--;
            } else {
                System.out.println("DLList: No such element");
            }
        } else {
            if (c == this.front) {
                this.front = this.front.getNext();
                this.front.setPrev(null);
                count--;
            } else {
                c.getPrev().setNext(c.getNext());
                c.getNext().setPrev(c.getPrev());
                count--;
            }
        }
    }

    public void removeFirst() {
        this.front = this.front.getNext();
        this.front.setPrev(null);
        count--;
    }

    public void removeLast() {
        this.rear = this.rear.getPrev();
        this.rear.setNext(null);
        count--;
    }

    public int size() {
        return count;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public DLIterator<T> iterator() {
        return new DLIterator<T>(this.front);
    }

    public String toString() {
        String output = "";
        for (DLItem<T> c = this.front; c != this.rear; c = c.getNext()) {
            output += c.getElem().toString() + " | ";
        }
        output += this.rear.getElem().toString();

        return output;
    }
}

