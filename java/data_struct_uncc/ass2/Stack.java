/* Armin Moradi ITCS2214
 * Stack implementation
 */

public class Stack<T> {
    private int count = 0;
    private StackItem<T> top;

    public Stack() {
    }

    public void push(T elem) {
        if (this.count == 0) {
            this.top = new StackItem<T>(elem);
        } else {
            StackItem<T> tmp = new StackItem<T>(elem);
            tmp.setNext(this.top);
            this.top = tmp;
        }
        this.count++;
    }

    public T pop() {
        StackItem<T> tmp = this.top;
        this.top = tmp.getNext();
        tmp.setNext(null);
        this.count--;
        return tmp.getValue();
    }

    public T peek() {
        return this.top.getValue();
    }

    public int size() {
        return this.count;
    }
}

class StackItem<T> {
    private T value;
    private StackItem<T> next;

    public StackItem(T elem) {
        this.value = elem;
    }

    public StackItem<T> getNext() {
        return this.next;
    }

    public void setNext(StackItem<T> elem) {
        this.next = elem;
    }

    public T getValue() {
        return this.value;
    }
}
