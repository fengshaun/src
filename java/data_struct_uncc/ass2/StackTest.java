public class StackTest {
    public static void main (String[] args) {
        Stack<Integer> m_stack = new Stack<Integer>();
        m_stack.push(new Integer(4));
        m_stack.push(new Integer(5));
        m_stack.pop();
        m_stack.push(new Integer(6));

        System.out.println(m_stack.pop());
        System.out.println(m_stack.pop());
    
        System.exit(0);
    }
}
