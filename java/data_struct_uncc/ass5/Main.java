/* Armin Moradi ITCS 1214
 */

import java.io.*;
import java.util.*;

public class Main {
    public static void main (String[] args) throws IOException {
        BTree<String> tree = new BTree<String>();
        System.out.print("Enter filename: ");
        Scanner kbd = new Scanner(System.in);
        Scanner iFile = new Scanner(new File(kbd.nextLine()));

        while (iFile.hasNextLine()) {
            String l = iFile.nextLine();
            tree.add(l);
        }

        System.out.println("\n\nPre-order traversal: ");
        for(Iterator<String> it = tree.itPreOrder(); it.hasNext();) { System.out.print(it.next() + " "); }

        System.out.println("\n\nIn-order traversal: ");
        for(Iterator<String> it = tree.itInOrder(); it.hasNext();) { System.out.print(it.next() + " "); }

        System.out.println("\n\nPost-order traversal: ");
        for(Iterator<String> it = tree.itPostOrder(); it.hasNext();) { System.out.print(it.next() + " "); }

        System.out.println("\n\nLevel-order traversal: ");
        for(Iterator<String> it = tree.itLevelOrder(); it.hasNext();) { System.out.print(it.next() + " "); }

        iFile.close();
    }
}
