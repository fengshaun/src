/* Armin Moradi ITCS 1214
 * Binary tree assignment
 */

import java.util.*;

public class BTree<T> {
    T[] storage;
    int count;
    final int D_CAP = 20;

    @SuppressWarnings("unchecked")
    public BTree() { storage = (T[])(new Object[D_CAP]); }

    public void add(T elem) {
        if (size() >= storage.length) { expand(); }
        storage[count] = elem;
        count++;
    }

    @SuppressWarnings("unchecked")
    private void expand() {
        T[] newS = (T[])(new Object[storage.length * 2]);
        for (int i = 0; i < storage.length; i++) { newS[i] = storage[i]; }
        storage = newS;
    }

    public T getRoot() { return storage[0]; }
    public boolean isEmpty() { return (count == 0); }
    public int size() { return count; }

    public boolean contains(T elem) {
        for (int i = 0; i < storage.length && storage[i] != null; i++) {
            if (elem.equals(storage[i])) { return true; }
        }
        return false;
    }

    public T find(T elem) {
        for (int i = 0; i < storage.length && storage[i] != null; i++) {
            if (elem.equals(storage[i])) { return storage[i]; }
        }
        return null;
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < storage.length && storage[i] != null; i++) { s += storage[i].toString() + " "; }
        return s;
    }

    public Iterator<T> itInOrder() {
        ArrayList<T> temp = new ArrayList<T>();
        inorder(0, temp);
        return temp.iterator();
    }

    private void inorder(int node, ArrayList<T> temp) {
        if (node < storage.length && storage[node] != null) {
            inorder(node * 2 + 1, temp);
            temp.add(storage[node]);
            inorder(node * 2 + 2, temp);
        }
    }

    public Iterator<T> itPreOrder() {
        ArrayList<T> temp = new ArrayList<T>();
        preorder(0, temp);
        return temp.iterator();
    }

    private void preorder(int node, ArrayList<T> temp) {
        if (node < storage.length && storage[node] != null) {
            temp.add(storage[node]);
            preorder(node * 2 + 1, temp);
            preorder(node * 2 + 2, temp);
        }
    }

    public Iterator<T> itPostOrder() {
        ArrayList<T> temp = new ArrayList<T>();
        postorder(0, temp);
        return temp.iterator();
    }

    private void postorder(int node, ArrayList<T> temp) {
        if (node < storage.length && storage[node] != null) {
            postorder(node * 2 + 1, temp);
            postorder(node * 2 + 2, temp);
            temp.add(storage[node]);
        }
    }

    public Iterator<T> itLevelOrder() {
        ArrayList<T> temp = new ArrayList<T>();
        for (int i = 0; i < storage.length && storage[i] != null; i++) { temp.add(storage[i]); }
        return temp.iterator();
    }

}
