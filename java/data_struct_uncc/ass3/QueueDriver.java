/* Armin Moradi ITCS 2214
 * Queue driver class
 */

import java.util.Scanner;

public class QueueDriver {
    public static void main (String[] args) {
        Queue<String> qu = new Queue<String>();

        while (true) {
            Scanner kbd = new Scanner(System.in);
            System.out.print("Command (@? for help): ");
            
            String cmd = kbd.nextLine();

            if (cmd.equals("@?") || cmd.equals("@help")) {
                help();
            } else if (cmd.equals("@q") || cmd.equals("@quit")) {
                break;
            } else if (cmd.equals("@dequeue")) {
                System.out.println((qu.first() != null) ? qu.dequeue() : "Error: queue is empty");
            } else if (cmd.equals("@first")) {
                System.out.println((qu.first() != null) ? qu.first() : "Error: queue is empty");
            } else if (cmd.equals("@print")) {
                System.out.println(qu);
            } else {
                qu.enqueue(cmd);
            }

            System.out.println();
        }
    }

    public static void help() {
        System.out.println("Command         Action");
        System.out.println("@? or @help     show this help message");
        System.out.println("@q or @quit     quit");
        System.out.println("@dequeue        dequeue and print item");
        System.out.println("@first          print first item");
        System.out.println("@print          print the queue");
        System.out.println("any string      equeue the string");
    }
}
