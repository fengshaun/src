/* Armin Moradi ITCS 2214
 * Queues assignment
 */

class QueueItem<T> {
    private T v;

    // class attribute value with type T
    private T value;
    public T getValue() { return this.value; }
    public void setValue(T value) { this.value = value; }

    // class attribute next with type QueueItem<T>
    private QueueItem<T> next;
    public QueueItem<T> getNext() { return this.next; }
    public void setNext(QueueItem<T> next) { this.next = next; }
    
    public QueueItem(T v) {
        this.setValue(v);
    }
}

public class Queue<T> {
    private QueueItem<T> front;
    private QueueItem<T> rear;

    private int count = 0;

    public Queue() { }

    public void enqueue(T i) {
        if (this.isEmpty()) {
            this.rear = new QueueItem<T>(i);
            this.front = rear;
        } else {
            QueueItem<T> tmp = new QueueItem<T>(i);
            this.rear.setNext(tmp);
            this.rear = tmp;
        }
        this.count++;
    }

    public T dequeue() {
        if (this.isEmpty()) { return null; }

        QueueItem<T> tmp = this.front;
        this.front = this.front.getNext();
        tmp.setNext(null);
        this.count--;
        return tmp.getValue();
    }

    public String toString() {
        if (this.isEmpty()) { return "empty"; }

        String tmp = "";
        QueueItem<T> current = this.front;
        tmp += current.getValue().toString() + " ";
        
        for (int i = 1; i < size(); i++) {
            current = current.getNext();
            tmp += current.getValue().toString() + " ";
        }

        return tmp;
    }

    public int size() {
        return this.count;
    }

    public boolean isEmpty() {
        return this.size() == 0 ? true : false;
    }

    public T first() {
        if (isEmpty()) { return null; }
        return this.front.getValue();
    }
}
