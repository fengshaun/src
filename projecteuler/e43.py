from pylib.numbers import xpermutations
from pylib.prime import primes_count

# precalculating primes reduces time
# from 1.55s to 55s!
primes = list(primes_count(7))

def is_divisible_by_primes(l):
    a = 1
    b = 2
    c = 3
    for i in primes:
        n = int('%d%d%d' % (l[a], l[b], l[c]))

        if n % i != 0:
            return False
        a += 1; b += 1; c += 1

    return True

def solve():
    sum = 0
    for i in xpermutations(range(10)):
        if is_divisible_by_primes(i):
            n = int(''.join([str(x) for x in i]))
            sum += n

    return sum

if __name__ == '__main__':
    print solve()
