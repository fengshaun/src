from pylib.prime import is_prime, prime_factors

def solve():
    for i in range(1, 1000000):
        if (len(prime_factors(i)) == 4 and
            len(prime_factors(i + 1)) == 4 and
            len(prime_factors(i + 2)) == 4 and
            len(prime_factors(i + 3)) == 4):
            return i

if __name__ == '__main__':
    print solve()
