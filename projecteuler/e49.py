from pylib.numbers import xpermutations, xcombinations
from pylib.prime import is_prime
from pylib.numbers import digits

def check_numbers(l):
    nums = []
    for i in xpermutations(l):
        nums.append(int(''.join([str(x) for x in i])))

    #print nums

    for i in xcombinations(nums, 3):
        #print '\t', i
        diff = i[1] - i[0]
        if (i[2] - i[1] == i[1] - i[0]):
            if (is_prime(i[0]) and
                is_prime(i[1]) and
                is_prime(i[2])):
                #print 'found:', i
                print 'found:', i
                return True
    return False

def check_digits(a, b, c):
    a = sorted(digits(a))
    b = sorted(digits(b))
    c = sorted(digits(c))

    for i in range(len(a)):
        if not a[i] == b[i] == c[i]:
            return False

    return True

def solve():
    '''
    for i in xcombinations(range(1, 10), 4):
        #print i,
        if check_numbers(i):
            #print 'FOUND:', i
            pass
    '''
    for a in range(1000, 10000):
        if is_prime(a):
            for b in range(a + 1, 10000):
                if is_prime(b):
                    if is_prime(b + (b - a)) and (b + (b - a)) < 10000:
                        if check_digits(a, b, b+(b-a)):
                            print a, b, b + (b - a)

if __name__ == '__main__':
    print solve()
