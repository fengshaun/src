use List::Util qw/sum/;

# processes the output of e59.hs
open IFH, "<", "cipher1_output.txt";
open OFH, ">", "cipher1_output_final.txt";

chomp(my $content = <IFH>);
my @x = split /\|\|\|\|/, $content;

# I had to go through the output by hand, and found the correctly
# decoded message to have The Gospel of John in the beginning
my $x = (grep { /The Gospel of John/ } (split /\|\|\|\|/, $content))[0];

my $answer = sum (map { ord $_ } (split //, $x));

print OFH $x;
print $answer;
