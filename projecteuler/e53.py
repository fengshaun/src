from pylib.mathfn import combination as com

def solve():
    return len([1 for n in range(1, 101) for r in range(n + 1) if com(n, r) > 1000000])

if __name__ == '__main__':
    print solve()
    
