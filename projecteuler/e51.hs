-- ProjectEuler.net problem 51

import Data.Numbers.Primes

replaceAt :: [a] -> Int -> [a] -> [a]
replaceAt xs n r = (fst . splitAt n $ xs) ++ r ++ (tail . snd . splitAt n $ xs)

e51 :: [[Int]]
e51 = map replace1Digit [x | x <- takeWhile (<1000) primes, x > 995]
      where 
        replace1Digit x = map read [replaceAt (show x) i r | i <- [0..(length . show $ x) - 1], r <- (map show [1..9])]

main :: IO ()
main = print . show $ e51
