def sum_of_fifth(n):
    l = [int(x) ** 5 for x in str(n)]
    s = reduce((lambda x, y: x + y), l)
    return True if n == s else False

def solve():
    sum = 0
    for i in range(1000, 1000000):
        if sum_of_fifth(i):
            sum += i
    return sum

if __name__ == '__main__':
    print solve()
