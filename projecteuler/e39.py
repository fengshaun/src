from __future__ import division
import math

def get_solutions(p):
    # pythagorean triples
    # http://www.friesian.com/pythag.htm
    num_sol = 0
    for a in range(1, p // 4 + 1):
        for b in range(a + 1, (p - a) // 2):
            c = math.sqrt(a ** 2 + b ** 2)

            if a + b + c == p:
                num_sol += 1

    return num_sol

import sys
def solve():
    max_sol = 0
    max_p = 0

    # only even valued perimeters are needed
    for p in xrange(1000, 0, -2):
        print p,
        num_sol = get_solutions(p)
        print num_sol
        if num_sol > max_sol:
            max_p = p
            max_sol = num_sol

    return max_p, max_sol

if __name__ == '__main__':
    print solve()
