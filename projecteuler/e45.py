from pylib.numbers import is_pentagonal, is_triangle, is_hexagonal
from pylib.numbers import hexagonal

def next_hex_from(n):
    num = n
    while 1:
        yield hexagonal(num)
        num += 1

def solve():
    # 143th hex number is 40755
    for n in next_hex_from(144):
        print n
        if is_pentagonal(n) and is_triangle(n):
            return n

if __name__ == '__main__':
    print solve()
