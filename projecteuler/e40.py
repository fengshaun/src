def solve():
    s = ''
    for i in xrange(1, 1000001):
        s += str(i)

    m = int(s[0]) * int(s[9]) * int(s[99]) * int(s[999]) * int(s[9999]) * int(s[99999]) * int(s[999999])
    return m

if __name__ == '__main__':
    print solve()
