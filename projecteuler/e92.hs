nextChainNum :: Int -> Int
nextChainNum n = f 0 n
                 where
                   f res x | x <= 0 = res
                           | otherwise = f (res + (x `mod` 10) ^ 2) (x `div` 10)

eventualChainNum :: Int -> Int
eventualChainNum 89 = 89
eventualChainNum 1 = 1
eventualChainNum n = eventualChainNum . nextChainNum $ n

chainUpTo :: Int -> [(Int, Int)]
chainUpTo n = [(x, x2) | x <- [1..n], let x2 = eventualChainNum x]

cache :: [Int]
cache = [fst x | x <- (filter (\(a, b) -> b == 89) . chainUpTo $ 9 * 9 * 7)]

e92 :: Int
e92 = length . filter (`elem` cache) . map nextChainNum $ [9*9*7..10000000]

main :: IO ()
main = putStrLn . show $ e92
