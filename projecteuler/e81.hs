{- In the 5 by 5 matrix below, the minimal path sum from the top left
 - to the bottom right, by only moving to the right and down, is indicated
 - in bold red and is equal to 2427.

	
  131	673	234	103	18
  201	96	342	965	150
  630	803	746	422	111
  537	699	497	121	956
  805	732	524	37	331
	

 - Find the minimal path sum, in matrix.txt (right click and 'Save 
 - Link/Target As...'), a 31K text file containing a 80 by 80 matrix, from
 - the top left to the bottom right by only moving right and down.
-}

import Data.List
import Data.List.Split
import Control.Applicative
import Text.Printf (printf)

getItem :: [[a]] -> Int -> Int -> a
getItem xxs row col = (xxs !! row) !! col

parse :: String -> [[Int]]
parse = map (map (read :: String -> Int)) . map (splitOn ",") . lines

data Item = Wall
          | Item { val :: Int }
          deriving Show

minPath :: [[Int]] -> Item
minPath []   = Wall
minPath xxs 
  | all null xxs = Wall
  | all (==1) (map length xxs) = Item $ sum . concat $ xxs
  | length xxs == 1 = Item $ sum . head $ xxs
  | otherwise = Item $ lastItem + (case (minPath leftPath, minPath topPath) of
                                    (Wall, Wall)     -> 0
                                    (Item x, Wall)   -> x
                                    (Wall, Item y)   -> y
                                    (Item x, Item y) -> min x y)
  where
  lastItem = last (last xxs)
  leftPath = map init xxs
  topPath  = init xxs

myList = [[131,673,234,103,18 ]
        ,[201,96 ,342,965,150]
        ,[630,803,746,422,111]
        ,[537,699,497,121,956]
        ,[805,732,524,37 ,331]]

testMinPath :: Bool
testMinPath = (==2427) . val . minPath $ myList

main = do
  matrix <- parse <$> readFile "matrix.txt"
  putStrLn . show . val . minPath $ matrix
