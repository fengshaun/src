from pylib import prime

def sum_prime_twice_square(num):
    for p in prime.primes_max(num):
        for n in range(1, (num - p) / 2 + 1):
            if p + (2 * (n ** 2)) == num:
                return True

    return False
    
def solve():
    for i in xrange(33, 1000000, 2):
        if prime.is_prime(i): continue
        if not sum_prime_twice_square(i):
            return i

if __name__ == '__main__':
    print solve()
