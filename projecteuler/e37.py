from pylib import prime
from e33 import digits

def trunc(n, mode='right'):
    l = digits(n)

    for i in range(len(l) - 1):
        if mode == 'right':
            l.pop()
        elif mode == 'left':
            l.pop(0)
        yield int(''.join([str(x) for x in l]))

def is_truncable(n):
    if n < 10: return False

    for i in trunc(n, 'right'):
        if not prime.is_prime(i):
            return False

    for i in trunc(n, 'left'):
        if not prime.is_prime(i):
            return False

    return True

def solve():
    sum = 0
    num = 0
    for n in prime.next_prime():
        if is_truncable(n):
            sum += n
            num += 1
            print n
        if num >= 11:
            break

    return sum
            

if __name__ == '__main__':
    print solve()
