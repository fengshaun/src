# A googol (10^(100)) is a massive number: one followed by one-hundred zeros;
# 100^(100) is almost unimaginably large: one followed by two-hundred zeros.
# Despite their size, the sum of the digits in each number is only 1.
# Considering natural numbers of the form, a^(b), where a, b < 100, what is
# the maximum digital sum?

sum = []

def digitsum(n):
    """sums up all the digits of n
    
    Arguments:
    - `n`: da number!
    """
    s = 0
    for i in str(n):
        s += int(i)

    return s


for a in range(100):
    for b in range(100):
        sum.append(a**b)

sums = []
for n in sum:
    sums.append(digitsum(n))

print max(sums)
