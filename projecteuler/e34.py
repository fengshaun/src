from pylib import mathfn
import e32

# precalculating factorials reduces time
# by half
fact = []
for i in range(10):
    fact.append(mathfn.fac(i))

def sum_of_factorials(l):
    sum = 0

    for i in l:
        sum += fact[i]

    return sum

def solve():
    sum = 0

    # upperlimit is 9999999 since 9!*7 is
    # smaller than 9999999
    for i in range(3, 1000000):
        if sum_of_factorials(e32.digits(i)) == i:
            sum += i

    return sum

if __name__ == '__main__':
    # these numbers are called factorion
    # see http://mathworld.wolfram.com/Factorion.html
    print solve()
