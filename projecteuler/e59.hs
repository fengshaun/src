import Data.List.Split
import Data.Bits
import Data.Char
import Data.List

stringToList = map (\a -> read a :: Int) . splitOn ","

passcodes :: [[Int]]
passcodes = [[a, b, c] | a <- [97..122], -- chr 97 == 'a', chr 122 == 'z'
                         b <- [97..122],
                         c <- [97..122]]

decode :: [Int] -> [[Int]]
decode xs = decode' passcodes xs
            where
              -- replicate because we want to repeat the 3-letter passcode to
              -- match the length of the sentence
              decode' :: [[Int]] -> [Int] -> [[Int]]
              decode' (ps:pps) xs = (zipWith xor (cycle ps) xs) : decode' pps xs
              decode' _ _ = [[]]

e59 :: [[Int]] -> [[Char]]
e59 (decoded:restDecoded) = (map chr $ decoded) : e59 restDecoded
e59 _ = [""]

main :: IO ()
main = do
       content <- readFile "cipher1.txt"
       writeFile "cipher1_output.txt" . show . intercalate "||||" . filter (\a -> isInfixOf "the" a) . e59 . decode . stringToList $ content
       print "done"
