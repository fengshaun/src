def oneOverTwoPlus(i)
  if i == 1
    return Rational(1, 2)
  end
  return Rational(1, oneOverTwoPlus(i - 1) + 2)
end

def iteration(n)
  return oneOverTwoPlus(n) + Rational(1, 1)
end

def main()
  count = 0
  (1..1000).each do |i|
#    print i.to_s + " "
    f = iteration i
    if f.numerator.to_s.length > f.denominator.to_s.length
      count += 1
#      puts "\n\n\n#{i}\n\n\n"
    end
  end
  puts "ANSWER = #{count}"
end

main
