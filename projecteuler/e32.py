import operator

pans = []
sum = 0

def digits(n):
    return [int(i) for i in str(n)]

def is_pandigital_9(l):
    l = sorted(l)
    for i in range(1, 10):
        try:
            if l[i - 1] != i:
                return False
        except IndexError:
            return False
    return True

def solve():
    for a in range(1, 100):
        for b in range(100, 10000):
            if len(digits(a * b)) == 4:
                if is_pandigital_9(digits(a) + digits(b) + digits(a * b)):
                    pans.append(a * b)
            elif len(digits(a * b)) > 4:
                break

    pans = dict([(x, '') for x in pans]).keys()  # uniquification

    return reduce(operator.add, pans)

if __name__ == '__main__':
    print solve()
