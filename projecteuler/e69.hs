import Data.Numbers.Primes

-- keep n large, and phi(n) small, meaning that we should multiply as many consecutive primes
-- as possible below 1,000,000

main = print . last . takeWhile (<1000000) . scanl (*) 1 $ primes
