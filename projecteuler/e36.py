from e33 import digits

_nibbles = {"0":"0000", "1":"0001", "2":"0010", "3":"0011",
            "4":"0100", "5":"0101", "6":"0110", "7":"0111",
            "8":"1000", "9":"1001", "A":"1010", "B":"1011",
            "C":"1100", "D":"1101", "E":"1110", "F":"1111",
            "-":"-"}

def toBase2(number):
    # copied from ActiveState
    # http://code.activestate.com/recipes/440528/
    """toBase2(number): given an int/long, converts it to
    a string containing the number in base 2."""
    # From a suggestion by Dennis Lee Bieber.
    if number == 0:
        return "0"
    result = [_nibbles[nibble] for nibble in "%X"%number]
    result[number<0] = result[number<0].lstrip("0")
    return "".join(result)

def is_palindromic(num):
    l = digits(num)

    if not len(l) % 2 == 0:
        l1 = l[:len(l) / 2 + 1]
    else:
        l1 = l[:len(l) / 2]
    l2 = l[len(l) / 2:]
    l2.reverse()

    for i in range(len(l1)):
        if not l1[i] == l2[i]:
            return False

    return True

def solve():
    sum = 0
    for i in range(1000000):
        if is_palindromic(i) and is_palindromic(int(toBase2(i))):
            sum += i

    return sum

if __name__ == '__main__':
    print solve()
