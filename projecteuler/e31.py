def xcombinations(items, n):
    if n < 1: yield []
    else:
        for i in xrange(len(items)):
            for j in xcombinations(items[i + 1:], n - 1):
                yield [items[i]] + j

def xpermutations(items):
    return xcombinations(items, len(items))

def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def gcd_many(l):
    return reduce(gcd, l)

def find_possibilities(money, max):
    sum = 0

    if max == 7: return 1
    for i in range(max, 8):
        if money - coins[i] == 0: sum += 1
        if money - coins[i] > 0: sum += find_possibilities(money - coins[i], i)

    return sum

coins = [200, 100, 50, 20, 10, 5, 2, 1]

def solve():
    return find_possibilities(200, 0)
    
if __name__ == '__main__':
    print solve()
