from e32 import is_pandigital_9
from e33 import digits

def concat_products(n):
    s = ''
    m = 1
    while 1:
        s += str(m * n)
        if len(s) >= 9:
            return int(s)
        m += 1

def solve():
    largest = 0
    for n in range(9, 10000):
        num = concat_products(n)
        if is_pandigital_9(digits(num)):
            if largest < num < 987654322:
                largest = num

    return largest

if __name__ == '__main__':
    print solve()
