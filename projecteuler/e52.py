from pylib.numbers import is_permutation as is_p

def is_good(x):
    if (is_p(2*x, 3*x) and is_p(3*x, 4*x) and
        is_p(4*x, 5*x) and is_p(5*x, 6*x)):
        return True
    return False

def solve():
    num = 1
    while 1:
        if is_good(num):
            return num
        num += 1

if __name__ == '__main__':
    print solve()
