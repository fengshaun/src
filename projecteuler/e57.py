# It is possible to show that the square root of two can be expressed as an
# infinite continued fraction.

# root(2) = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

# By expanding this for the first four iterations, we get:

# 1 + 1/2 = 3/2 = 1.5
# 1 + 1/(2 + 1/2) = 7/5 = 1.4
# 1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
# 1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

# The next three expansions are 99/70, 239/169, and 577/408, but the eighth
# expansion, 1393/985, is the first example where the number of digits in
# the numerator exceeds the number of digits in the denominator.

# In the first one-thousand expansions, how many fractions contain a
# numerator with more digits than denominator?

class Fraction(object):
    """Represents a fraction in its own right
    """
    
    def __init__(self, num, denom):
        """initializes a fraction using num as numerator and denom
        as denominator
        """
        self.num = num
        self.denom = denom

        if (isinstance(self.num, Fraction) and
            isinstance(self.denom, Fraction)):
            num = self.num.num * self.denom.denom
            denom = self.num.denom * self.denom.num

            self.num = num
            self.denom = denom

    def __div__(self, other):
        """Divides the number and returs another Fraction
        """
        print self, other
        if isinstance(other, Fraction):
            num = self.num * other.denom
            denom = self.denom * other.num

        print num, denom
        return Fraction(num, denom)

    def __add__(self, other):
        """Adds two fractions together
        """
        if isinstance(other, Fraction):
            num = (self.num * other.denom) + (other.num * self.denom)
            denom = self.denom * other.denom
        else:
            return self + Fraction(other, 1)
        
        return Fraction(num, denom)

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        """Subtracts two fractions together
        """
        if isinstance(other, Fraction):
            num = (self.num * other.denom) - (other.num * self.denom)
            denom = self.denom * other.denom
        else:
            return self - Fraction(other, 1)

        return Fraction(num, denom)

    def __mul__(self, other):
        """Multiplies two fractions
        """
        if isinstance(other, Fraction):            
            num = self.num * other.num
            denom = self.denom * other.denom
        else:
            return self * Fraction(other, 1)

        return Fraction(num, denom)

    def __rmul__(self, other):
        return self * other

    def truth(self):
        """Returns true if numerator is not zero
        """
        if self.num != 0:
            return True
        else:
            return False
        
    def __pow__(self, p):
        """Raises the fraction to the power of a number
        """
        num = self.num ** p
        denom = self.denom ** p

    def reduced(self):
        """Reduces the fraction as much as possible
        """
        num = self.num
        denom = self.denom
        while num % 2 == 0 and denom % 2 == 0:
            num /= 2
            denom /= 2
            
        i = 3

        while i <= min(num, denom):
            while (num % i == 0 and
                   denom % i == 0):
                num /= i
                denom /= i

            i += 2

        return Fraction(num, denom)

    def __repr__(self):
        """Representation of Fraction
        """
        return 'Fraction(' + str(self.num) + ', ' + str(self.denom) + ')'

def oneOverTwoPlus(i):
    """1 / (2 + ...)
    """
    if i == 1: return Fraction(1, 2)
    return Fraction(1, oneOverTwoPlus(i - 1) + 2)


def iteration(n, l=None):
    """finds the fraction regarding the nth iteration of
    root of 2
    """
    if l is None:
        return oneOverTwoPlus(n) + 1
    else:
        return Fraction(1, l + 1) + 1

if __name__ == "__main__":
    count = 0
#    lastIter = Fraction(54608393, 38613965)
    f = iteration(34).reduced()
    if len(str(f.num)) > len(str(f.denom)):
        print "YUP"
    
    """    
    for i in range(21, 31):
        f = iteration(i, lastIter).reduced()
        print i, f
        if len(str(f.num)) > len(str(f.denom)):
            print f
            count += 1
        lastIter = f
    """
    print count

