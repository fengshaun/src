def tonum(a):
    if a == 'T': return 10
    elif a == 'J': return 11
    elif a == 'Q': return 12
    elif a == 'K': return 13
    elif a == 'A': return 14
    else:
        return int(a)

def sort_hand_key(x, y):
    # x > y -> 1
    # x < y -> -1
    # x == y -> 0
    a = tonum(x[0])
    b = tonum(x[1])
    
    if a > b: return 1
    elif a == b: return 0
    else: return -1

def evaluate_hand(hand):
    hand.sort(sort_hand_key)
    
    # straight
    for i in range(1, 5):
        if tonum(hand[i][0]) - tonum(hand[i - 1][0]) != 1:
            break
    else:
        # straight flush
        for i in range(1, 5):
            if hand[i][1] != hand[i - 1][1]:
                return 'straight starting %s' % tonum(hand[0][0])
        else:
            # royal flush
            if tonum(hand[0][0]) == 10:
                return 'royal flush'
            else:
                return 'straight flush of %s' % hand[2][1]
    

def compare_hands(a, b):
    i = evaluate_hand(a)
    j = evaluate_hand(b)

    if i > j: return 1
    elif i < j: return -1
    else:
        pass

def solve():
    f = open('poker.txt', 'r')
    for line in f:
        cards = line.split()
        a = cards[:5]
        b = cards[5:]

        if compare_hands(a, b) == 1:
            n += 1

    return n

if __name__ == '__main__':
    print solve()
