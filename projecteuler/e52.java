class e52 {
    public boolean isPandigital(int num) {
        String number = new String(num);
        for (int i = 0; i < number.length; i++) {
            if (number[i] != number[number.length - i]) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        
