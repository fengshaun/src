from pylib import prime
import e33

def xcombinations(items, n):
    if n < 1: yield []
    else:
        for i in xrange(len(items)):
            for j in xcombinations(items[:i] + items[i + 1:], n - 1):
                yield [items[i]] + j

def xpermutations(items):
    return xcombinations(items, len(items))

def xrotations(items):
    yield items

    # len(items) - 1 because we don't want the
    # original number to be repeated
    for i in range(len(items) - 1):
        items = items[1:] + [items.pop(0)]
        yield items

def is_circular(p):
    d = e33.digits(p)

    for i in [int(''.join([str(x) for x in perm])) for perm in xrotations(d)]:
        if not prime.is_prime(i):
            return False

    return True

def solve():
    count = 0

    for p in prime.primes_max(1000000):
        if is_circular(p):
            count += 1

    return count

if __name__ == '__main__':
    print solve()
