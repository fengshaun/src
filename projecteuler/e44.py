from pylib.numbers import pentagonal
from pylib.numbers import is_pentagonal

def solve():
    for i in range(1, 3000):
        for j in range(i + 1, 3000):
            if (is_pentagonal(abs(pentagonal(i) - pentagonal(j))) and
                is_pentagonal(pentagonal(i) + pentagonal(j))):
                return abs(pentagonal(i) - pentagonal(j))

if __name__ == '__main__':
    print solve()
