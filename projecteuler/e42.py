from pylib.mathfn import triangle_nums

# the maximum score is 192
t = triangle_nums(200)

def score(name):
    s = 0
    for c in name:
        s += ord(c) - 64

    return s

def solve():
    data = open('words.txt', 'r').read().replace('"', '').split(',')
    scores = []
    n = 0

    for name in data:
        s = score(name)
        scores.append(s)

        if s in t:
            n += 1
         
    return n

if __name__ == '__main__':
    print solve()
