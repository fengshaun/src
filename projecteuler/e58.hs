import Data.Numbers.Primes

diagonals :: [Int]
diagonals = 1 : diag' 1 2
            where
              diag' :: Int -> Int -> [Int]
              diag' n m = [n + m
                          ,n + m*2
                          ,n + m*3
                          ,n + m*4] ++ diag' (n + m*4) (m + 2)

diagonalsOfSide :: Int -> [Int]
diagonalsOfSide n | even n = takeWhile (<= (n+1)^2) diagonals
                  | odd  n = takeWhile (<= n^2) diagonals

sideLength :: [Int] -> Int
sideLength xs = (last xs) - (last . init $ xs) + 1

addLayer :: [Int] -> ([Int], [Int])
addLayer xs = (xs ++ ys, filter isPrime ys)
              where
                a = last xs
                b = sideLength xs + 1
                ys = [a + b, a + b*2, a + b*3, a + b*4]

diagRatio :: (Fractional a) => [Int] -> [Int] -> a
diagRatio primeList diagList = fromIntegral (length primeList)
                             / fromIntegral (length diagList)

allRatios :: (Fractional a) => [(Int, a)]
allRatios = ratios' 3 (diagonalsOfSide 3) (filter isPrime . diagonalsOfSide $ 3)
            where
              -- ratios' :: SideLength -> ListOfAllDiagonals -> ListOfPrimeDiagonals -> (SideLength, Ratio)
              ratios' sideLen listOfDiagonals listOfPrimes = let newDiag = addLayer listOfDiagonals
                                                             in (sideLen, diagRatio listOfPrimes listOfDiagonals) :
                                                                ratios' (sideLen + 2) (fst newDiag) (listOfPrimes ++ snd newDiag)

--main = sequence_ [putStrLn (show a ++ " => " ++ show b) | (a, b) <- takeWhile (\(a, b) -> b >= 0.1)allRatios]
main = print . (+ 2) . fst . last . takeWhile (\(a, b) -> b >= 0.1) $ allRatios
