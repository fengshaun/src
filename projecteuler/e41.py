from pylib.prime import is_prime
from pylib.numbers import is_pandigital
from pylib.numbers import digits
from pylib.numbers import xpermutations

def solve():
    largest = 0
    for i in range(9, 1, -1):
        for p in xpermutations(range(i, 0, -1), i):
            if is_prime(int(''.join([str(x) for x in p]))):
                return int(''.join([str(x) for x in p]))

if __name__ == '__main__':
    print solve()
