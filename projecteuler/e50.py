from pylib.prime import is_prime, primes_max
import sys

ps = list(primes_max(1000000))

def sum_consecutive(p):
    for start in (2, 3):
        n = start
        acc = ps[n]
        while acc < p:
            n += 1
            acc += ps[n]

        if acc == p:
            return len([ps[i] for i in range(start, n + 1)])

    return 0

def solve():
    max_n = 0
    max = 0
    for p in ps[::-1]:
        num = sum_consecutive(p)
        if num > max:
            max_n = p
            max = num

    return max_n

if __name__ == '__main__':
    print solve()
