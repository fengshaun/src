# By starting at the top of the triangle below and moving to adjacent numbers 
# on the row below, the maximum total from top to bottom is 23.

#  3
#  7 4
#  2 4 6
#  8 5 9 3

# That is, 3 + 7 + 4 + 9 = 23.

# Find the maximum total from top to bottom in triangle.txt, a 15K text file
# containing a triangle with one-hundred rows.

cIndex = 0
sum = 0
currentList = []

with open("triangle.txt", "r") as f:
    currentList.append(int(f.readline()))
    for line in f:
        numList = [int(n) for n in line.split(" ")]

        numList[0] += currentList[0]
        for pos in xrange(1, len(numList) - 1):
            numList[pos] += max(currentList[pos - 1], currentList[pos])
        numList[-1] += currentList[-1]

        currentList = numList[:]
print max(currentList)
