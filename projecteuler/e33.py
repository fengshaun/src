from pylib import prime
import math

def digits(n):
    return [int(i) for i in str(n)]

def is_trivial(a, b):
    if digits(a)[-1] == digits(b)[-1] == 0:
        return True
    else: return False

def reduce_fraction(a):
    num = min(a)

    for i in xrange(2, num + 1):
        while a[0] % i == 0 and a[1] % i == 0:
            a[0] /= i
            a[1] /= i

    return a

def are_equal(a, b):
    # a and b are two tuples representing fractions
    if reduce_fraction(a) == reduce_fraction(b):
        return True
    else:
        return False

def is_good(a, b):
    if is_trivial(a, b):
        return False

    for i in range(2):
        for j in range(2):
            if digits(a)[i] == digits(b)[j]:
                x = digits(a)
                y = digits(b)
                x.remove(x[i])
                y.remove(y[j])

                if are_equal([a, b], [x[0], y[0]]):
                    return True
                else:
                    return False

    return False

def solve():
    numerator = 1
    denominator = 1

    for a in range(10, 100):
        for b in range(a + 1, 100):
            if is_good(a, b):
                numerator *= a
                denominator *= b

    x, y = reduce_fraction([numerator, denominator])
    return y

if __name__ == '__main__':
    print solve()
