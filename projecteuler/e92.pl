use List::Util qw/sum/;

my @arr;
$arr[0] = 0;
$arr[1] = 1;
$arr[89] = 89;

my $answer = 0;

for my $num (1..10000000) {
    print "$num\n";
    if (&numChainIs89($num)) {
        $answer++;
    }
}

print $answer;

sub numChainIs89 {
    my $num = shift;

    if (defined $arr[$num]) {
        if ($arr[$num] == 89) {
            return true;
        } elsif ($arr[$num] == 1) {
            return false;
        } else {
            return &numChainIs89($arr[$num]);
        }
    } else {
        $arr[$num] = &sumSquareDigits($num);
        return &numChainIs89($arr[$num]);
    }
}

sub sumSquareDigits {
    my $num = shift;

    my @l;
    for ("$num") {
        push @l, int($_) ** 2;
    }

    sum @l;
}
