import string
from datetime import datetime

from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from mysite.bmarks.models import Bmark, Tag, UserProfile

def api_addbmark(request):
    try:
        user = authenticate(keycode=request.POST["keycode"])
    except KeyError:
        try:
            user = authenticate(username=request.POST["username"], password=request.POST["password"])
        except KeyError:
            return HttpResponse("loginerror")

    if user is not None and user.is_active:
        login(request, user)
    else:
        return HttpResponse("loginerror")

    b = Bmark(user=user, name=request.POST["name"], link=request.POST["link"], create_date=datetime.now())
    b.save()
    return HttpResponse("success")

