from django.contrib.auth.models import User

class KeycodeAuthenticationBackend:
    def authenticate(self, keycode=None):
        if keycode is not None:
            try:
                return User.objects.get(profile__keycode=keycode)
            except User.DoesNotExist:
                return None
        else:
            return None

    def get_user(user_id):
        try:
            return User.objects.get(pk=user_id)
        except:
            return None
        
