from django.contrib import admin
from mysite.bmarks.models import Bmark, UserProfile, Tag
from django.contrib.auth.models import User

class UserProfileInline(admin.TabularInline):
    model = UserProfile

class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Basic",                       {"fields": ["first_name",
                                                    "last_name",
                                                    "email",
                                                    "username",
                                                    "password",
                                                    "last_login",
                                                    "date_joined"]}),
        ("Groups and Permissions",      {"fields": ["is_active",
                                                    "is_staff",
                                                    "is_superuser",
                                                    "groups",
                                                    "user_permissions"]})
    ]

    inlines = [UserProfileInline]
    list_display = ["username", "email"]

class BmarkAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Basic",                   {"fields": ["user",
                                                "name",
                                                "link"]}),
        ("Date information",        {"fields": ["create_date"]})
    ]

    list_display = ["name", "link", "user"]
    list_filter = ["user", "create_date"]

class TagAdmin(admin.ModelAdmin):
    list_filter = ["user"]

admin.site.register(Bmark, BmarkAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
