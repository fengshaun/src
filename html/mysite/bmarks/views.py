from datetime import datetime
import urllib2
import string

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout, load_backend
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.db import IntegrityError

from mysite.bmarks.models import Bmark, Tag, UserProfile
from mysite.bmarks import forms

@login_required
def index(request, tag=None):
    if not tag is None:
        if tag == "all":
            bmark_list = Bmark.objects.filter(user=request.user)
        elif tag == "untagged":
            bmark_list = [b for b in Bmark.objects.filter(user=request.user) if b.tag.count() == 0]
        else:
            bmark_list = Bmark.objects.filter(user=request.user).filter(tag__name=tag)
    else:
        bmark_list = Bmark.objects.filter(user=request.user)

    return render_to_response("bmarks/index.html",
                              {"object_list": bmark_list,
                               "keycode": request.user.get_profile().keycode,
                               "tags": Tag.objects.filter(user=request.user)})

@login_required
def add_or_logout(request):
    if "add" in request.GET:
        return redirect("/bmarks/add/")
    elif "logout" in request.GET:
        return redirect("/bmarks/logout/")

@login_required
def tools(request):
    return render_to_response("bmarks/tools.html",
                              {"keycode": request.user.get_profile().keycode})

@login_required
def addBmark(request):
    if request.method == "POST":
        form = forms.AddForm(request.POST)
        if not form.is_valid():
            return render_to_response("bmarks/addbmark.html",
                                      {"form": form})

        b = Bmark(user=request.user, name=request.POST["name"], link=request.POST["link"], create_date=datetime.now())
        b.save()

        if request.POST["tags"]:
            tags = map(string.strip, request.POST["tags"].split(","))

            for tag in tags:
                try:
                    t = Tag.objects.filter(user=request.user).get(name=tag)
                    b.tag.add(t)
                except Tag.DoesNotExist:
                    b.tag.create(user=request.user, name=tag)

        b.save()
        return redirect("/bmarks/")
    else:
        # wanting to fill a form
        form = forms.AddForm()
        return render_to_response("bmarks/addbmark.html",
                                  {"form": form})

@login_required
def editBmark(request):
    if request.method == "POST":
        form = forms.EditForm(request.POST)
        if not form.is_valid():
            return render_to_response("bmarks/editbmark.html",
                                      {"form": form})

        # teh editing!
        b = Bmark.objects.filter(user=request.user).filter(name=request.POST["old_name"])[0]
        b.name = request.POST["name"]
        b.link = request.POST["link"]
        
        current_tags = set([tag.name for tag in b.tag.all()])

        if request.POST["tags"]:
            new_tags = set(map(string.strip, request.POST["tags"].split(",")))
        else:
            new_tags = set([])

        for tag in current_tags - new_tags:
            # tags that are removed
            t = Tag.objects.filter(user=request.user).get(name=tag)
            b.tag.remove(t)

        for tag in new_tags - current_tags:
            # tags that are added
            try:
                t = Tag.objects.filter(user=request.user).get(name=tag)
                b.tag.add(t)
            except Tag.DoesNotExist:
                b.tag.create(user=request.user, name=tag)

        b.save()

        return redirect("/bmarks/")

    else:
        form = forms.EditForm(initial={"name": request.GET["name"],
                                       "link": request.GET["link"],
                                       "tags": ",".join([tag.name for tag in Tag.objects.filter(user=request.user).filter(bmark__name=request.GET["name"])])})
        return render_to_response("bmarks/editbmark.html",
                                  {"form": form,
                                   "bmark": request.GET["name"]})

@login_required
def removeBmark(request):
    if request.method == "POST":
        request.user.bmark_set.filter(name=request.POST["name"]).delete()

    return redirect("/bmarks/")

@login_required
def removeTag(request):
    if request.method == "POST":
        Tag.objects.filter(user=request.user).filter(name=request.POST["tag"]).delete()

    return redirect("/bmarks/")

def register(request):
    if request.method == "POST":
        # the user has just registered
        form = forms.RegisterForm(request.POST)
        if not form.is_valid():
            return render_to_response("bmarks/register.html",
                                      {"form": form})

        if form.cleaned_data["password"] != form.cleaned_data["password_repeat"]:
            return render_to_response("bmarks/register.html",
                                      {"form": form,
                                       "custom_errors": ["Passwords don't match"]})

        try:
            user = User.objects.create_user(username=form.cleaned_data["username"],
                                            email=form.cleaned_data["email"],
                                            password=form.cleaned_data["password"])
        except IntegrityError:
            return render_to_response("bmarks/register.html",
                                      {"form": form,
                                       "custom_errors": ["Username already taken"]})

        user.first_name = form.cleaned_data["first_name"]
        user.last_name = form.cleaned_data["last_name"]

        # prevent keycode duplication
        while 1:
            try:
                code = User.objects.make_random_password(length=10)
                User.objects.get(profile__keycode=code)
            except User.DoesNotExist:
                break

        profile = UserProfile(user=user, keycode=code)
        profile.save()
        user.save()

        return redirect("/bmarks/")
    else:
        form = forms.RegisterForm()
        return render_to_response("bmarks/register.html", 
                                  {"form": form})

def login_view(request):
    if request.method == "POST":
        if "register" in request.POST:
            return redirect("/bmarks/register/")

        form = forms.LoginForm(request.POST)
        if not form.is_valid():
            return render_to_response("bmarks/login.html",
                                      {"form": form})

        user = authenticate(username=form.cleaned_data["username"], password=form.cleaned_data["password"])
        if user is not None and user.is_active:
            login(request, user)
            return redirect(request.POST["next"])
        else:
            return render_to_response("bmarks/login.html",
                                      {"form": form,
                                       "custom_errors": ["Username and password do not match"]})
    else:
        form = forms.LoginForm()
        return render_to_response("bmarks/login.html", 
                                  {"form": form,
                                   "next": (request.GET["next"] if "next" in request.GET else "") or "/bmarks/"})

def logout_view(request):
    logout(request)
    return render_to_response("bmarks/logout.html")

