from django.conf.urls.defaults import *
from mysite.bmarks.models import Bmark
from django.http import HttpResponse

info_dict = {
    'queryset': Bmark.objects.all(),
}

def api(request):
    return HttpResponse("Not Implemented")

urlpatterns = patterns('mysite.bmarks.views',
    (r'^$', 'index'),
    (r'^tools/$', 'tools'),
    (r'^api/$', lambda x: HttpResponse("Here goes the doc about the api")),
    (r'^addorlogout/$', 'add_or_logout'),
    (r'^register/$', 'register'),
    (r'^login/$', 'login_view'),
    (r'^logout/$', 'logout_view'),
)

urlpatterns += patterns('mysite.bmarks.views',
    (r'^add/$', 'addBmark'),
    (r'^edit/$', 'editBmark'),
    (r'^remove/$', 'removeBmark'),
)

urlpatterns += patterns('mysite.bmarks.views',
    (r'^tags/(?P<tag>.+)/$', 'index'),
    (r'^remove/tag/$', 'removeTag'),
)

urlpatterns += patterns('mysite.bmarks.apiviews',
    (r'^api/add/$', 'api_addbmark'),
)
