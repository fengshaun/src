from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(error_messages={"required": "Enter your username"})
    password = forms.CharField(widget=forms.PasswordInput,
                               error_messages={"required": "Enter your password"})

class RegisterForm(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)

    username = forms.CharField(label="*Username",
                               error_messages={"required": "Enter your desired username"})
    password = forms.CharField(label="*Password",
                               widget=forms.PasswordInput,
                               error_messages={"required": "Enter a password"})
    password_repeat = forms.CharField(label="*Password (repeat)",
                                      widget=forms.PasswordInput,
                                      error_messages={"required": "Repeat your password"})
    email = forms.EmailField(label="*Email",
                             error_messages={"required": "Enter your email"})

class AddForm(forms.Form):
    name = forms.CharField(error_messages={"required": "Enter the name of the link"})
    link = forms.URLField(error_messages={"required": "Enter the url of the page"})
    tags = forms.CharField(required=False)

class EditForm(AddForm):
    pass
