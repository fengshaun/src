from django.contrib.auth.models import User
from django.db import models

class Bmark(models.Model):
    user = models.ForeignKey(User)
    
    name = models.CharField(max_length=200)
    link = models.CharField(max_length=300)
    create_date = models.DateTimeField("date created")

    def __unicode__(self):
        return self.name

class Tag(models.Model):
    bmark = models.ManyToManyField(Bmark, related_name="tag")
    user = models.ForeignKey(User)

    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True, related_name="profile")

    keycode = models.CharField(max_length=10)

    def __unicode__(self):
        return '%s - %s' % (self.user, self.keycode)
