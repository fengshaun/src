#import os
#import sys
#
#path = '/home/armin/public_html/mysite'
#if path not in sys.path:
#    sys.path.append(path)
#
#os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
#
#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()

def application(environ, start_response):
    status = '200 OK'
    output = 'Hello World!'

    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
                        start_response(status, response_headers)

    return [output]
