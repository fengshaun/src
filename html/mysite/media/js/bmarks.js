$(document).ready(function() {
	$(".tag-image-delete").hide();
	$(".bmark-image-delete").hide();
	$(".bmark").css("padding-left", "30px");
	$(".bmark-edit-image").hide();
	$(".tag-filter").draggable();
})

function help() {
	alert("drag & drop this button to your bookmark bar");
	return false;
}

function editBmarks() {
	if ($(".bmarks-header-edit").text() == "edit") {
		$(".bmarks-header-edit").text("done");
		$(".content li:visible .bmark-edit-image").fadeIn("fast");
		$(".bmark").css("padding-left", "0px");
	} else {
		$(".bmarks-header-edit").text("edit");
		$(".bmark-edit-image").hide();
		$(".bmark").css("padding-left", "30px");
	}
}

function removeBmark(id, bmark) {
	$.ajax({
			type: "POST",
			url: "/bmarks/remove/",
			data: {name: bmark},
			success: function() {
				$("#" + id).hide("slow");
			},
			error: function() {
				alert("An error occurred!");
			}
			});
}

function editTags() {
	if ($(".tags-header-edit").text() == "edit") {
		$(".tags-header-edit").text("don");
		$("td:visible img.tag-image-delete").fadeIn("fast");
		$(".tag").css("padding-left", "0px");
		$(".tag-image-delete").css("padding-left", "15px");
	} else {
		$(".tags-header-edit").text("edit");
		$(".tag-image-delete:visible").hide()
		$(".tag").css("padding-left", "25px");
	}
}

function removeTag(id, tag) {
	$.ajax({
		type: "POST",
		url: "/bmarks/remove/tag/",
		data: {tag: tag},
		success: function() {
			$("#" + id).fadeOut("fast");
		},
		error: function() {
			alert("An error occurred!");
		}
		});
}
