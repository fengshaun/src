from django.conf.urls.defaults import *
from django.conf import settings
from django.shortcuts import redirect

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('mysite.polls.views',
    (r"^$", lambda x: redirect("/bmarks")),
    (r"^polls/", include("mysite.polls.urls")),
    (r"^bmarks/",include("mysite.bmarks.urls")),
    (r"^admin/", include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/armin/public_html/mysite/media/'}),
    )
