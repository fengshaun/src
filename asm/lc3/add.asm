; prompts for two numbers and adds them together
.orig x3000
ld    r6, temp

; get inputs and save the result in r3
trap  x23       ; input from keyboard into r0
add   r1,r0,x0
trap  x23
add   r2,r0,r6  ; convert to int.  only one compensation is desired
add   r3,r1,r2

; print the result
lea   r0,mesg
trap  x22       ; "puts" outputs a string starting at r0 until x0
add   r0,r3,x0  ; move the sum to r0, to be output
trap  x21       ; display the sum

; end
halt
mesg .stringz "the sum of those two numbers is "
temp .fill #-48
.end
