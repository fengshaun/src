; prints only uppercase character in a string
.orig x3000
ld    r3,upper                          ;load uppercase bit mask  x0020
lea   r1,string                      ;pointer into string
load  ldr r0,r1,#0              ;get a character
brz   end                                  ;x00 means end of string
and   r2,r0,r3                         ;test for uppercase with mask
brp   next                               ;not uppercase, get the next
out                                          ;output the character
next  add r1,r1,#1             ;advance pointer
brnzp load                           ;go get the next character
end   halt
upper .fill x0020 ; bit 6 determines upper/lower case
string .stringz "This Is a Sample String"
.end
