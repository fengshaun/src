; finds the largest number among the numbers
; and prints it
.orig x3000

        ld    r0,length     ; number of numbers to check (r0 <- 6)
        lea   r1,data       ; pointer to data (ld r1 with the data address)
largest ldr   r2,r1,#0      ; new largest number
        not   r3,r2         ; make negative of largest
        add   r3,r3,#1
next    add   r0,r0,#-1     
        brz   end
        add   r1,r1,#1      ; move to next
        ldr   r4,r1,#0      ; get next
        add   r5,r3,r4      ; compare
        brnz  next          ; not larger, get next
        brnzp largest       ; save new largest

end     lea   r0,res
        trap  x22           ; puts
        ldr   r0,r2,#0
        trap  x21           ; display result
        halt
length  .fill #6
res     .stringz "The largest is "
data    .fill x0010
        .fill x0004
        .fill x0023
        .fill x001b
        .fill x000c
        .fill x0011
        .end

