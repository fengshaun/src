#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv)
{
    /* udp:514 */
    struct addrinfo hints;
    struct addrinfo *resaddr;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = 0;

    int ret;
    if ((ret = getaddrinfo("127.0.0.1", "12345", &hints, &resaddr)) != 0)
        fprintf(stderr, "getaddrinfo error: %s", gai_strerror(ret));

    int sock;
    if ((sock = socket(resaddr->ai_family, resaddr->ai_socktype, resaddr->ai_protocol)) == -1)
        perror("socket");

    if (connect(sock, resaddr->ai_addr, resaddr->ai_addrlen) == -1)
        perror("connect");

    close(STDOUT_FILENO);
    dup2(sock, STDOUT_FILENO);

    setbuf(stdout, NULL);
    printf("hello world from dup'ed test\n");
    return 0;
}
