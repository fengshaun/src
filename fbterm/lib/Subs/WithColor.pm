package Subs::WithColor;
use Devel::Declare;

#
# Usage:
# with_color 'blue' {
#   ...do stuff...
# }
#

sub import {
    my $class = shift;
    my $caller = caller;

    Devel::Declare->setup_for(
        $caller,
        { with_color => { const => \&parser } },
    );

    no strict 'refs';
    *{$caller . '::with_color'} = sub (&) {};
}

our ($Declarator, $Offset);

sub parser {
    local ($Declarator, $Offset) = @_;

    skipdeclarator;
}

