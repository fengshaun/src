package FacebookGraph;
use Moose;

use strict;
use warnings;

use JSON;
use HTTP::Request;
use LWP::UserAgent;
use URI;

has 'access_token' => (is => 'rw');
has 'graph_url' => (is => 'ro', default => 'https://graph.facebook.com/');
has 'fql_url' => (is => 'ro', default => 'https://api.facebook.com/method/fql.query');

sub make_request {
	# type is GET/POST/DELETE/etc.
	# whatever HTTP::Request supports
	my ($self, $type, $uri) = @_;

	my $ua = LWP::UserAgent->new("fbcurse/0.1");
	my $req = HTTP::Request->new($type => $uri);
	my $resp = $ua->request($req);
	JSON::decode_json($resp->decoded_content);
}

sub get {
	my ($self, %args) = @_;
	my $limit = $args{limit} // 30;
	my $connection = join '/', @{ $args{connection} };

	my $uri = URI->new($self->graph_url . join('/', $connection));
	$uri->query_form(limit => $limit, access_token => $self->access_token);

	$self->make_request(GET => $uri->as_string);
}

sub put {
	my ($self, $parent_id, $connection, $args) = @_;

	my $uri = URI->new($self->graph_url . join('/', ($parent_id, $connection)));
	$args->{access_token} = $self->access_token;
	$uri->query_form($args);

	$self->make_request(POST => $uri->as_string);
}

sub fql {
	my ($self, $query) = @_;

	my $uri = URI->new($self->fql_url);
	$uri->query_form(format => "JSON",
					 access_token => $self->access_token,
					 query => $query,
	);
						
	$self->make_request(GET => $uri->as_string);
}

no Moose;
1;

