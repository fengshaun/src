package MainWindow;

use 5.010;
use strict;
use warnings;

use lib '/home/armin/fbterm/lib';
use base qw/Window/;
use UI::Post;

use Curses;
use Data::Dumper;
use List::Util qw/max/;
use Term::ReadKey qw/GetTerminalSize/;
use Text::Wrap;
use Log::Log4perl qw/:easy/;
use Try::Tiny;

$Text::Wrap::separator = "|";
Log::Log4perl->easy_init({ file => 'log', level => $DEBUG });

sub debug {
    print STDERR $_[0] . "\n";
}

sub new {
    my ($class, %opts) = @_;
    my $self = bless {
        cur_row => 1,                                                       # to keep track of current row for selection
        cur_page => 0,                                                      # what page are we on
        last_row => 1,                                                      # the maximum lines we have printed
        last_post_row => 1,                                                 # the last row that is also the beginning of a post
        col1_start => 2,                                                    # left-most column to start writing
        col2_start => 0,                                                    # the start of second column
        col1_width => 1,                                                    # width of the first column
        msg_width => 1,                                                     # width of the message on second column
        rows => [],                                                         # stores information about rows
        posts => [],                                                        # all the available posts
        post_num => 0,                                                      # current post number
        pages => [],                                                        # keeps track of pages of posts
        cur_post => 0,                                                      # the currently focused post
        data_cache => [],                                                   # caches data for faster screen updates
    }, $class;

    $self->{conf} = \%opts;
    $self->set_win(newwin((GetTerminalSize)[1], (GetTerminalSize)[0], 0, 0));
    $self->set_colors($opts{colors});
    $self->set_title("Fbterm -- Home");

    $self->color($self->{conf}{colors}{border});
    $self->SUPER::refresh;
    $self->color_reset;

    $self;
}

sub add_posts {
    debug "adding posts";
    my ($self, $data) = @_;
    return unless @$data;

    $self->{data_cache} = $data;

    $self->{posts} = [];
    $self->{pages} = [];

    my $row = 1;
    my $page = 0;

    # getting all the needed dimensions
    my $term_width = (GetTerminalSize)[0];
    $self->{col1_width} = $self->{col1_start} + max(map { length $_->{from}{name} } @{ $data }) + 1;
    $self->{col2_start} = $self->{col1_start} + $self->{col1_width} - 1;
    $self->{msg_width} = $term_width - $self->{col1_width} - 13; # - 13 to account for the padding and 'type' in front of the message

    # add the posts
    for (@{ $data }) {
        # id of the post is the index of it in the $self->{posts} list.
        say STDERR "adding post with id: " . scalar @{ $self->{posts} };

        if ($row >= (GetTerminalSize)[1] - 1) { $page++; $row = 1; }
        say STDERR "\t\$row = $row\n\t\$page = $page";

        my $post = Post->new(id => scalar @{ $self->{posts} }, data => $_, msg_length => $self->{msg_width}, page => $page, row => $row);
        push @{ $self->{pages}[$page] }, $post;
        push @{ $self->{posts} }, $post;
        $row += ($post->lines + 1);
    }

    $self->{cur_page} = 0;
    $self->{post_num} = 0;

    $self->clear_arrow;
    $self->draw_posts(@{ $self->{pages}[$self->{cur_page}] });
    $self->draw_arrow;
    $self->refresh;
}

sub add_posts_from_cache {
    my $self = shift;
    $self->add_posts($self->{data_cache});
}

sub current_post {
    my $self = shift;
    $self->{posts}[$self->{post_num}];
}

sub current_page {
    my $self = shift;
    $self->{pages}[$self->{cur_page}];
}

sub draw_posts {
    say STDERR "drawing posts";
    my ($self, @posts) = @_;

    for (@posts) {
        $self->draw_post($_);
    }
}

sub draw_post {
    my ($self, $post) = @_;
    my $row = $post->row;

    return if ($row > (GetTerminalSize)[1] - 2);

    my @parts = @{ $post->message_parts };

    # print the name
    $self->color($self->{conf}{colors}{name}); $self->{win}->addstr($row, $self->{col1_start}, sprintf("%@{[ $self->{col1_width} - 2 ]}s", $post->name)); $self->color_reset;

    # the first line of the message should have the type of the message, with the right color
    $self->{win}->move($row, $self->{col2_start});
    $self->color($self->{conf}{colors}{type}); $self->{win}->addstr(sprintf "%-6.6s:", $post->type); $self->color_reset;
    $self->{win}->addstr(" " . shift @parts);

    $row++;

    $self->color($self->{conf}{colors}{message});
    # and then comes the rest of the lines
    for (@parts) {
        $self->{win}->addstr($row, $self->{col2_start}, " " x 8 . $_);
        $row++;
    }
    $self->color_reset;
}

sub open_dialog {
    my $self = shift;
    $self->current_post->open_dialog(with_color => $self->{conf}{colors});
}

sub go_to_post {
    my $self = shift;
    $self->current_post->open_browser;
}

sub go_to_link {
    my $self = shift;
    $self->current_post->open_link;
}

sub draw_arrow {
    my $self = shift;
    $self->color($self->{conf}{colors}{arrow}); $self->{win}->addstr($self->current_post->row, 1, ">"); $self->color_reset;
}

sub clear_arrow {
    my $self = shift;
    $self->{win}->addstr($self->current_post->row, 1, " ");
}

sub next_post {
    my $self = shift;
    $self->change_post(1);
}

sub prev_post {
    my $self = shift;
    $self->change_post(-1);
}

sub change_post {
    my ($self, $inc) = @_;
    say STDERR "attempting to change post";

    $self->clear_arrow;

    my $old_post_num = $self->{post_num};

    # illegal move, out of bound?
    if ($self->{post_num} <= 0 && $inc < 0) {
        # first post, do nothing
        say STDERR '$self->{post_num} <= 0 && $inc < 0';
    } elsif ($self->current_post == $self->{posts}[-1] && $inc > 0) {
        # it's the last post, do nothing
        say STDERR '$self->current_post->row == $self->{posts}[-1]->row && $inc > 0'
    } else {
        if ($inc < 0 && $self->current_post == $self->current_page->[0]) {
            $self->prev_page;
            $self->{post_num} = $self->current_page->[-1]->id;
        } elsif ($inc > 0 && $self->current_post == $self->current_page->[-1]) {
            $self->next_page;
            $self->{post_num} = $self->current_page->[0]->id;
        } else {
            $self->{post_num} += $inc;
        }
    }

    $self->draw_arrow;
    $self->refresh;

    say STDERR "\t\$post_num: @{[ $self->{post_num} ]}\n\t\$page = @{[ $self->{cur_page} ]}";
}

sub next_page {
    my $self = shift;
    say STDERR "next page";
    $self->change_page(1);
}

sub prev_page {
    my $self = shift;
    say STDERR "prev page";
    $self->change_page(-1);
}

sub change_page {
    my ($self, $inc) = @_;
    say STDERR "changing page $inc";
    $self->{cur_page} += $inc;

    $self->clear;
    $self->draw_posts(@{ $self->current_page });
    if ($inc > 0 && $inc < 0) {
        # went next page, should start from first post
        $self->{post_num} = $self->current_page->[0]->id;
    } else {
        # going prev page, should start from last post
        $self->{post_num} = $self->current_page->[-1]->id;
    }
    $self->refresh;
}

# redefining refresh and clear here!
no warnings;
sub refresh {
    my $self = shift;

    $self->{win}->vline(1, $self->{col1_width}, ACS_VLINE, (GetTerminalSize)[1] - 2);
    $self->SUPER::refresh;
}

sub clear {
    my ($self, $refresh_ok) = @_;

    $self->SUPER::clear;
    $self->clear_arrow;
    $self->refresh if $refresh_ok;
}
use warnings;

sub redraw {
    my $self = shift;

    $self->{win}->clear;
    $self->{win}->resize((GetTerminalSize)[1], (GetTerminalSize)[0]);
    $self->add_posts_from_cache;
}

sub add_header {
    my $self = shift;
    $self->{win}->addstr(0, 2, '[ Fbterm -- Home ]');
}

1;
