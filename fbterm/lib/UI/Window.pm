package Window;

use strict;
use warnings;

use Curses;
use Try::Tiny;
use Carp;

our %colors = (
    default => 0,
    red     => 1,
    green   => 2,
    yellow  => 3,
    blue    => 4,
    magenta => 5,
    cyan    => 6,
    white   => 7,
    black   => 8,
);

sub new {
    my $class = shift;

    my $self = bless {
        title => "",
        colors => \%colors,
        win => undef,
    }, $class;

    $self;
}

sub color {
    my ($self, $color) = @_;
    $self->{win}->attrset(COLOR_PAIR($colors{$color} // 0));
} 
 
sub color_reset {
    my $self = shift;
    $self->{win}->attrset(COLOR_PAIR $colors{default});
}

no warnings;
sub refresh {
    my $self = shift;

    $self->color($self->{colors}{border});
    $self->{win}->box(ACS_VLINE, ACS_HLINE);
    $self->{win}->addstr(0, 1, "[ @{[ $self->{title} ]} ]");
    $self->color_reset;
    $self->{win}->refresh;
}

sub clear {
    my $self = shift;
    $self->{win}->clear;
}
use warnings;

sub set_win { $_[0]->{win} = $_[1] }
sub set_colors { $_[0]->{colors} = $_[1] }
sub set_title { $_[0]->{title} = $_[1] }

1;
