package Post;

use strict;
use warnings;

use Data::Dumper;
use Text::Wrap;
$Text::Wrap::separator = "|";

use lib '/home/armin/fbterm/lib';
use UI::Dialog;
use FacebookGraph;

sub new {
    my ($class, %opts) = @_;

    my $self = bless {
        id => $opts{id},

        url => $opts{data}{actions}[0]{link},
        link => $opts{data}{link},
        name => $opts{data}{from}{name},
        message => $opts{data}{message},
        message_parts => [],
        type => $opts{data}{type},
        is_focused => 0,
        row => $opts{row},
        page => $opts{page},

        attach_type => $opts{data}{type} ne 'status' ? $opts{data}{type} : undef, 
        attach_name => $opts{data}{name} // "",
        attach_link => $opts{data}{link},

        comments => $opts{data}{comments}{data} // [],
        likes => $opts{data}{likes},
        target => $opts{data}{to}{data}[0]{name},
    }, $class;

    $self->prepare_message($opts{msg_length});
    $self;
}

sub prepare_message {
    my ($self, $length) = @_;
    $Text::Wrap::columns = $length;

    $self->{message} = $self->{message} // "<no message attached>";

    $self->{message} =~ s/\n/ /g;
    $self->{message} =~ s/\|\s*//g;

    # here we compensate for the addition of 'type' in front of the message
    $self->{message_parts} = [split /\|/, wrap(' ' x length $self->{type}, '', $self->{message})];
    $self->{message_parts}[0] =~ s/^\s+//;
}

sub open_dialog {
    my ($self, %opts) = @_;
    my $dialog = Dialog->new($self, $opts{with_color});
    $dialog->show;
}

sub open_browser {
    my $self = shift;
    system "uzbl-browser @{[ $self->url ]} &> /dev/null &";
}

sub open_link {
    my $self = shift;
    system "uzbl-browser @{[ $self->link ]} &> /dev/null &";
}

sub lines {
    my $self = shift;
    scalar @{ $self->{message_parts} };
}

sub focus {
    my $self = shift;
    $self->{is_focused} = 1;
}

sub blur {
    my $self = shift;
    $self->{is_focused} = 0;
}

sub name {
    my $self = shift;
    $self->{name};
}

sub id {
    $_[0]->{id};
}

sub url {
    my $self = shift;
    $self->{url};
}

sub type {
    my $self = shift;
    $self->{type};
}

sub message_parts {
    my $self = shift;
    $self->{message_parts};
}

sub is_focused {
    my $self = shift;
    $self->{is_focused};
}

sub row {
    my $self = shift;
    $self->{row};
}

sub page {
    $_[0]->{page};
}

sub message {
    my $self = shift;
    $self->{message};
};

sub attach_name {
    $_[0]->{attach_name};
}

sub attach_type {
    $_[0]->{attach_type};
}

sub attach_link {
    $_[0]->{attach_link};
}

sub comments {
    @{ $_[0]->{comments} };
}

sub target {
    $_[0]->{target};
}

sub link {
    $_[0]->{link};
}

sub likes {
    $_[0]->{likes};
}

1;
