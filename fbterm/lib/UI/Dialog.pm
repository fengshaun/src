package Dialog;

use strict;
use warnings;

use lib '/home/armin/fbterm/lib';
use base qw/Window/;

use Curses;
use List::Util qw/max/;
use Term::ReadKey qw/GetTerminalSize/;
use Text::Capitalize;
use Text::Wrap qw/wrap/;
$Text::Wrap::separator = "|";

sub new {
    my ($class, $post, $colors) = @_;

    my $width =  max(length($post->message_parts->[0]) + 5, (GetTerminalSize)[0] / 2);
    my $height = (GetTerminalSize)[1] / 2;
    my $x = ((GetTerminalSize)[0] - $width) / 2;
    my $y = (GetTerminalSize)[1] / 4;

    $Text::Wrap::columns = $width - 2;

    my $self = bless {
        post => $post,
        colors => $colors,
        width => $width,
        height => $height,
    }, $class;

    $self->set_win(newwin($height, $width, $y, $x));
    $self->set_title("Fbterm -- Post");
    $self->set_colors($colors);
    $self->refresh;
    $self;
}

sub show {
    my $self = shift;
    $self->draw_content;
    $self->refresh;
}

sub draw_content {
    my $self = shift;

    my $wrote_or_posted = $self->{post}->type eq 'status' ? ' wrote': ' posted a ' . $self->{post}->type;
    my $to_or_for = $wrote_or_posted =~ /wrote/ ? " to " : " for ";

    my $header = $self->{post}->name . $wrote_or_posted;
    $header .= $to_or_for . $self->{post}->target if $self->{post}->target;

    $self->color($self->{colors}{name}); $self->{win}->addstr(1, 1, $header); $self->color_reset;

    my $row = 2;

    $self->color($self->{colors}{message});

    # map is used to remove the post typo (e.g. video, link, etc.) from
    # the start of the post
    for (map { s/^\w+:\s*//; $_ } @{ $self->{post}->message_parts }) {
        $self->{win}->addstr($row, 1, $_);
        $row++;
    }

    # information about the attached whatever, if present
    if ($self->{post}->type ne 'status') {
        my $title = length($self->{post}->attach_name) > $self->{width} - 9 ? substr($self->{post}->attach_name, 0, $self->{width} - 12) . '...'
                                                                            : $self->{post}->attach_name;
        my $link  = length($self->{post}->attach_link) > $self->{width} - 9 ? substr($self->{post}->attach_link, 0, $self->{width} - 12) . '...'
                                                                            : $self->{post}->attach_link;
        $row++;     # one blank line for clarity
        $self->color($self->{colors}{title}); $self->{win}->addstr($row, 1, capitalize $self->{post}->type . " information"); $self->color_reset;
        $row++;
        $self->color($self->{colors}{message});
        $self->{win}->addstr($row, 1, "Title: " . $title); $row++;
        $self->{win}->addstr($row, 1, "Link:  " . $link ); $row++;
        $self->color_reset;
    }

    $row++;

    # the likes count:
    $self->{win}->move($row, 1);
    $self->color($self->{colors}{title}); $self->{win}->addstr("Likes: "); $self->color_reset;
    $self->color($self->{colors}{message}); $self->{win}->addstr($self->{post}->likes // 0); $self->color_reset;

    $row++;
    
    # and the comments:
    $self->color($self->{colors}{title}); $self->{win}->addstr($row, 1, "Comments:"); $self->color_reset; $row++;
    if (! scalar $self->{post}->comments) {
        $self->{win}->addstr($row, 1, "No comments");
    } else {
        for ($self->{post}->comments) {
            last if $row > $self->{height} - 2;

            my $msg = $_->{message};
            my $len = $self->{width} - length($_->{from}{name}) - 8;

            $msg =~ s/\n/ /g;
            $msg = substr($msg, 0, $len) . "..." if length $msg > $len;

            $self->{win}->move($row, 1);

            $self->color($self->{colors}{name});
            $self->{win}->addstr($_->{from}{name});
            $self->color_reset;
            $self->{win}->addstr(": ");
            $self->color($self->{colors}{message});
            $self->{win}->addstr($msg);
            $self->color_reset;
            $row++;
        }
    }

    $self->color($self->color_reset);
}

no warnings;
sub refresh {
    my $self = shift;
    $self->SUPER::refresh;
}
use warnings;

1;
