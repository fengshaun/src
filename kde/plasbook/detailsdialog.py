# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Plasbook.  If not, see <http://www.gnu.org/licenses/>.

import copy

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from debug import debug
import post

class DetailsDialog(QDialog):
    def __init__(self, rawPost):
        QDialog.__init__(self)

        self.rawPost = copy.deepcopy(rawPost)
        
        self.setupUi()
        debug('POSTID: %s' % self.rawPost.post_id)

    def addComment(self):
        pass

    def sendComment(self):
        pass
    
    def addLike(self):
        pass

    def setupUi(self):
        self.layout = QVBoxLayout()

        # the layout showing the poster and the text
        self.topLayout = QHBoxLayout()
        self.topLeftLayout = QVBoxLayout()

        self.topLeftLayout.addWidget(post.PosterIcon(self.rawPost.user.profilePic,
                                                     self.rawPost.user.name,
                                                     self.rawPost.user.profileUrl))
        self.topLeftLayout.addWidget(QLabel(self.rawPost.user.name))

        self.topLayout.addLayout(self.topLeftLayout)

        textLabel = QLabel('%s\n%s' % (self.rawPost.message,
                                       self.rawPost.extendedText))
        textLabel.setWordWrap(True)
        self.topLayout.addWidget(textLabel)

        self.layout.addLayout(self.topLayout)


        # showing the comments and everything else
        self.commentsGroupBox = QGroupBox('Comments (%s)' % self.rawPost.commentsCount)
        self.commentsGroupBoxLayout = QHBoxLayout()
        self.commentsLayout = QVBoxLayout()

        for rawComment in self.rawPost.comments:
            self.commentsLayout.addWidget(post.Comment(rawComment))

        self.layout.addWidget(self.commentsGroupBox)


        # and three buttons to add comment, like, and close
        self.addCommentButton = QPushButton('Comment')
        self.addCommentButton.clicked.connect(self.addComment)
        self.addLikeButton = QPushButton('Like (%s)' % self.rawPost.likesCount)
        self.addLikeButton.clicked.connect(self.addLike)
        self.closeButton = QPushButton('Close')
        self.closeButton.clicked.connect(self.close)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.addCommentButton)
        self.bottomLayout.addWidget(self.addLikeButton)
        self.bottomLayout.addStretch()
        self.bottomLayout.addWidget(self.closeButton)

        self.layout.addLayout(self.bottomLayout)
        self.setLayout(self.layout)
