# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import sys
import inspect

DEBUG = 1

def debug(message):
    if DEBUG:
        filename = inspect.stack()[1][1].split('/')[-1]
        line = inspect.stack()[1][2]
        sys.stdout.write('(%s, %s): %s\n' % (filename,
                                             line,
                                             message))
        sys.stdout.flush()
