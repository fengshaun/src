import webbrowser

from PyQt4.QtGui import QDialog
from PyQt4.QtCore import *
from ui_registerdialog import Ui_RegisterDialog

from fbuser import FbUser

class RegisterDialog(QDialog):
    def __init__(self):
        QDialog.__init__(self)

        self.ui = Ui_RegisterDialog()
        self.ui.setupUi(self)
        self.ui.stackWidget.setCurrentIndex(0)

        self.connect(self.ui.nextButton, SIGNAL('clicked()'), self.openRequest)
        self.connect(self.ui.nextButton, SIGNAL('clicked()'), self.nextPage)
        self.connect(self.ui.finishButton, SIGNAL('clicked()'), self.finish)
        self.connect(self.ui.cancelButton, SIGNAL('clicked()'), self.reject)
        self.connect(self.ui.cancelButton2, SIGNAL('clicked()'), self.reject)
        self.connect(self.ui.prevButton, SIGNAL('clicked()'), self.prevPage)

    def openRequest(self):
        webbrowser.open('http://www.facebook.com/code_gen.php?v=1.0&api_key=%s&next=http://facebook.com/' % FbUser.API_KEY)

    def nextPage(self):
        self.ui.stackWidget.setCurrentIndex(self.ui.stackWidget.currentIndex() + 1)

    def prevPage(self):
        self.ui.stackWidget.setCurrentIndex(self.ui.stackWidget.currentIndex() - 1)

    def finish(self):
        self.code = self.ui.codeLineEdit.text()
        self.accept()
