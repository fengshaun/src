# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'registerdialog.ui'
#
# Created: Tue Aug 11 21:11:50 2009
#      by: PyQt4 UI code generator 4.5.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_RegisterDialog(object):
    def setupUi(self, RegisterDialog):
        RegisterDialog.setObjectName("RegisterDialog")
        RegisterDialog.resize(331, 142)
        self.horizontalLayout_2 = QtGui.QHBoxLayout(RegisterDialog)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.stackWidget = QtGui.QStackedWidget(RegisterDialog)
        self.stackWidget.setObjectName("stackWidget")
        self.page = QtGui.QWidget()
        self.page.setObjectName("page")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.page)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtGui.QLabel(self.page)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.prevButton1 = QtGui.QPushButton(self.page)
        self.prevButton1.setEnabled(False)
        self.prevButton1.setObjectName("prevButton1")
        self.horizontalLayout.addWidget(self.prevButton1)
        self.nextButton = QtGui.QPushButton(self.page)
        self.nextButton.setObjectName("nextButton")
        self.horizontalLayout.addWidget(self.nextButton)
        self.cancelButton = QtGui.QPushButton(self.page)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.stackWidget.addWidget(self.page)
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName("page_2")
        self.verticalLayout = QtGui.QVBoxLayout(self.page_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtGui.QLabel(self.page_2)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.codeLineEdit = QtGui.QLineEdit(self.page_2)
        self.codeLineEdit.setObjectName("codeLineEdit")
        self.verticalLayout.addWidget(self.codeLineEdit)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.prevButton = QtGui.QPushButton(self.page_2)
        self.prevButton.setObjectName("prevButton")
        self.horizontalLayout_3.addWidget(self.prevButton)
        self.finishButton = QtGui.QPushButton(self.page_2)
        self.finishButton.setObjectName("finishButton")
        self.horizontalLayout_3.addWidget(self.finishButton)
        self.cancelButton2 = QtGui.QPushButton(self.page_2)
        self.cancelButton2.setObjectName("cancelButton2")
        self.horizontalLayout_3.addWidget(self.cancelButton2)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.stackWidget.addWidget(self.page_2)
        self.horizontalLayout_2.addWidget(self.stackWidget)

        self.retranslateUi(RegisterDialog)
        self.stackWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(RegisterDialog)

    def retranslateUi(self, RegisterDialog):
        RegisterDialog.setWindowTitle(QtGui.QApplication.translate("RegisterDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("RegisterDialog", "Click Next to request a login code from Facebook\n"
"to be used by Plasbook.\n"
"\n"
"Using this code, you can stay online in Plasbook\n"
"whenever you open it without having to login \n"
"again.", None, QtGui.QApplication.UnicodeUTF8))
        self.prevButton1.setText(QtGui.QApplication.translate("RegisterDialog", "<< Previous", None, QtGui.QApplication.UnicodeUTF8))
        self.nextButton.setText(QtGui.QApplication.translate("RegisterDialog", "Next >>", None, QtGui.QApplication.UnicodeUTF8))
        self.cancelButton.setText(QtGui.QApplication.translate("RegisterDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("RegisterDialog", "Now copy/paste the code here and click Next again.", None, QtGui.QApplication.UnicodeUTF8))
        self.prevButton.setText(QtGui.QApplication.translate("RegisterDialog", "<< Previous", None, QtGui.QApplication.UnicodeUTF8))
        self.finishButton.setText(QtGui.QApplication.translate("RegisterDialog", "Fininsh >>", None, QtGui.QApplication.UnicodeUTF8))
        self.cancelButton2.setText(QtGui.QApplication.translate("RegisterDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

