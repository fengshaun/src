# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import os

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from ui_mainwindow import Ui_MainWindow

from fbuser import FbUser
from debug import debug
from registerdialog import RegisterDialog
from post import Post
        
class MainWindow(QMainWindow):
    def __init__(self):
        debug('initializing MainWindow')
        
        QMainWindow.__init__(self)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.image_dir = os.path.join(os.path.dirname(__file__), 'images')
        self.setWindowIcon(QIcon(os.path.join(self.image_dir, 'book.png')))

        self.ui.updateInfoButton.clicked.connect(self.login)

        self.lastMessage = None
        self.settings = QSettings('Armin Moradi', 'Plasbook')

        self.createActions()
        self.createNotificationIcon()

        self.timer = QTimer(self)
        self.timer.setInterval(5 * 60 * 1000)  # 5 minutes
        self.timer.timeout.connect(self.updateInfo)

        debug('MainWindow init done')

        self.show()
        
        # to show the window befor logging in
        qApp.processEvents()
        
        self.login()

    def createActions(self):
        debug('Creating actions')
        self.restoreAction = QAction('Restore', self)
        self.quitAction = QAction('Quit', self)

        self.restoreAction.triggered.connect(self.showNormal)
        self.quitAction.triggered.connect(qApp.quit)
        debug('Creating actions done')

    def createNotificationIcon(self):
        debug('Creating notification icon (tray icon)')
        notifMenu = QMenu(self)
        notifMenu.addAction(self.restoreAction)
        notifMenu.addAction(self.quitAction)
            
        self.notifIcon = QSystemTrayIcon(self)
        self.notifIcon.setIcon(QIcon(os.path.join(self.image_dir, 'book.png')))
        self.notifIcon.setContextMenu(notifMenu)

        self.notifIcon.activated.connect(self.notificationIconActivated)
        
        self.notifIcon.setVisible(True)
        debug('Creating notification icon (tray icon) done')

    def notificationIconActivated(self, reason):
        debug('Notification icon (tray icon) is activated')
        if reason == QSystemTrayIcon.Trigger:
            if self.isVisible():
                self.hide()
            else:
                self.showNormal()
                self.raise_()

    def login(self):
        debug('logging in...')

        if not self.hasRegistered():
            dialog = RegisterDialog()
            if dialog.exec_():
                self.authToken = dialog.code

            self.settings.setValue('registered', True)
            self.settings.setValue('authToken', self.authToken)

            self.user = FbUser(self.authToken)
        else:
            self.user = FbUser()

        if self.user.loggedIn:
            self.ui.updateInfoButton.setText('Update info')
            
            self.ui.updateInfoButton.clicked.disconnect(self.login)
            self.ui.updateInfoButton.clicked.connect(self.updateInfo)
            self.ui.updateStatusButton.clicked.connect(self.updateStatus)

            self.ui.titleLabel.setText(('Profile of <b>%s</b>' %
                                        self.user.name()))
        else:
            debug('ERROR: Could not login')
            self.user = None

        self.timer.start()

        debug('login done')

    def hasRegistered(self):
        return self.settings.value('registered', False).toBool()

    def latestPosts(self, limit=10):
        debug('Retrieving %s latest posts' % limit)
        rawPosts = list(self.user.getLatestPosts(limit=limit))
        rawPosts.reverse()
        for rawPost in rawPosts:
            yield Post(rawPost)
        debug('Retrieving latest posts done')
            
    def updateInfo(self):
        debug('Updating information...')

        self.setGroupInvitesCount(self.user.groupInvitesCount())
        self.setMessagesCount(self.user.unreadMessageCount())
        self.setPokesCount(self.user.pokesCount())
        self.setLatestPosts(self.latestPosts())

        self.ui.statusLineEdit.setText(self.user.status())

        debug('Updating info done')

        # restarts the timer in case the user updated
        # manually.
        self.timer.start()

    def updateStatus(self):
        debug('Updating status...')
        self.user.setStatus(self.ui.statusLineEdit.text())
        debug('Status successfully updated')

    def setLatestPosts(self, posts):
        debug('Adding latest posts to MainWindow')
        for post in posts:
            self.ui.newsLayout.insertWidget(0, post)
        debug('Adding latest posts to MainWindow done')

    def setGroupInvitesCount(self, count):
        debug('Setting group invitations count: %s' % count)
        self.ui.groupInvitesLabel.setText('<b>%s</b> group invitations' % count)

    def setMessagesCount(self, count):
        debug('Setting unread messages count: %s' % count)
        self.ui.messagesLabel.setText('<b>%s</b> unread messages' % count)

    def setPokesCount(self, count):
        debug('Setting pokes count: %s' % count)
        self.ui.pokesLabel.setText('<b>%s</b> pokes' % count)
