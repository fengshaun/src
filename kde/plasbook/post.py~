# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import webbrowser
import urllib

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from debug import debug
from detailsdialog import DetailsDialog

class PosterIcon(QLabel):
    def __init__(self, icon, name, profileUrl):
        QLabel.__init__(self)

        urllib.urlretrieve(icon, '/tmp/Plasbook/%s_pic' % name.replace(' ', '_').lower())

        self.icon = '/tmp/Plasbook/%s_pic' % name.replace(' ', '_').lower()
        self.url = profileUrl
        self.pixmap = QPixmap(self.icon)
        self.setPixmap(self.pixmap)

    def setProfileLink(self, url):
        self.url = url

    def link(self):
        return url

    def mousePressEvent(self, event):
        if self.url:
            webbrowser.open(self.url)
        else:
            debug('No url set on this PosterIcon: %s' % self)

class Post(QFrame):
    def __init__(self, rawPost):
        QFrame.__init__(self)

        self.rawPost = rawPost

        firstname = rawPost.user.name.split(' ')[0]
        self.name = QLabel(firstname)
        self.name.setWordWrap(True)
        
        self.icon = PosterIcon(rawPost.user.profilePic,
                               rawPost.user.name,
                               rawPost.user.profileUrl)

        self.text = QLabel('%s\n%s' % (rawPost.message,
                                       rawPost.extendedText))
        self.text.setOpenExternalLinks(True)
        self.text.setTextFormat(Qt.RichText)
        self.text.setTextInteractionFlags(Qt.TextBrowserInteraction)

        self.text.setWordWrap(True)
        
        self.postInfo = QLabel('%s comment(s)\t%s like(s)' % (rawPost.commentsCount,
                                                              rawPost.likesCount))

        self.detailsToolButton = QToolButton()
        self.detailsToolButton.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.detailsToolButton.setText('details')

        self.detailsToolButton.clicked.connect(self.showDetails)

        self.prepareLayout()

    def showDetails(self):
        self.dialog = DetailsDialog(self.rawPost)
        self.dialog.show()

    def prepareLayout(self):
        self.layout = QGridLayout()

        self.icon.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        self.name.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.text.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.postInfo.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.layout.addWidget(self.icon, 0, 0)
        self.layout.addWidget(self.name, 1, 0, Qt.AlignCenter)
        self.layout.addWidget(self.text, 0, 1, 1, 2, Qt.AlignLeft)
        self.layout.addWidget(self.postInfo, 1, 1, Qt.AlignLeft)
        self.layout.addWidget(self.detailsToolButton, 1, 2, Qt.AlignRight)

        self.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)

        self.setLayout(self.layout)
