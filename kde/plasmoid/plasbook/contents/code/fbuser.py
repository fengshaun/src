# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Plasbook.  If not, see <http://www.gnu.org/licenses/>.

import urllib

from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import QSettings

import facebook

from debug import debug

class RawUser:
    def __init__(self, data=None):
        if data is not None:
            self.id = data['id']
            self.name = data['name']
            self.profilePic = data['pic_square']
            self.profileUrl = data['url']
        else:
            self.id = 0
            self.name = 'Unknown'
            self.profilePic = ''
            self.profileUrl = ''

class RawComment:
    def __init__(self, data):
        self.fromid = data['fromid']
        self.text = data['text']
        self.time = data['time']
        
class RawPost:
    def __init__(self, data, comments, profiles):
        self.likesCount = data['likes']['count'] if data['likes']['can_like'] else 0
        self.permalink = data['permalink']
        self.taggedIds = data['tagged_ids']
        self.privacyValue = data['privacy']['value']
        self.commentsCount = data['comments']['count']
        self.message = data['message']
        self.post_id = data['post_id']

        self.user = None
        for profile in profiles:
            if data['actor_id'] == profile['id']:
                self.user = RawUser(profile)
        if self.user is None:
            self.user = RawUser()

        self.extendedText = ''
        self.media = []

        if data['attachment']:
            if 'name' in data['attachment']:
                self.extendedText += data['attachment']['name']

            if 'description' in data['attachment']:
                self.extendedText += data['attachment']['description']

            for m in data['attachment']['media']:
                if m['type'] in ['photo', 'video', 'link']:
                    if 'src' in m and m['src'] and m['src'][0] == '/':
                        m['src'] = 'http://facebook.com' + m['src']
                    self.media.append(m)

        self.comments = [RawComment(comment) for comment in comments]

class FbUser:

    API_KEY = 'a3caef90690d04a42c4bc0bc96af2143'
    SECRET = '02683d80263e0ba075c9cac8d92808ec'
    
    def __init__(self, authToken=None):
        self.login(authToken)
        
        self.uid = self.fb.uid
        self.lastPostId = ''
	
    def login(self, authToken):
        settings = QSettings('Armin Moradi', 'Plasbook')

        if authToken is not None:
            self.fb = facebook.Facebook(FbUser.API_KEY, FbUser.SECRET, authToken)
            data = self.fb.auth.getSession()
        
            if data and 'session_key' in data:
                debug('saving settings:')
                debug('\t%s' % data['secret'])
                debug('\t%s' % data['session_key'])
                
                settings.setValue('secret', str(data['secret']))
                settings.setValue('session_key', str(data['session_key']))
                
                if not self.fb.users.hasAppPermission('status_update', uid=self.fb.uid):
                    self.fb.request_extended_permission('status_update', popup=True)
                    self.waitLogin()
            
                if not self.fb.users.hasAppPermission('read_stream', uid=self.fb.uid):
                    self.fb.request_extended_permission('read_stream', popup=True)
                    self.waitLogin()

                if not self.fb.users.hasAppPermission('read_inbox', uid=self.fb.uid):
                    self.fb.request_extended_permission('read_inbox', popup=True)
                    self.waitLogin()
        else:
            self.fb = facebook.Facebook(FbUser.API_KEY, FbUser.SECRET)
            self.fb.session_key = str(settings.value('session_key', '').toString()).strip()
            self.fb.secret = str(settings.value('secret', '').toString()).strip()
            self.fb.uid = self.fb.session_key.split('-')[1]

            if not (self.fb.secret or self.fb.session_key):
                debug('No session_key or secret found...clearing settings')
                settings.clear()
                return None

        self.loggedIn = True

    def setStatus(self, status=''):
        self.fb.status.set(self.uid, status)

    def name(self):
        return self.fb.users.getInfo([self.fb.uid], ['name'])[0]['name']

    def status(self):
        return self.fb.status.get(self.uid, 1)[0]['message']

    def setStatus(self, message):
        if self.fb.status.set(message, self.uid) == 1:
            print 'successfully set status message'
        else:
            print 'An error occurred while setting status message'

    def affiliations(self):
        return [aff['name'] for aff in self.fb.users.getInfo([self.uid], ['affiliations'])[0]['affiliations']]

    def unreadMessageCount(self):
        return self.fb.notifications.get()['messages']['unread']

    def pokesCount(self):
        return self.fb.notifications.get()['pokes']['unread']

    def groupInvitesCount(self):
        return len(self.fb.notifications.get()['group_invites'])

    def friendRequests(self):
        friends = []
        for uid in self.fb.notifications.get()['friend_requests']:
            name = self.fb.users.getInfo([uid], ['name'])[0]['name']
            friends.append(name)
        return friends

    def friendRequestsCount(self):
        return len(self.fb.notifications.get()['friend_requests'])

    def getLatestPosts(self, limit=10):
        latestPosts = self.fb.stream.get(limit=limit)['posts']
        latestPeople = self.fb.stream.get(limit=(max(limit, 20)))['profiles']

        for data in latestPosts:
            if self.lastPostId == data['post_id']:
                debug('returning: %s == %s' % (self.lastPostId, data['post_id']))
                self.lastPostId = latestPosts[0]['post_id']
                return
            commentsData = self.fb.stream.getComments(data['post_id'])
            yield RawPost(data, commentsData, latestPeople)

        debug('yielded all posts: %s' % latestPosts[0]['post_id'])
        self.lastPostId = latestPosts[0]['post_id']

    @staticmethod
    def waitLogin(self):
        dialog = QMessageBox(QMessageBox.Information,
                             '',
                             'Press Ok after login',
                             QMessageBox.Ok)
        dialog.exec_()
