# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Plasbook.  If not, see <http://www.gnu.org/licenses/>.

import urllib
import copy
import threading
from multiprocessing import Pool

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript
from PyKDE4.kdecore import KUrl

from fbuser import FbUser
from debug import debug
from post import Post
import facebook

class Plasbook(plasmascript.Applet):
    def __init__(self, parent):
        plasmascript.Applet.__init__(self, parent)

    def init(self):
        debug('applet initialization started...')
        self.setHasConfigurationInterface(False)

        self.theme = Plasma.Svg(self)
        self.theme.setImagePath('widgets/background')
        self.setBackgroundHints(Plasma.Applet.DefaultBackground)
        self.setAspectRatioMode(Plasma.IgnoreAspectRatio)

        # (Trying to) center the loginButton on startup
        self.loginButton = Plasma.PushButton(self.applet)
        self.loginButton.setText('Login to Facebook')
        self.loginButton.clicked.connect(self.login)

        self.mainLayout = QGraphicsLinearLayout(Qt.Horizontal, self.applet)
        innerLayout = QGraphicsLinearLayout(Qt.Vertical)
        innerLayout.addStretch()
        innerLayout.addItem(self.loginButton)
        innerLayout.addStretch()
        self.mainLayout.addStretch()
        self.mainLayout.addItem(innerLayout)
        self.mainLayout.addStretch()
        debug('done')

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_F5:
            self.updateData()
            event.accept()
        elif event.key() == Qt.Key_F6:
            # a debugging feature :)
            self.testNew()
            event.accept()
                
    def login(self):
        debug('logging in...')
        self.loginButton.hide()
        self.setBusy(True)

        self.user = FbUser()
        debug('done')
        if self.user.loggedIn:
            self.setupUi()
        self.setBusy(False)

        self.timer = QTimer()
        self.timer.setInterval(5 * 60 * 1000)  # 5 minutes
        self.timer.timeout.connect(self.updateData)
        self.timer.start()

    def setupUi(self):
        debug('Setting up UI...')
        self.mainLayout = QGraphicsLinearLayout(Qt.Vertical, self.applet)
        self.tabBar = Plasma.TabBar(self.applet)
        self.newsLayout = QGraphicsGridLayout()
        self.notifLayout = QGraphicsGridLayout()
        self.postsLayout = QGraphicsLinearLayout(Qt.Vertical)
        self.friendRequestsLayout = QGraphicsLinearLayout(Qt.Vertical)
        self.friendRequestsLayout.setContentsMargins(30, 5, 5, 5)

        self.updateStatusBtn = Plasma.PushButton()
        self.updateStatusBtn.setText('Update status')
        self.updateStatusBtn.setSizePolicy(QSizePolicy.Fixed,
                                           QSizePolicy.Fixed)

        self.statusLineEdit = Plasma.LineEdit()
        self.statusLineEdit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.statusLineEdit.returnPressed.connect(self.setStatus)

        self.titleLabel = Plasma.Label(self.applet)
        self.titleLabel.setText('Profile of <b>%s</b>' % self.user.name())
        self.titleLabel.setAlignment(Qt.AlignHCenter)
        self.titleLabel.setSizePolicy(QSizePolicy.Preferred,
                                      QSizePolicy.Fixed)

        # Notifications layout labels
        self.messagesLabel = Plasma.Label()
        self.messagesLabel.setText('<b>--</b> unread messages')
        self.pokesLabel = Plasma.Label()
        self.pokesLabel.setText('<b>--</b> pokes')
        self.groupLabel = Plasma.Label()
        self.groupLabel.setText('<b>--</b> group invitations')
        self.friendRequestsLabel = Plasma.Label()
        self.friendRequestsLabel.setText('<b>--</b> friend requests')

        # News layout labels
        self.postsLabel = Plasma.Label()
        self.postsLabel.setText('Recent posts')
        self.refreshLabel = Plasma.Label()
        self.refreshLabel.setText('<b>Refreshing...</b>')
        self.refreshLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.refreshLabel.setAlignment(Qt.AlignRight)
        self.refreshLabel.hide()

        self.scrollWidget = Plasma.ScrollWidget()
        self.postsWidget = QGraphicsWidget()
        self.scrollWidget.setWidget(self.postsWidget)
        self.postsLayout = QGraphicsLinearLayout(Qt.Vertical, self.postsWidget)
        self.postsLayout.setSizePolicy(QSizePolicy.Expanding,
                                       QSizePolicy.Expanding)

        debug('laying out newsLayout')
        self.newsLayout.addItem(self.statusLineEdit, 0, 0)
        self.newsLayout.addItem(self.updateStatusBtn, 0, 1)
        self.newsLayout.addItem(self.postsLabel, 1, 0)
        self.newsLayout.addItem(self.scrollWidget, 2, 0, 1, 2)
        
        self.newsLayout.addItem(self.refreshLabel, 1, 1)
        self.newsLayout.setSizePolicy(QSizePolicy.Expanding,
                                      QSizePolicy.Expanding)

        debug('laying out notifLayout')
        self.notifLayout.addItem(self.pokesLabel, 0, 0)
        self.notifLayout.addItem(self.messagesLabel, 1, 0)
        self.notifLayout.addItem(self.groupLabel, 2, 0)
        self.notifLayout.addItem(self.friendRequestsLabel, 3, 0)
        self.notifLayout.addItem(self.friendRequestsLayout, 4, 0)
        
        self.tabBar.addTab('News', self.newsLayout)
        self.tabBar.addTab('Notifications', self.notifLayout)

        self.mainLayout.addItem(self.titleLabel)
        self.mainLayout.addItem(self.tabBar)

        self.updateStatusBtn.clicked.connect(self.setStatus)

        debug('UI setup done')

        QTimer.singleShot(1000, self.updateData)
 
    def updateData(self):
        self.refreshLabel.show()
        qApp.processEvents()
        debug('Updating data...')
        data = {}
        data['unreadMessageCount'] = self.user.unreadMessageCount()
        qApp.processEvents()
        data['pokesCount'] = self.user.pokesCount()
        qApp.processEvents()
        data['groupInvitesCount'] = self.user.groupInvitesCount()
        qApp.processEvents()
        data['friendRequestsCount'] = self.user.friendRequestsCount()
        qApp.processEvents()
        data['status'] = self.user.status()
        qApp.processEvents()
        latestPosts = []
        for post in self.user.getLatestPosts(2):
            latestPosts.insert(0, post)
            qApp.processEvents()
        data['latestPosts'] = latestPosts
        qApp.processEvents()
        self.updateUi(data)

    def updateUi(self, data):
        debug('updating Ui: %s' % data)
        self.messagesLabel.setText('<b>%s</b> unread messages' %
                                   data['unreadMessageCount'])
        self.pokesLabel.setText('<b>%s</b> pokes' % data['pokesCount'])
        self.groupLabel.setText('<b>%s</b> group invitations' %
                                data['groupInvitesCount'])
        self.friendRequestsLabel.setText('<b>%s</b> friend requests' %
                                         data['friendRequestsCount'])
        self.statusLineEdit.setText(data['status'])
        self.statusLineEdit.nativeWidget().setCursorPosition(0)

        allNotifsCount = (data['unreadMessageCount'] +
                          data['pokesCount'] +
                          data['groupInvitesCount'] +
                          data['friendRequestsCount'])

        self.tabBar.setTabText(1, 'Notifications (%d)' % allNotifsCount)

        for post in data['latestPosts']:
            self.postsLayout.insertItem(0, Post(post))

        debug('done')
        self.refreshLabel.hide()

    def setStatus(self):
        debug('Updating status...')
        self.user.setStatus(self.statusLineEdit.text())

    def testNew(self):
        debug('testing...')
	
def CreateApplet(parent):
    return Plasbook(parent)
