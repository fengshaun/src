# Copyright (c) Armin Moradi 2009
#
# This file is part of Plasbook.
#
# Plasbook is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Plasbook is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Plasbook.  If not, see <http://www.gnu.org/licenses/>.

import urllib
import webbrowser

from PyKDE4.plasma import Plasma
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class CommentsDialog(Plasma.Dialog):
    def __init__(self, comments, parent):
        Plasma.Dialog.__init__(self, parent)

        self.l = QGraphicsLinearLayout(Qt.Vertical, self.graphicsWidget())
        self.b = Plasma.PushButton()
        self.b.setText('hello')
        self.l.addItem(self.b)

class PostTextBrowser(Plasma.TextBrowser):
    def __init__(self):
        Plasma.TextBrowser.__init__(self)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.nativeWidget().setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.nativeWidget().setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.nativeWidget().setCursor(Qt.ArrowCursor)

    def wheelEvent(self, event):
        event.ignore()

class Post(Plasma.Frame):
    def __init__(self, rawPost):
        Plasma.Frame.__init__(self)
        
        self.rawPost = rawPost
        self.setupUi()
        self.commentsDialog = CommentsDialog(self.rawPost.comments, self.nativeWidget())

    def setupUi(self):
        self.iconFullName = '/tmp/%s_profile_pic' % self.rawPost.user.name.replace(' ', '_').lower()
        urllib.urlretrieve(self.rawPost.user.profilePic, self.iconFullName)

        self.layout = QGraphicsGridLayout()
        
        self.icon = Plasma.IconWidget(QIcon(self.iconFullName),
                                      self.rawPost.user.name)
        iconSize = self.icon.sizeFromIconSize(32)
        self.icon.setMinimumSize(iconSize)
        self.icon.setMaximumSize(iconSize)
        self.icon.setTextBackgroundColor(QColor())
        self.icon.clicked.connect(self.openProfile)

        self.text = PostTextBrowser();
        self.text.setText('%s\n%s' % (self.rawPost.message,
                                      self.rawPost.extendedText))
        
        self.commentsToolBtn = Plasma.ToolButton()
        self.commentsToolBtn.setText('%s comment(s)' % self.rawPost.commentsCount)
        self.commentsToolBtn.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.commentsToolBtn.clicked.connect(self.openCommentsDialog)
        
        self.likesToolBtn = Plasma.ToolButton()
        self.likesToolBtn.setText('%s like(s)' % self.rawPost.likesCount)
        self.likesToolBtn.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.layout.addItem(self.icon, 0, 0, 1, 1)
        self.layout.addItem(self.text, 0, 1, 1, 3)
        self.layout.addItem(self.commentsToolBtn, 1, 1, 1, 1)
        self.layout.addItem(self.likesToolBtn, 1, 2, 1, 1)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        
        self.setLayout(self.layout)

    def openCommentsDialog(self):
        if self.commentsDialog.isVisible():
            self.commentsDialog.animatedHide(Plasma.Left)
        else:
            self.commentsDialog.animatedShow(Plasma.Right)

    def openProfile(self):
        webbrowser.open(self.rawPost.user.profileUrl)
