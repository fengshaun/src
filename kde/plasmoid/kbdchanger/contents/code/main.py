# Armin Mordi (c) 2010, GPLv3
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript

class Config:
    def __init__(self, f):
        self.kblayouts = {}
        try:
            self.config = open(f, 'r')

            for line in self.config.readlines():
                if not line: continue
                name = line.strip().split('=')[0].strip()
                layout = line.strip().split('=')[1].strip()     

                self.kblayouts[name] = layout
        except IOError as e:
            pass

        if not self.kblayouts:
            self.kblayouts = {'Dvorak': 'dvorak', 'Qwerty': 'us'}

    def layouts(self):
        return self.kblayouts

class ChangerWrapper:
    def __init__(self, layout):
        self.layout = layout

    def __call__(self):
        os.system("setxkbmap %s" % self.layout)

class KbdChanger(plasmascript.Applet):
    def __init__(self, parent, args=None):
        plasmascript.Applet.__init__(self, parent)

    def init(self):
        self.setHasConfigurationInterface(False)
        self.resize(100, 100)

        self.setupUi()

    def setupUi(self):
        self.layout = QGraphicsLinearLayout(Qt.Vertical, self.applet)

        for name, layout in Config("/home/armin/.kbchangerrc").layouts().items():
            btn = Plasma.PushButton()
            btn.setText(name)
            btn.clicked.connect(ChangerWrapper(layout))
            self.layout.addItem(btn)

def CreateApplet(parent):
    return KbdChanger(parent)
