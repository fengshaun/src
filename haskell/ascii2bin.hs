{- Converts a text file containing ones and zeros
 - to an actual binary file containing those same
 - ones and zeros - obviously in binary
 -}

import System.Environment
import System.IO
import System.FilePath.Posix
import Control.Monad
import Data.Char
import Data.Word
import Data.List
import Data.List.Split (chunksOf)
import qualified Data.ByteString as B

binFactors :: [Integer]
binFactors = [2^x | x <- [1..]]

processFile :: Handle -> Handle -> IO ()
processFile inHandle outHandle = hGetContents inHandle
                               >>= \s -> B.hPut outHandle (asciiToBS (concat. lines $ s))
                               >> hClose inHandle
                               >> hClose outHandle

binToWord8 :: String -> Word8
binToWord8 = fromIntegral . sum . zipWith (*) [2^x | x <- [0..]] . map digitToInt . reverse

asciiToBS :: String -> B.ByteString
asciiToBS = B.pack . map binToWord8 . chunksOf 8

main = do
  [filename] <- getArgs
  inHandle <- openFile filename ReadMode
  outHandle <- openBinaryFile (dropExtension filename ++ ".bin") WriteMode
  processFile inHandle outHandle
