{- Reverse polish notation calculator
 - NOT error safe, it'll expolode in yo face
 - if you give it the wrong food!
 -}

import System.Environment
import Data.Char (digitToInt)

{- Why?  Because I can, that's why! -}
(-:) :: a -> (a -> b) -> b
(-:) = flip ($)

calcRPN :: [String] -> Double
calcRPN xs = head $ foldl rpn [] xs
  where
  rpn :: [Double] -> String -> [Double]
  rpn acc x = case x of "*" -> acc -: \(x:y:zs) -> (x*y):zs
                        "/" -> acc -: \(x:y:zs) -> (x/y):zs
                        "+" -> acc -: \(x:y:zs) -> (x+y):zs
                        "-" -> acc -: \(x:y:zs) -> (x-y):zs
                        _   -> read x : acc

main = getArgs >>= (putStrLn . show . calcRPN)
