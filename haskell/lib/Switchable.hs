-- switching stuff on and off
{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

module Switchable (
  Switchable,
  switch
) where

class Switchable a where
  switch :: a -> a

instance Switchable Bool where
  switch True = False
  switch False = True

instance (Num a) => Switchable a where
  switch = negate


