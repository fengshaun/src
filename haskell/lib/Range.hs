{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

{- implementing an overloaded "range" function like this:
 - range 5 -> [0,1,2,3,4,5]
 - range (2, 5) -> [2,3,4,5]
 - range (2, 8, 3) -> [2, 5, 8]
 -}

module Range (
  range,
) where

class Range a where
  range :: a -> [Int]

instance Range Int where
  range n = [0..n]

instance Range (Int, Int) where
  range (a, b) = [a..b]

instance Range (Int, Int, Int) where
  range (a, b, s) = (:) a $ map fst . filter (\(x, y) -> y `mod` s == 0) . zip (tail [a..b]) $ [1..]

