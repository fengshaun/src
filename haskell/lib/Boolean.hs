-- turn everything into Bool
{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

module Boolean (
  Boolean,
  toBool,
) where

class Boolean a where
  toBool :: a -> Bool

instance Boolean Bool where
  toBool = id

instance (Ord a, Num a) => Boolean a where
  toBool x | x <= 0 = False
           | otherwise = True

