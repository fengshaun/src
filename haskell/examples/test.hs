take' :: Integral a => a -> [b] -> [b]
take' n _ | n <= 0 = []
take' _ [] = []
take' n (x:xs) = x : take' (n-1) xs

rev' :: [a] -> [a]
rev' [] = []
rev' (x:xs) = rev' xs ++ [x]

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f xs ys = map (\x -> f (fst x) (snd x)) (zip xs ys)

-- largest number below 100000 that's divisible by 3829
largest :: Int
largest = head . filter (\x -> x `mod` 3829 == 0) $ [100000, 99999..]

 --sum of all odd squares below 100000
sumOfAll :: Integer
sumOfAll = sum . filter odd . takeWhile (<10000) . map (^2) $ [1..]

-- collatz sequences
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n | odd n =  n : chain (n * 3 + 1)
        | even n = n : chain (n `div` 2)

collatzSpecific :: Int
collatzSpecific = length . filter (>15) . map (length . chain) $ [1..100]

-- maximum function with foldl1
maxim :: Ord a => [a] -> a
maxim = foldl1 max 
