{- Haskell Network test with the joys of facebook -}
module Main where

import Network.URI
import Network.HTTPS
import Network.Browser
import Data.List
import Data.Either
import Data.Either.Unwrap

accessToken :: String
accessToken = "112366752125381|f2e613ef0e9a03c9108b3282-595033059|VKfwvVP3wuksH_uA1MJzwJP8Eww."

baseUrl :: String
baseUrl = "https://graph.facebook.com/"

--baseUrl ++ (concat . intersperse "/" $ ["me", "home"]) ++ "?access_token=" ++ accessToken ++ "&limit5"
myReq = Request {
          rqURI = URI {
            uriScheme = "https",
            uriAuthority = Just URIAuth {
              uriRegName = "graph.facebook.com",
              uriUserInfo = "",
              uriPort = "" 
            },
            uriPath = concat . intersperse "/" $ ["me", "home"],
            uriQuery = "?access_token=" ++ accessToken ++ "&limit=5",
            uriFragment = ""
          },
          rqMethod = GET,
          rqHeaders = [],
          rqBody = ""
        }

extractResponse :: Result (Response [Char]) -> String
extractResponse res = rspBody . fromRight $ res

main :: IO ()
main = do resp <- simpleHTTP myReq
          putStrLn . extractResponse $ resp
