import Data.List

myLength :: [a] -> Int
myLength (_:xs) = 1 + myLength xs
myLength [] = 0

mySum :: [Int] -> Int
mySum (x:xs) = x + mySum xs
mySum [] = 0

myMean :: [Int] -> Double
myMean (xs) | not (null xs) = (fromIntegral (mySum xs)) / (fromIntegral (myLength xs))
----------------------------------------------------
makePalindrome :: [a] -> [a]
makePalindrome xs | null xs   = []
		  | otherwise = xs ++ reverse xs

isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs = xs == reverse xs
----------------------------------------------------
myOrdering :: [a] -> [a] -> Ordering
myOrdering as bs = if a == b then
			EQ
		   else if a > b then
			GT
		   else
			LT
		   where a = length as
			 b = length bs
sortByLength xs = sortBy myOrdering xs ----------------------------------------------------
joinList :: Show a => [a] -> String
joinList (x:xs) = show x ++ joinList xs
joinList [] = ""

myConcat :: Show a => a -> [a] -> String
myConcat a (x:xs) = init (show x ++ show a ++ myConcat a xs)

--myIntersperse :: a -> [b] -> String
myIntersperse _ [] = []
myIntersperse sep (x:xs) | not (null xs) = x ++ [sep] ++ myIntersperse sep xs
			 | otherwise = x
----------------------------------------------------
fac :: Int -> Int
fac 1 = 1
fac n = n * fac(n - 1)
----------------------------------------------------
-- Euler20
lastTenDigits :: Integer -> Integer
lastTenDigits i = read(drop (length sn - 10) sn) :: Integer
		  where sn = show . sum $ [x^x | x <- [1..i]]
----------------------------------------------------
isPrime :: Integer -> Bool
isPrime n | n < 2 = False
          | even n = False
          | otherwise = not . any (\r -> n `mod` r == 0) $ [x | x <- [3,5..n-1]]
-----------------------------------------------------
primesUpTo :: Integer -> [Integer]
primesUpTo n | n < 2 = []
             | otherwise = 2 : [x | x <- [3,5..n-1], isPrime x]
-----------------------------------------------------
main = return . primesUpTo $ 1000
