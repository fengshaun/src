-- Testing Functor
import Control.Monad (Functor)

data TwoTuple a = TwoTuple a a

instance (Show a) => Show (TwoTuple a) where
    -- show :: TwoTuple a -> String
    show (TwoTuple a b) = "(" ++ show a ++ ", " ++ show b ++ ")"

instance Functor TwoTuple where
    -- fmap :: (a -> b) -> TwoTuple a -> TwoTuple b
    fmap f (TwoTuple a b) = TwoTuple (f a) (f b)

main = print . fmap (+1) $ TwoTuple 1 2
