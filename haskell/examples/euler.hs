import System.IO

-- Euler63 = 49
euler63 :: [(Integer, Integer, Int)]
euler63 = [(x^y, x, y) | x <- [1..9], y <- [1..500], (length . show $ x^y) == y]

formatEuler63 :: [(Integer, Integer, Int)] -> String
formatEuler63 (x:xs) = show x ++ "\n" ++ formatEuler63 xs
formatEuler63 _ = ""

-- Euler67
euler67 :: [[Int]] -> Int -> Int -> Integer
euler67 (row:rows) acc cIndex = euler67 rows (acc + row !! i) i
                                where i = if row !! cIndex > row !! (cIndex + 1) then
                                            cIndex
                                          else
                                            cIndex + 1
euler67 _ acc _ = fromIntegral acc

{- TODO
readEuler67 :: IO [[Int]]
readEuler67 = do contents <- readFile "triangle.txt"
                 makeListEuler67 . break (\c -> c == '\n') $ contents

makeListEuler67 :: [String] -> [Int]
makeListEuler67 = [0]
-}

-- Utility functions
split :: Char -> String -> [String]
split _ "" = []
split c xs = fst part : (split c . drop 1 . snd $ part)
             where part = break (\i -> i == c) xs

formatVertical :: [String] -> String
formatVertical (s:ss) = s ++ "\n" ++ formatVertical ss
formatVertical [] = ""

processFile :: Handle -> IO ()
processFile h = do inEOF <- hIsEOF h
                   if inEOF then 
                      return ()
                      else
                      do l <- hGetLine h
                         putStrLn . show . split ' ' $ l
                         processFile h

-- Main
main :: IO ()
main = do fHandle <- openFile "test_triangle.txt" ReadMode
          processFile fHandle
