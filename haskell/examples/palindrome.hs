main :: IO ()
main = interact $ unlines . filter (\l -> l == reverse l) . filter (not . null) . lines
