-- find second last element of a list
secondLast :: [a] -> a
secondLast = head . tail . reverse

main = print . secondLast $ [1..99]
