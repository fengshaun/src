-- Run-length encoding of a list.  Use the result of problem 9.hs (use
-- group from Data.List instead) to implement the so-called run-length
-- encoding data compression method.  Consecutive duplicates of elements
-- are encoded as lists (N E) where N is the number of duplicates of element E.

import Data.List (group)

encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = [(length ys, head ys) | ys <- group xs]

main = print . encode $ "aaaabccaadeeee"
