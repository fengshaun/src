-- create a list containing all integers within a range
range :: Integer -> Integer -> [Integer]
range n1 n2 = filter (>= n1) [1..n2]

main = print $ range 4 9
