-- find the length of a list
myLength :: [a] -> Integer
myLength xs = sum [1 | _ <- xs]

main = print . myLength $ [1..99]
