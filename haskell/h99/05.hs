-- reverse a list
myReverse :: [a] -> [a]
myReverse (x:xs) = (myReverse xs) ++ [x]
myReverse [] = []

main = print . myReverse $ [1..99]
