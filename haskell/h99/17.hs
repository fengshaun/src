-- split a list into two parts; the length of the first part is given

split :: Int -> [a] -> ([a], [a])
split n xs = (firstPart, secondPart)
             where
               zipped = zip xs [1..]
               firstPart = map fst $ filter (\(a, b) -> b <= n) zipped
               secondPart = map fst $ filter (\(a, b) -> b > n) zipped

main = print . split 3 $ [1..7]
