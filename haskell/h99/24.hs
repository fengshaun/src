-- draw N different random numbers from the set 1..M
import System.Random

diffSelect :: Int -> Int -> IO [Int]
diffSelect n m = do
  gen <- getStdGen
  return . take n $ randomRs (1, m) gen

main = do
  l <- diffSelect 6 49
  print l

