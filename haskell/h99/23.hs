-- extract a given number of randomly selected elements from a list
import System.Random

pick :: [Int] -> [a] -> [a]
pick is xs = foldl (\acc e -> (xs !! e) : acc) [] is

rndSelect :: Int -> [a] -> IO [a]
rndSelect n xs = do
  gen <- getStdGen
  let indexes = take n $ randomRs (0, length xs - 1) gen
      res = pick indexes xs
  return res

main = do
  res <- rndSelect 3 $ ['a'..'h']
  print res
