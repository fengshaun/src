-- generate the combinations of K distinct objects chosen from the N elements of a list
import Data.List (subsequences)

combinations :: Int -> [a] -> [[a]]
combinations n = filter ((== n) . length) . subsequences

main = print . combinations 3 $ ['a'..'f']
