-- decode a run-length encoded list
import Data.List (group)

-- Run-Length Item
data RLItem a = Multiple Int a | Single a
                deriving (Show, Eq)

encode :: (Eq a) => [a] -> [RLItem a]
encode xs = [y | x <- group xs, let y = if (length x) == 1 then Single (head x) else Multiple (length x) (head x)]

decode :: [RLItem a] -> [a]
decode (Single x : rest) = x : decode rest
decode (Multiple n x : rest) = (take n . repeat $ x) ++ decode rest
decode [] = []

decode2 :: [RLItem a] -> [a]
decode2 = foldl (\acc e -> case e of Single x -> acc ++ [x]; Multiple n x -> acc ++ replicate n x) []

main = print . decode . encode $ "aaaabccaadeeee"
