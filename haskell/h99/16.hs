-- drop every n'th element from the list
dropEvery :: Int -> [a] -> [a]
dropEvery 0 xs = xs
dropEvery _ [] = []
dropEvery n xs | length xs < n = xs
               | otherwise = (init . fst . splitAt n $ xs) ++ dropEvery n (snd . splitAt n $ xs)

dropEvery2 :: Int -> [a] -> [a]
dropEvery2 n xs = snd $ foldl (\acc e -> if fst acc > 1 then (fst acc - 1, snd acc ++ [e]) else (n, snd acc)) (n, []) xs

main = print . dropEvery2 3 $ "abcdefghik"
