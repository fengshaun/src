-- 1 find the last element of a list
lastElement :: [a] -> a
lastElement (x:[]) = x
lastElement (x:xs) = lastElement xs

main = print . lastElement $ [1..99]
