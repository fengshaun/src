-- duplicate elements of a list
dupli :: [a] -> [a]
dupli (x:xs) = replicate 2 x ++ dupli xs
dupli [] = []

dupli2 :: [a] -> [a]
dupli2 = foldl (\acc e -> acc ++ replicate 2 e) []

dupli3 :: [a] -> [a]
dupli3 = concatMap (replicate 2)

main = print . dupli3 $ [1..3]
