-- pack consecutive duplicates of list elements into sublists.  If  a list
-- contains repeated elements, they should be put into different sublists.
pack :: (Eq a) => [a] -> [[a]]
pack (x:xs) = (x : takeWhile ((==) x) xs) : pack (dropWhile ((==) x) xs)
pack [] = [[]]

main = print . pack $ ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']
