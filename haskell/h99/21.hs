-- insert an element at a given position in a list
insertAt :: Int -> a -> [a] -> [a]
insertAt n x xs = let (b, f) = splitAt n xs in b ++ [x] ++ f

main = print . insertAt 2 'X' $ ['a'..'d']
