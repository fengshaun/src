-- extract a slice from a list
slice :: Int -> Int -> [a] -> [a]
slice n1 n2 xs = map fst $ filter ((<=n2) . snd) . filter ((>=n1) . snd) . zip xs $ [1..]

main = print . slice 3 7 $ ['a','b','c','d','e','f','g','h','i','k']
