-- replicate the elements of a list a given number of times
repli :: Int -> [a] -> [a]
repli n xs = foldl (\acc e -> acc ++ repli' n e) [] xs
             where
               repli' :: Int -> a -> [a]
               repli' 0 _ = []
               repli' n x = x : repli' (n-1) x

main = print . repli 3 $ "abc"
