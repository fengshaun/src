-- flatten a nested list structure
data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List []) = []

main = print . flatten $ List [Elem 1, List [Elem 2, Elem 3, List [Elem 4, Elem 5], Elem 6]]
