-- rotate a list N places to the left
rotate :: Int -> [a] -> [a]
rotate 0 xs = xs
rotate n xs | n < 0 = liftRotate (length xs - n)
            | n > 0 = liftRotate n
            where
              liftRotate n = let (back, front) = splitAt n xs in back ++ front

main = print . rotate (-2) $ ['a'..'h']
