-- find out whether a list is a palindrome
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

main = print . isPalindrome $ [1..5] ++ [5,4..1]
