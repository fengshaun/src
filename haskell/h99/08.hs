-- Eliminate consecutive duplicates of list elements
compress :: (Eq a) => [a] -> [a]
compress (x:y:ys) = if x == y then compress (y:ys) else x : compress (y:ys)
compress [x] = [x]

main = print . compress $ ["a","a","a","a","b","c","c","a","a","d","e","e","e","e", "a","b","c","a","d","e"]
