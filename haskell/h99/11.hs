-- modify 10.hs in a way that if an element has no duplicates
-- it is simply copied into the list

import Data.List (group)

-- Run-Length Item
data RLItem a = Multiple Int a | Single a
                deriving (Show, Eq)

encode :: (Eq a) => [a] -> [RLItem a]
encode xs = [y | x <- group xs, let y = if (length x) == 1 then Single (head x) else Multiple (length x) (head x)]

main = print . encode $ "aaaabccaadeeee"
