-- remove the K'th element from the list
removeAt :: Int -> [a] -> (a, [a])
removeAt n xs = let (front, back) = splitAt n xs in (last front, init front ++ back)

main = print . removeAt 2 $ ['a'..'d']
