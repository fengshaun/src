-- find the K'th element in the list where first element is 1
elementAt :: [a] -> Int -> a
elementAt xs n = xs !! (n - 1)

main = print . elementAt [1..99] $ 5
