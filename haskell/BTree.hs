module BSTree
  (BStree(..),
   left
  ) where

data BSTree a = Tip | Node a (BSTree a) (BSTree a) deriving (Show, Eq)
data Direction = Left | Right

class Functor BTree where
  fmap :: (a -> b) -> BTree a -> BTree b
  fmap _ (Tip) = Tip
  fmap f (Node x l r) = Node (f x) (fmap f l) (fmap f r)

leaf :: a -> BTree a
leaf x = Node x Tip Tip

isLeaf :: BTree a -> Bool
isLeaf (Node _ Tip Tip) = True
isLeaf _ = False

traverse :: Direction -> BTree a -> BTree a
traverse _     (Tip)        = Tip
traverse Left  (Node _ l _) = l
traverse Right (Node _ _ r) = r

left :: BTree a -> BTree a
left t = traverse Left t

right :: BTree a -> BTree a
right t = traverse Right t


insertAt :: [Direction] -> a -> BTree a -> BTree a
insertAt _ x (Tip) = leaf x
insertAt (d:ds) x t = insertAt ds x (traverse d t)

fromList :: [a] -> BTree a
fromList xs = foldr 
