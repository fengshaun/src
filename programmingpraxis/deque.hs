{- A normal linked list can be accessed only at its head. A double-ended queue, or 
 - deque (pronounced “deck”), can be accessed at either end. Like a normal list, 
 - a deque can be null. New elements can be added at either end, the element at 
 - either end of a non-null deque can be fetched, and the element at either end 
 - of a non-null deque can be deleted. Deques are a combination of stacks and queues.
 -
 - Your task is to write a function library that implements deques; you should be 
 - sure that all operations are performed in constant time. When you are finished, 
 - you are welcome to read or run a suggested solution, or to post your own solution 
 - or discuss the exercise in the comments below. 
 -}
 
data Deque a = Deque { deque :: [a] }
               deriving (Show)

beg :: Deque a -> a
beg d = head . deque $ d

end :: Deque a -> a
end d = last . deque $ d

delBeg :: Deque a -> Deque a
delBeg d = Deque $ tail . deque $ d

delEnd :: Deque a -> Deque a
delEnd d = Deque $ init . deque $ d

addBeg :: a -> Deque a -> Deque a
addBeg i d = Deque $ ((:) i) . deque $ d

addEnd :: a -> Deque a -> Deque a
addEnd i d = Deque $ (flip (++) $ [i]) . deque $ d

main = print . addEnd 1 . addEnd 7 . addBeg 5 . addBeg 4 $ (Deque [1, 2, 3] :: Deque Int)
