-- return the first non-repeating character in a string
import qualified Data.List as L

data C = C { value :: Char, index :: Int }

instance Show C where
  show c = "(" ++ [value c] ++ "-" ++ show (index c) ++ ")"

instance Eq C where
  a == b = value a == value b

instance Ord C where
  compare a b = compare (value a) (value b)

firstNonRepeating :: String -> Char
firstNonRepeating = value . safeHead . L.sortBy (\a b -> compare (index a) (index b)) . concat . filter (not . (> 1) . length) . L.group . L.sort . buildIndex
                    where
                      tupleToC (v, i) = C v i
                      buildIndex xs = foldl (\acc e -> acc ++ [tupleToC e]) [] $ zip xs [1..]
                      safeHead xs | null xs = error "no non-repeating characters"
                                  | otherwise = head xs

main = print . firstNonRepeating $ "aabbcc"
