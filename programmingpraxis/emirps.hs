import Data.Numbers.Primes

emirps :: [Integer]
emirps = filter (\a -> isEmirp a) . filter (\a -> not . isPalindrome $ a) . takeWhile (< 1000000) $ primes
         where
           isPalindrome :: Integer -> Bool
           isPalindrome x = (reverse . show $ x) == (show x)

           isEmirp :: Integer -> Bool
           isEmirp x = isPrime . read . reverse . show $ x

main :: IO ()
main = print . show $ emirps
