import Data.List (nub)

(!!!) :: [a] -> [a] -> [a]
[] !!! _  = []
_  !!! [] = []
xs !!! (y:ys) = (xs !! y) : (xs !!! ys)

combinations :: Int -> [a] -> [[a]]
combinations _ [] = [[]]
combinations n xs | n <= 0 = [[]]
                  | n >= length xs = xs
                  | otherwise = id --TODO

allCombinations :: [a] -> [[a]]
allCombinations xs = allCombinations' 1 xs
                     where
                       allCombinations' n ys | n > length ys = [[]]
                                             | otherwise = (combinations n ys) ++ allCombinations' (n + 1) ys

subsetSums :: [Int] -> [[Int]]
subsetSums xs = filter isSubsetSums . allCombinations $ xs
                where
                  isSubsetSums :: [Int] -> Bool
                  isSubsetSums ys | null ys = False
                                  | otherwise = (sum . init $ ys) == last ys

mySet = [3, 4, 9, 14, 15, 19, 28, 37, 47, 50, 54, 56, 59, 61, 70, 73, 78, 81, 92, 95, 97, 99]
mySet2 = [1, 2, 3, 4, 6]

main :: IO ()
main = putStrLn . show . allCombinations $ mySet2
