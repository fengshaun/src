-- reverse every K nodes of a linked list
reverseNodes :: Int -> [a] -> [a]
reverseNodes _ [] = []
reverseNodes n xs = (reverse $ take n xs) ++ reverseNodes n (drop n xs)

main = print . reverseNodes 2 $ [1..5]
