-- These two problems seem to be on every list of programming interview questions:
-- 1) Remove all duplicate characters from a string. Thus, “aaabbb” becomes “ab” and “abcbd” becomes “abcd”.
-- 2) Replace all runs of consecutive spaces with a single space. Thus, “a.b” is unchanged and “a..b” becomes
--    “a.b”, using a dot to make the space visible.
-- Your task is to write the two requested functions. When you are finished, you are welcome to read or run a 
-- suggested solution, or to post your own solution or discuss the exercise in the comments below.

import Data.List (nub, nubBy)

removeDuplicates :: (Eq a) => [a] -> [a]
removeDuplicates = nub

removeSpaces :: [Char] -> [Char]
removeSpaces = foldl (\acc e -> if e == ' ' && last acc == ' ' then acc else acc ++ [e]) [] 

main = print $ removeSpaces "aabb   eue  eua     eua"
