-- Find the smallest prime fibonacci number greater than a given input number.
-- Add one to that fibonacci prime, find the factors of the result, and return 
-- the sum of the factors. For instance, if the input number is 10, the
-- smallest fibonacci prime greater than 10 is 13, the factors of 13 + 1 = 14
-- are 2 and 7, and their sum is 9.

import qualified Data.Numbers.Primes as P

fibs :: [Integer]
fibs = 0:1:zipWith (+) fibs (tail fibs)

smallestFibPrimeAfter :: Integer -> Integer
smallestFibPrimeAfter lowBound = head . dropWhile (\a -> not . P.isPrime $ a) . filter (>lowBound) $ fibs

factors :: Integer -> [Integer]
factors x = 1:factors' x 2
            where
              factors' n f | f >= (n `div` 2) = [n]
                           | n `mod` f == 0 = f:factors' (n `div` f) (f+1)
                           | otherwise = factors' n (f+1)

main :: IO ()
main = print . show . sum . factors . (+ 1) . smallestFibPrimeAfter $ 227000
