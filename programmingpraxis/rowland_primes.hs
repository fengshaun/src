import Data.Numbers.Primes

-- a(1) = 7, a(n) = a(n-1) + gcd (n, a(n-1))
seq1 :: [Int]
seq1 = seq1' 7 2
       where
         seq1' nPrev n = nPrev : seq1' (nPrev + (gcd n nPrev)) (n + 1)

seq2 :: [Int] -> [Int]
seq2 (x:[]) = []
seq2 (x:y:ys) = (y - x) : seq2 (y:ys)

seq3 :: [Int] -> [Int]
seq3 xs = filter (/= 1) xs

lpd :: Int -> Int
lpd n = head [x | x <- takeWhile (<= n) primes
                , n `mod` x == 0]

-- a(1) = lpd(6-1) = 5
-- a(2) = lpd(6-2+5) = 3
shortSeq :: [Int]
shortSeq = shortSeq' [] 1
      where
        shortSeq' ps n = x : shortSeq' (x:ps) (n + 1)
                         where
                           x = lpd $ 6 - n + sum ps

main = print . take 20 $ shortSeq
