import Data.List (nub)

combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations _ [] = [[]]
combinations n (x:xs) = map (x:) (combinations (n - 1) xs) ++ combinations n xs

allCombinations :: [a] -> [[a]]
allCombinations xs = allCombinations' 1 xs
                     where
                       allCombinations' n ys | n > length ys = [[]]
                                             | otherwise = (combinations n ys) ++ allCombinations' (n + 1) ys

subsetSums :: [Integer] -> [[Integer]]
subsetSums xs = filter isSubsetSums . allCombinations $ xs
                where
                  isSubsetSums :: [Integer] -> Bool
                  isSubsetSums ys | null ys = False
                                  | otherwise = (sum . init $ ys) == last ys

mySet = [3, 4, 9, 14, 15, 19, 28, 37, 47, 50, 54, 56, 59, 61, 70, 73, 78, 81, 92, 95, 97, 99]
mySet2 = [1, 2, 3, 4, 6]

main :: IO ()
main = putStrLn . show . length . subsetSums $ mySet
