/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <iostream>

#include <QTcpSocket>
#include <QMessageBox>

#include "server.h"

Server::Server(QObject *parent)
	: QTcpServer(parent)
{
	initialize();
}

Server::Server(quint16 port, QObject *parent)
	: QTcpServer(parent)
{
	initialize();
	listen(QHostAddress::Any, port);
}

void Server::initialize() {
	m_blockSize = 0;
	m_data = 10;
}

void Server::read() {
	QDataStream in(m_socket);
	in.setVersion(QDataStream::Qt_4_4);

	if (m_blockSize == 0) {
		if (m_socket->bytesAvailable() <  sizeof(quint16)) {
			return;
		}
		in >> m_blockSize;
	}

	if (m_socket->bytesAvailable() < m_blockSize)
		return;

	m_data = 10;
	in >> m_data;

	if (!(in.status() == QDataStream::Ok)) {
		std::cout << "Error while reading data" << std::endl;
		return;
	}

	if (m_socket->bytesAvailable() > 0) {
		read();
		return;
	}
	
	emit readDone(int(m_data));
}

void Server::send(int data) {
	m_data = quint8(data);

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_4_4);

	out << quint16(0) << m_data;
	out.device()->seek(0);
	out << quint16(block.size() - sizeof(quint16));

	m_socket->write(block);
}

void Server::error(QAbstractSocket::SocketError err) {
	if (err == QAbstractSocket::RemoteHostClosedError) {
		QMessageBox::information(NULL, tr("Player left"),
								 tr("Remote player has left the game"),
								 QMessageBox::Ok);
	}
	std::cout << "Error: " << qPrintable(m_socket->errorString()) << std::endl;
}

void Server::incomingConnection(int socketId) {
	m_socket = new QTcpSocket(this);
	m_socket->setSocketDescriptor(socketId);

	connect(m_socket, SIGNAL(readyRead()), this, SLOT(read()));
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));

	emit newConnection();
}
