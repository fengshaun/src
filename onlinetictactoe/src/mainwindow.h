/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

class QMenu;
class QAction;
class QToolBar;
class QDesktopWidget;
class QLabel;
class QTcpServer;
class QTcpSocket;
class QDockWidget;

class Server;
class Client;
class TicTacToe;
class CounterDockWidget;

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
	public:
		MainWindow(int = 300, int = 370);

	public slots:
		void setOptions();
		void connectToServer();
		void createServer();
		void center();
		void updateStatusBar();
		void clearStatusBar();
		void xWon();
		void oWon();

	protected:
		virtual void closeEvent(QCloseEvent *);
		
		void createActions();
		void createMenus();
		void createToolBars();
		void createStatusBar();
		void createDockWidgets();
		void initVariables();

		void saveSettings();
		void readSettings();

		int width();
		int height();

	protected slots:
		void updateTicTacToe(int);

	private:
		TicTacToe *m_tictactoe;
		
		QMenu *m_fileMenu;
		QMenu *m_optionsMenu;
		QMenu *m_viewMenu;
		QMenu *m_multiMenu;
		QMenu *m_helpMenu;

		QAction *m_newAction;
		QAction *m_singleAction;
		QAction *m_exitAction;
		QAction *m_settingsAction;
		QAction *m_enableCounterAction;
		QAction *m_connectToServerAction;
 		QAction *m_createServerAction;
		QAction *m_aboutAction;
		QAction *m_aboutQtAction;
		QAction *m_reportBugsAction;

		QLabel *m_statusLabel;

		QToolBar *m_fileToolBar;

		QDesktopWidget *desktop;
		CounterDockWidget *m_counterDockWidget;

		int m_width;
		int m_height;

		// Multiplayer
		Server *m_server;
		Client *m_client;
		quint16 m_port;
		QString m_host;
		quint16 m_serverPort;
		bool m_serverPortLoaded;

		bool m_isServer;
		bool m_isClient;

		// Preferences
		bool m_restoreSize;
		bool m_restorePosition;
		bool m_restoreCounter;
		bool m_saveServer;
		bool m_saveConnection;
		bool m_saveColors;

		QColor m_tttGridColor;
		QColor m_tttPenColor;
		QColor m_tttCrossColor;

		enum { MagicNumber = 0x6150 };
		
	private slots:
		void newGame();
		void about();
		void connected();
		void reportBugs();
};

#endif //MAINWINDOW_H
