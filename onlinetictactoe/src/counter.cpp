/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <iostream>

#include <QtGui>

#include "counter.h"

Counter::Counter(QWidget *parent)
	: QWidget(parent)
{
	m_oneValue = 0;
	m_twoValue = 0;
	setupUi();

	connect(this, SIGNAL(playerOneWinsChanged(int)), m_oneLcd, SLOT(display(int)));
	connect(this, SIGNAL(playerTwoWinsChanged(int)), m_twoLcd, SLOT(display(int)));
}

void Counter::setupUi() {
	m_oneLcd = new QLCDNumber(3);
	m_oneLcd->setSegmentStyle(QLCDNumber::Filled);
	m_twoLcd = new QLCDNumber(3);
	m_twoLcd->setSegmentStyle(QLCDNumber::Filled);
	m_oneLcd->display(0);
	m_twoLcd->display(0);

	m_oneLabel = new QLabel(tr("Player 1"));
	m_twoLabel = new QLabel(tr("Player 2"));
	m_oneLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	m_twoLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	m_leftLayout = new QVBoxLayout;
	m_rightLayout = new QVBoxLayout;
	m_mainLayout = new QBoxLayout(QBoxLayout::LeftToRight);

	m_leftLayout->addWidget(m_oneLabel);
	m_leftLayout->addWidget(m_oneLcd);

	m_rightLayout->addWidget(m_twoLabel);
	m_rightLayout->addWidget(m_twoLcd);

	m_mainLayout->addLayout(m_leftLayout);
	m_mainLayout->addLayout(m_rightLayout);
	

	setLayout(m_mainLayout);
}

void Counter::changeLayout(Qt::DockWidgetArea area) {
	if (area == Qt::LeftDockWidgetArea ||
		area == Qt::RightDockWidgetArea)
	{
		m_mainLayout->setDirection(QBoxLayout::TopToBottom);
	} else if (area == Qt::TopDockWidgetArea ||
			   area == Qt::BottomDockWidgetArea)
	{
		m_mainLayout->setDirection(QBoxLayout::LeftToRight);
	}
}

void Counter::playerOneWon() {
	m_oneValue++;
	emit playerOneWinsChanged(playerOneWins());
}

void Counter::playerTwoWon() {
	m_twoValue++;
	emit playerTwoWinsChanged(playerTwoWins());
}

int Counter::playerOneWins() {
	return m_oneValue;
}

int Counter::playerTwoWins() {
	return m_twoValue;
}

void Counter::reset() {
	m_oneValue = 0;
	m_twoValue = 0;
	m_oneLcd->display(0);
	m_twoLcd->display(0);
}

void Counter::setNetworkPlay(bool yes, int num) {
	if (yes) {
		if (num == 1) {
			m_oneLabel->setText(tr("You"));
			m_twoLabel->setText(tr("Remote player"));
		} else if (num == 2) {
			m_oneLabel->setText(tr("Remote player"));
			m_twoLabel->setText(tr("You"));
		}
	} else {
		m_oneLabel->setText(tr("Player 1"));
		m_twoLabel->setText(tr("Player 2"));
	}
}