#include <iostream>
#include <bitset>
#include <cmath>

#define LENGTH 1000000
std::bitset<LENGTH> primes;

void primes_sieve2() {
    primes.set(2); primes.set(3); primes.set(5); primes.set(7);

    for (int i = 11; i < LENGTH; i += 6) {
	primes.set(i);
	primes.set(i + 2);
    }

    for (int i = 5; i < int(std::sqrt(LENGTH)); i += 2) {
	if (primes[i]) {
	    for (int j = i * i; j < LENGTH; j += 2 * i) {
		primes.reset(j);
	    }
	}
    }
}

void primes_sieve() {
    primes.set(); primes.reset(0); primes.reset(1);

    for (int i = 2; i < LENGTH; i++) {
	if (primes[i]) {
	    for (int j = 2 * i; j < LENGTH; j += i) {
		primes.reset(j);
	    }
	}
    }
}

int main() {
    primes_sieve2();

    // for the sake of making sure the number is correct
    std::cout << primes.count() << '\n';
}
