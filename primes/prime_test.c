#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define LENGTH 1000000
short int primes[LENGTH];

void primes_sieve() {
    int i;
    int j;

    primes[2] = 1; primes[3] = 1; primes[5] = 1; primes[7] = 1;

    for (i = 11; i < LENGTH; i += 6) {
	primes[i] = 1;
	primes[i + 2] = 1;
    }

    for (i = 5; i < floor(sqrt(LENGTH)); i += 2) {
	if (primes[i]) {
	    for (j = i * i; j < LENGTH; j += 2 * i) {
		primes[j] = 0;
	    }
	}
    }
}

int main() {
    int i;
    int acc;

    primes_sieve();

    // making sure it's correct
    for (i = 0; i < LENGTH; i++) {
	if (primes[i]) {
	    acc++;
	}
    }
    printf("%d\n", acc);
}
