import java.lang.Math;

public class Prime {
    final int LENGTH = 1000000;
    boolean[] primes = new boolean[LENGTH];
    public void primes_sieve2() {
	primes[2] = true; primes[3] = true; primes[5] = true; primes[7] = true;

	for (int i = 11; i < LENGTH; i += 6) {
	    primes[i] = true;
	    primes[i + 2] = true;
	}

	for (int i = 5; i < (int)Math.sqrt(LENGTH); i += 2) {
	    if (primes[i]) {
		for (int j = i * i; j < LENGTH; j += 2 * i) {
		    primes[j] = false;
		}
	    }
	}
    }

    public static void main(String[] args) {
	Prime prime = new Prime(); prime.primes_sieve2();

	// for the sake of making sure the number is correct
	int acc = 0;
	for (int i = 0; i < prime.LENGTH; i++) {
	    if (prime.primes[i]) {
		acc++;
	    }
	}
	System.out.println(acc);
    }
}