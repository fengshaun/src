#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  thread_test.pl
#
#        USAGE:  ./thread_test.pl  
#
#  DESCRIPTION:  A test for perl threads
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  YOUR NAME (), 
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  05/28/2010 10:52:55 PM
#     REVISION:  ---
#===============================================================================

use 5.010;
use strict;
use warnings;

use threads;

sub do_sth {
	say "I'm doing something";
}

my $thr = threads->create(\&do_sth);
