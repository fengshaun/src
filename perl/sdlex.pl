use 5.010;
use warnings;

use SDL;

use Module::Load;
BEGIN { load "SDL::$_" for qw/App Surface Cursor Event Mixer Sound TTFont/ }

my $app = SDL::App->new(-title => "Kaboom!",
						-width => 800,
						-height => 600,
						-depth => 32,
						-flags => SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_HWACCEL,
);

#my $mixer = SDL::Mixer(-frequency => 44100,
#					   -channels => 2,
#					   -size => 1024,
#);
say "Press escape to quit";
&event_loop();

sub event_loop {
	my $event = SDL::Event->new;

	MAINLOOP:
	while (1) {
		while ($event->poll) {
			my $type = $event->type;

			last MAINLOOP if ($type == SDL_QUIT);
			last MAINLOOP if ($type == SDL_KEYDOWN and $event->key_name eq "escape");

			warn("keypress = " . $event->key_name) if ($type == SDL_KEYDOWN);
		}
		$app->delay(5);
	}
}
