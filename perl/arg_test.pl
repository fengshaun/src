#!/usr/bin/perl 

use 5.010;
use strict;
use warnings;
use Data::Dumper;

sub print_args {
	say Dumper @_;
}

print_args {hello => 1};
print_args 'good', hello => 1, world => 2;
print_args 'hello', 1, 'world', 2;
