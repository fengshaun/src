my $foo = 23;

sub do_stuff (&\$) {
	my ($block, $var) = @_;
	$block->($var);
}

do_stuff {
	my $var = shift;
	print $var . "\n";
} $foo;
