use 5.010;

sub greet {
	my $name = shift @_;
	state @names;
	my $msg = "Hi $name! ";

	if (scalar @names == 0) {
		$msg .= "You are the first one here";
	} else {
		$msg .= "I've seen: @names";
	}

	push @names, $name;
	say $msg;
}

greet "Fred";
greet "Barney";
greet "Wilma";
greet "Betty";
