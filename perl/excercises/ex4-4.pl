use 5.010;

sub greet {
	my $name = shift @_;
	state $prev_name = "";
	my $msg;

	$msg .= "Hi $name! ";

	if ($prev_name eq "") {
		$msg .= "You are the first one here";
	} else {
		$msg .= "$prev_name is also here!";
	}
	$prev_name = $name;
	print $msg, "\n";
}

greet "fred";
greet "barney";
