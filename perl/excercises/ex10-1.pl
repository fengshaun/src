use 5.010;

print @ARGV;

our $secret = int(1 + rand 100);
say "\$secret = $secret" if $DEBUG;

print "Guess? ";

while (<>) {
	chomp;

	if ($_ > $secret) { say "too high" }
	elsif ($_ < $secret) { say "too low" }
	else { say "you guessed right!"; last }
}

