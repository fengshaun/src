use 5.010;

my %people = (qw/fred flintstone barney rubble wilma flintstone/);

while(<>) {
	chomp;
	say "$people{$_}";
}
