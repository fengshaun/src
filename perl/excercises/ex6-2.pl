use 5.010;

my %words;

while (<>) {
	chomp;
	$words{$_}++;
}

say "";

@sk = sort keys %words;
say($_ . ": " . $words{$_}) for (@sk);
