use 5.010;

@people = qw/ fred betty barney dino wilma pebbles bamm-bamm /;

while (<STDIN>) {
	say $people[$_ - 1];
}
