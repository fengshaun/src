use 5.010;

print "radius? ";
chomp ($radius = <STDIN>);

$radius = 0 if ($radius < 0);
say "circumference is ", $radius * 2 * 3.141592654;
