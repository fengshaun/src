use 5.010;

sub total {
	my $t = 0;
	for (@_) { $t += $_ }
	$t;
}

print "total is: ", &total(qw/1 3 5 7 9/), "\n";
print "1..1000: ", &total(1..1000), "\n";
