sub average {
	my $t = 0;
	for (@_) { $t += $_ }
	$t / scalar @_;
}

sub above_average {
	$avg = average @_;
	grep { $_ > $avg } @_;
}

my @fred = &above_average(1..10);
print "\@fred is @fred\n";
print "(Sholud be 6 7 8 9 10)\n";
my @barney = &above_average(100, 1..10);
print "\@barney is @barney\n";
print "(Should be 100)\n";
