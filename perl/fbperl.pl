#!/usr/bin/perl

# TODO:
# - updating should just work(TM) 										DONE
# - 'u' binding for updating posts 										DONE
# - dynamic calculation of width of screen 								DONE
# - - no 80 and 20 width hard-coding 									DONE
# - - no Text::Wrap::columns hard-coding 								DONE
# - remove <attachment_type> from post dialog 							DONE
# - from -> to target discovery 										DONE
# - - will go into dialog 												DONE
# - width of Poster column = length of the longest name 				DONE
# - put '>' in front of rows instead of highlighted cursor 				DONE
# - use Curses::UI::POE													DONE
# - add POE mainloop													DONE
# - update on intervals													DONE
# - checking messages in a separate page								DONE
# - 'via ThisApp' in dialog												DONE
# - Add unread message show capability									DONE
# - Scrolling through posts with PgUp/PgDown
# - Proper login system for FBCurse
# - remove highlighted cursor
# - making posts bold correction
# - colorify fbperl.pl
# - config file (colors, access_token)
#
# - proper pod documentation
# - proper WWW::Facebook::Graph module
# - upload to CPAN
# - - both script and module
#
# extended permission link
# https://graph.facebook.com/oauth/authorize?client_id=112366752125381&redirect_uri=http://www.bing.com/&scope=read_mailbox
#
# FQL link
# https://api.facebook.com/method/fql.query?format=JSON&query=QUERY
# QUERY is in the form
# SELECT [fields] FROM [table] WHERE [condition]


use strict;
use warnings;

use lib '/home/armin/fbterm/lib';

use FacebookGraph;
use Data::Dumper;
use POE;
use Curses;
use Curses::UI;
use Curses::UI::POE;
use Curses::UI::Common qw/CUI_TAB/;
use Term::ReadKey qw/GetTerminalSize/;
use Term::ANSIColor;
use Log::Log4perl qw/:easy/;
use Text::Wrap;
use Browser::Open qw/open_browser/;
use List::Util qw/max/;
use Getopt::Std;
use Config::Auto;

our $access_token = '112366752125381|f2e613ef0e9a03c9108b3282-595033059|VKfwvVP3wuksH_uA1MJzwJP8Eww.';

my %opts;
getopts('h', \%opts);

if ($opts{'h'}) {
	print <<END;
Usage: perl fbperl.pl

these bindings work while fbperl.pl is running:
   j/DOWN_ARROW    go down one post
   k/UP_ARROW      go up one post
   l/RIGHT_ARROW   go left one column
   h/LEFT_ARROW    go right one column
   o               open post in dialog
   g               go to post in browser
   q               quit fbperl.pl
END
	exit(0);
}

### setting up environment
# parse ~/.fbtermrc
my $conf = Config::Auto::parse;
Log::Log4perl->easy_init({ file => '> /home/armin/err_fbperl.log', level => $DEBUG });
$Text::Wrap::separator = "|";

our $ui = Curses::UI::POE->new(-color_support => 1,
							   -cursor_mode => 0,
							   -mouse_support => 0,
);

### curses ui stuff
# keeps track of focused window,
# 0 -> mainwin
# 1 -> profilewin
my $focused_win = 0;

# main window
# contains posts
my @win;
$win[0] = $ui->add(
	'mainwin', 'Window',
	-title => "FBCurse -- Home",
	-border => 1,
);

# information window
# contains events, messages, etc.
$win[1] = $ui->add(
	'profilewin', 'Window',
	-title => "FBCurse -- Profile",
	-border => 1,
);

my $profile_text = $win[1]->add(
	'profile_text', 'TextViewer',
	-text => "",
);

# column labels
my %labels = (
	cell1 => "Poster",
	cell2 => "Message");

my $maingrid = $win[0]->add(
	'maingrid', 'Grid',
	-editable => 0,
	-rows => 1,
	-columns => scalar keys %labels,
	-onrowfocus => sub {
		my $row = shift;
		my $name = $row->get_value('cell1');
		$name =~ s/^ />/;
		$row->set_value('cell1', $name);
	},
	-onrowblur => sub {
		my $row = shift;
		my $name = $row->get_value('cell1');
		$name =~ s/^>/ /;
		$row->set_value('cell1', $name);
	}
);

for (keys %labels) {
	$maingrid->set_label($_, $labels{$_});
}

# keeps track of each row's 'personal' function
my %callbacks;

my $graph = FacebookGraph->new(access_token => $access_token);

sub update {
	$ui->status("updating...");

	my ($term_width, $term_height) = GetTerminalSize;

	$ui->status("retrieving posts...");
	my $content = $graph->get(connection => ['me', 'home'], limit => 30);

	my $longest_name_width = max(map { length $_->{'from'}{'name'} } @{ $content->{'data'} });
	my $width = $term_width - $longest_name_width - 2;

	$Text::Wrap::columns = $width - 12;

	$maingrid->set_cell_width("cell1", $longest_name_width + 1);
	$maingrid->set_cell_width("cell2", $width);

	# keeps track of current row
	my $c_row = 1;

	$ui->status("updating posts...");
	for my $post (@{ $content->{'data'} }) {
		DEBUG "adding post: $post";
		# $msg is in the form:
		# <type>: <message>
		my $t = $post->{'type'} // "";
		my $m = $post->{'message'} // "";
		my $msg = "$t: $m";

		# replacing \n with a single space so we can control all the
		# wrapping and formatting business
		$msg =~ s/\n/ /g;

		# in the rare case that someone puts '|' in their status.
		# this has actually happened and it partially broke the main page
		$msg =~ s/\|//;

		# when there is no <message>, we just say what the poster has
		# posted
		$msg = "Posted a @{[ $post->{'type'} ]} (@{[ $post->{'name'} ]})" unless $post->{'message'};

		# and here comes the formatting
		# $Text::Wrap::{columns,separator} comes into place here
		my @lines = split /\|/, wrap('', '', $msg);

		# this is where we construct the callback that makes the
		# post dialog.  open_dialog(ARRAYREF, HASHREF) returns
		# a subroutine to be called later.  open_post(HASHREF) opens
		# the post in a browser.
		$callbacks{"row$c_row"} = {
			dialog => &open_dialog($post, @lines),
			link   => &open_post($post),
		};

		# add a row and populate it for every line of the
		# post and for every post
		$maingrid->add_row("row$c_row") unless $maingrid->get_row("row$c_row");
		$maingrid->set_values("row$c_row",
			cell1 => " " . $post->{'from'}{'name'},
			cell2 => shift @lines);

		++$c_row >= $term_height && last;

		# doing the right formatting for every line of wrapped message
		for (@lines) {
			$maingrid->add_row("row$c_row") unless $maingrid->get_row("row$c_row");
			$maingrid->set_values("row$c_row",
				cell1 => "", # clearing the first cell
				cell2 => $_);
			++$c_row >= $term_height && last;
		}

		# a blank row for clarity
		$maingrid->add_row("row$c_row") unless $maingrid->get_row("row$c_row");
		$maingrid->set_values("row$c_row",
			cell1 => "",
			cell2 => "");

		++$c_row >= $term_height && last;
	}

	#----------------------------------------------------------------------------
	# updating profile (messages, events, etc.)
	#----------------------------------------------------------------------------
	$ui->status("updating profile...");
	my $query = $graph->fql("SELECT thread_id, unread, snippet, snippet_author FROM thread WHERE folder_id=0 LIMIT 5");
	my $txt = "Unread Messages:\n";
	$txt   .= "----------------\n\n";

	for (@$query) {
		if ($_->{unread}) {
			(my $snippet = $_->{snippet}) =~ s/\n/ /g;

			$txt .= "From:    " . $graph->get(connection => [$_->{snippet_author}])->{'name'} . "\n";
			$txt .= "Message: " . $snippet . "\n";
			$txt .= "\n";
		}
	}

	my $events = $graph->get(connection => ['me', 'events'], limit => 5);
	$txt .= "New Events:\n";
	$txt .= "-----------\n\n";
	
	for (@{ $events->{data} }) {
		$txt .= "here goes an event\n";
	}

	$profile_text->text($txt);

	$maingrid->draw;
	$ui->nostatus;

	#-----------------------------------------------------------------------------
	# colorizing all the cells
	#-----------------------------------------------------------------------------
	for ($c_row = 1; $maingrid->get_row("row$c_row"); $c_row++) {
		my $row = $maingrid->get_row("row$c_row");
	}
}

sub open_dialog {
	# opens each row's dialog when it's called
	# my $callback_ref = open_dialog(\@my_message_lines, $my_post_hash);
	# ... later on ...
	# ... bindings get pressed ...
	# ... stars move ...
	# $callback->(); opens the dialog
	my ($post, @lines) = @_;

	# what is eventually displayed in the dialog
	my $message;

	# the origin of message, plus the target
	$message .= "@{[ $post->{'from'}{'name'} ]} wrote ";
	$message .= "to @{[ $post->{'to'}{'data'}[0]{'name'} ]} " if $post->{'to'};
	$message .= "via @{[ $post->{'attribution'} ]}" if $post->{'attribution'};
	$message .= "\n";

	# the message itself, as seen in the main page
	# remove <message_type> at the beginning of the message
	(my $first_line = shift @lines) =~ s/^.*: //g;
	$message .= $first_line;
	$message .= "\n";

	# and slap the rest of the @lines at the end
	$message .= join "\n", @lines;
	$message .= "\n\n";

	# the video/photo/etc. link with its name in the form:
	# <link_type>: <link_name>
	#              <link>
	# video: My awesome movie
	#        http://www.myawesomemovie.com/my_movie.ogv
	# and, I tried to align the <link_name> and <link> by " " x length(<type_name>) + 2.
	# + 2 for considering ": " at the end of <link_type>
	$message .= "Links:\n@{[ $post->{'type'} // '' ]}: @{[ $post->{'name'} // '' ]}\n" . (" " x (length($post->{'type'} // '') + 2 )) . "@{[ $post->{'link'} // '' ]}\n\n" if $post->{'link'};

	# then the statistics of the post:
	# <num> comments and <num> likes
	my $comments = @{ $post->{'comments'}{'data'} or [] };
	my $likes = $post->{'likes'} // 0;
	$message .= "$comments comment(s) and $likes likes(s)\n";

	# skip a line
	$message .= "\n";

	# now it's time to show all the comments
	# notice that <from_name>: <comment_message> is a string itself, and
	# there is a gap between every new comment
	$message .= "Comments:\n";
	$message .= join("\n\n", map { "@{[ $_->{'from'}{'name'} ]}: @{[ $_->{'message'} ]}" } @{ $post->{'comments'}{'data'} });

	# and the most neglected part, but the main business
	# returns a subroutine to be called later, which does
	# the right thing
	sub { $ui->dialog($message) };
}

sub open_post {
	# gets a $post hashref and opens the browser to the post
	# url.  Note that $post->{'actions'}[0]{'link'} is the url
	# to the post.
	my $post = shift;
	my $url = $post->{'actions'}[0]{'link'};
	#sub { open_browser($url) };
	sub { system "uzbl-browser $url &> /dev/null &" };
}

update;

### setting bindings
# main page
$win[0]->set_binding(sub { 
		# go down to the next row
		# that means going down until cell1 is occupied
		DEBUG "going down";
		do {
			$maingrid->next_row;
		} until ($maingrid->getfocusrow->get_value("cell1"));
	}, 'j', Curses::KEY_DOWN);

$win[0]->set_binding(sub {
		# go up to the previous post
		# that means going up until cell1 is occupied
		DEBUG "going up";
		do {
			$maingrid->prev_row;
		} until ($maingrid->getfocusrow->get_value("cell1"));
	}, 'k', Curses::KEY_UP);

$win[0]->set_binding(sub {
		# pressing this binding activates the row-specific
		# function we set when we populated the maingrid
		DEBUG "opening post in dialog...";
		my $row_id = $maingrid->getfocusrow->{'-id'};
		$callbacks{"$row_id"}{"dialog"}->();
	}, 'o');

$win[0]->set_binding(sub {
		# opening the post in the default browser
		DEBUG "opening post in browser...";
		my $row_id = $maingrid->getfocusrow->{'-id'};
		$callbacks{"$row_id"}{"link"}->();
	}, 'g');

$win[0]->set_binding(sub {}, 'l', Curses::KEY_RIGHT);
$win[0]->set_binding(sub {}, 'h', Curses::KEY_LEFT);


$ui->set_binding(sub { 
		DEBUG "changing windows";
		$focused_win ? ($focused_win = 0) : ($focused_win = 1);
		$win[$focused_win]->focus;
	}, CUI_TAB);

$ui->set_binding(sub { exit(0) }, 'q');
$ui->set_binding(sub { update }, 'u');

$ui->set_timer("update_timer", \&update, 5 * 60);

### mainloop
$ui->mainloop;

