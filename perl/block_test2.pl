use Data::Dumper;

sub do_stuff {
    print 'before';
    $_[1]->();
    print 'after';
}

do_stuff 'blue', sub { print "hello" };
