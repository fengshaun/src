#!/usr/bin/perl

use 5.012;

our $browser = "uzbl-browser ";

sub run_dmenu {
    my $dmenu = "dmenu -i -p '>' -fn 'Bauhaus:pixelsize=15' -nb '#333333' -nf '#818181' -sb '#1793d1' -sf '#ffffff'";
    my @supported_cmds = qw/define search open/;

    my $all_cmds = `dmenu_path` . join("\n", @supported_cmds);
    my $dmenu_cmd = "echo \"$all_cmds\" | $dmenu";

    # this is the returned command
    my $cmd = `$dmenu_cmd`;
    return 1 if ! $cmd;

    # return the command in array form
    my @cmd = split / /, $cmd;
}

sub process_output {

    my $cmd = shift;
    chomp(my @args = @_);

    given($cmd) {
        when(/define/) { &process_define_cmd(@args) }
        when(/search/) { &process_search_cmd(@args) }
        when(/open/)   { &process_open_cmd(@args) }
        default { exec($cmd . join " ", @args) }
    }
}

sub process_define_cmd {
    #TODO: use the API
    my $word = join " ", @_;
    exec($browser . "\"http://dictionary.reference.com/browse/$word\"");
}

sub process_search_cmd {
    my $query = join "+", @_;
    exec($browser . "\"http://duckduckgo.com/?q=$query\"");
}

sub process_open_cmd {
    my $url = shift;

    given($url) {
        when(/^mail$/) { exec("urxvt -e mutt") }
        when(/^news$/) { exec("urxvt -e canto") }
        when(/^facebook$/) { exec("firefox \"http://facebook.com\"") }
    }
}

process_output(run_dmenu);
