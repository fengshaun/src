#!/usr/bin/env python

# Copyright (c) 2009 Armin Moradi <amoradi at fedoraproject dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

def help():
    print '''Syntax: parse_spec.py --{name,version,release} name-of-specfile
Returns {name,version,release} of name-of-specfile

Really helpful help message, I know :)'''

if len(sys.argv) < 3:
    help()
    sys.exit(1)

option = sys.argv[1][2:]  # removing the '--' in the beginning

filename = "/home/" + os.getlogin() + "/rpmbuild/SPECS/" + sys.argv[2] + ".spec"
specfile = open(filename, 'r')

options = ["name", "version", "release"]

for i in options:
    if i == option:
        for line in specfile:
            if line.split(' ')[0].lower() == option + ":":
                name = line.replace(' ', '').split(':')[1].strip()
                if option == "release":
                    # in case of %{?dist}
                    name = name.split('%')[0] + os.popen("rpm -E '%{?dist}'").read().rstrip()
                print name
                sys.exit(0)
