#!/usr/bin/python
# Copyright (c) Armin Moradi 2010
# GPLv3
# a one liner (not nice, I know) to check the unread count for bloglines account
# commandline options:
#     user: the email address of the user

import sys; import urllib; print urllib.urlopen("http://rpc.bloglines.com/update?user=%s" % sys.argv[1]).read().strip("|").split("|")[0]


