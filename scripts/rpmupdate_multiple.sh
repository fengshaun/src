#!/bin/bash

# Copyright (c) 2009 Armin Moradi <amoradi at fedoraproject dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" = "--help" ]; then
    echo -e "$0 'options to be passed to updatepkg' spec1 spec2 spec3 ..."
    exit 0
fi

options=$1
shift;

while [ "$#" -gt "0" ]; do
    updatepkg.sh $options $1; shift;
done