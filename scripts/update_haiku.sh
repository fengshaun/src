# Downloads and installs the latest nightly build from haiku-files.org
# Copyright (c) Armin Moradi <feng.shaun at gmail dot com>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.
#
# requires: gksu

# The partition in which the image is installed
export HAIKU_DESTINATION="/dev/sda7"

# DO NOT EDIT BEYOND THIS POINT
# UNLESS YOU KNOW WHAT YOU ARE DOING

help() {
	echo "Usage: $0 [Option]"
	echo "Downloads and installs the haiku raw image to an specified partition"
    echo 
    echo "Options:"
    echo "    --help         shows this help message"
    echo "    --dest DEST    set where the image will be installed.  Should be in form of /dev/sdaX."
	exit 1
}

for arg; do
    case $arg in
        "--help") help; exit 1;;
        "--dest") shift 1; export HAIKU_DESTINATION=$1;;
    esac
done

echo "WARNING: CONTENTS OF $HAIKU_DESTINATION WILL BE ERASED.  PRESS CTRL+C NOW IF YOU DO NOT WANT THIS"

sleep 5

# Determining latest revision available as image
# "scrapes" the rev number from haiku-files.org/raw
echo -n "retrieving latest revision number: "
wget http://haiku-files.org/raw
export HAIKU_LATEST_REV=`cat index.html | grep -E "<a href=\"http://haiku-files.org/raw/haiku-nightly-r[0-9]+-x86gcc4hybrid-raw.tar.xz\"" | sed 's/^[ \t]*//g' | grep -Eo '[0-9]{5,}' | head -1`
echo $HAIKU_LATEST_REV

sleep 2

dl_mkbootable() {
    wget http://stefanschramm.net/dev/makebootabletiny/makebootabletiny.c
    gcc makebootabletiny.c -o makebootabletiny
    rm makebootabletiny.c
}

dl_image() {
    wget "http://haiku-files.org/raw/haiku-nightly-r${HAIKU_LATEST_REV}-x86gcc4hybrid-raw.tar.xz"
    tar -xvf "haiku-nightly-r${HAIKU_LATEST_REV}-x86gcc4hybrid-raw.tar.xz"
    
    rm "haiku-nightly-r${HAIKU_LATEST_REV}-x86gcc4hybrid-raw.tar-xz"
}

install_image() {
    gksu dd if=haiku-nightly.image of=$HAIKU_DESTINATION
    gksu ./makebootabletiny /dev/sda7
}

cleanup() {
    rm haiku-nightly.image
    rm makebootabletiny
}

echo "Downloading makebootabletiny.c"
dl_mkbootable

echo "Downloading Haiku r$HAIKU_LATEST_REV gcc4hybrid raw image"
dl_image

echo "Installing Haiku image to $HAIKU_DESTINATION"
install_image

echo "Cleaning up"
cleanup

echo "Done."
