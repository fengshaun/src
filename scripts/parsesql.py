#!/usr/bin/python3
#
# parses postgres and strips out comments
# this is also a challenge in how much work I can fit in one line
#
# input:
#
# --- The Schema where bob's stuff is.
# CREATE SCHEMA bob;
# 
# --- This is my table
# --- It contains stuff
# CREATE TABLE .* bob.tablename
# (
#    --- unique identifier for row
#    id big serial,
#    --- This column is mostly empty
#    money integer,
#    --- When money was added
#    time_stamp TIMESTAMP,
# )
#
# output:
#
# comment on schema bob 'The schema where bob\'s stuff is.';
# comment on table bob.tablename 'This is my table It contains stuff';
# comment on column bob.tablename.id 'unique identifier for row';
# comment on column bob.tablename.money 'This column is mostly empty';
# comment on column bob.tablename.time_stamp 'When money was added';

import sys, os
import re

# boilerplate
re_schema = re.compile(r"^create (?P<type>schema) (?P<name>(\w+))\s*;", re.IGNORECASE)
re_table  = re.compile(r"^create (?P<type>table) [\w\s]+ (?P<name>(\w+\.\w+))", re.IGNORECASE)
re_column = re.compile(r"^(?P<type>\s+)(?P<name>\w+)\s+[().\n\w\s]+[,)]", re.IGNORECASE)
re_comment = re.compile(r"\s*---\s+(?P<comment>.*)")
re_end = re.compile(r"(?P<name>\s*)\)(\s+(inherits|with|on|tablespace))?.*(?P<type>;)", re.IGNORECASE)

IGNORED_KEYWORDS = ["primary", "constraint", "references", "unique"]

# global variables because reasons
last_table = ""
in_table = False

# python stdlib surprisingly doesn't have this
def filter_until(f, l):
    for i in l:
        if f(i):
            yield i
        else:
            raise StopIteration

def get_prev_comments(lines, i):
    try:
        return ["%s" % m.group("comment") for m in filter_until(lambda x: x is not None, [re_comment.match(line)
                                          for line in lines[i-1 if i > 0 else 0::-1]])][::-1]
    except AttributeError:
        return ""

def process_comments(m, lines, i):
    global last_table, in_table

    if m.group("type").lower() == "table":
        last_table = m.group("name").lower()
        in_table = True
    elif m.group("type").lower() == ";":
        last_table = ""
        in_table = False
        return None
    elif m.group("type").lower() != "schema" and (not in_table or m.group("name").lower() in IGNORED_KEYWORDS):
        return None

    last_table = m.group("name").lower() if m.group("type").lower() == "table" else last_table

    print("comment on %s %s is '%s';" % (m.group("type").lower() if m.group("type").lower() in ["schema", "table"]
                                                                 else "column",
                                         "".join([last_table + "." if m.group("type").lower() not in ["schema", "table"] else "",
                                                  m.group("name")]),
                                         re.sub(r"'", r"''", " ".join(get_prev_comments(lines, i)))))

def get_comments(lines):
    [process_comments(m, lines, i) for i in range(len(lines))
                                   for m in filter(lambda x: x is not None,
                                                   [mre.match(lines[i]) for mre in [re_table, re_schema, re_column, re_end]])]

def main():
    [get_comments(list(map(lambda x: x.rstrip(),
                           open(fname, 'r').readlines())))
     for fname in sys.argv[1:]]

if __name__ == "__main__":
    main()

