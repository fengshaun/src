#!/usr/bin/perl -w
# randomizes all the files in a directory
# (c) Armin Moradi <amoradi at fedoraproject dot org>

use strict;
use warnings;

use List::Util qw/shuffle/;
use File::Basename qw/basename/;
use Cwd qw/getcwd/;
use List::Util qw/max/;

# are we really sure?  This operation is non-reversible
my $dir = getcwd;
print "randmizing all filename in <$dir> (y/n)? ";
chomp(my $answer = <STDIN>);
if ($answer ne "y") {
    exit 1;
}

# the file list
my @files = <*>;

# for some odd reason, map doesn't work properly!
# padding the numbers to the proper digit
my @names = shuffle (1..scalar @files);
my @names2;

# getting the length of the biggest number in @names
my $len = scalar (split //, (sprintf "%d", (max @names)));

for (@names) {
    push @names2, sprintf "%0${len}d", $_;
}

# adding the extensions to the new filenames
my %all_names;
@all_names{@files} = @names2;

for my $key (keys %all_names) {
    $key =~ /(\.[^.]+)$/;
    $all_names{$key} .= $1;
}

# doing the actual renaming
while (my ($old, $new) = each %all_names) {
    print "$old => $new\n";
    rename $old, $new;
}

