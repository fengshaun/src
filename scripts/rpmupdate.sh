#!/bin/bash

# Copyright (c) 2009 Armin Moradi <amoradi at fedoraproject dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SRCDIR=$HOME/src/kde
SPECNAME=""
DESTDIR=""
SOUND=""
SERVER="fedorapeople.org"
USER="armin"
SEND=0

configs=(fedora-10-x86_64 fedora-10-i386 fedora-rawhide-x86_64 fedora-rawhide-i386)

# semi-sane defaults (I have lost my sanity atm)
update=1
update_svn=1
update_git=0
compress=1
refresh=1

function help {
    # TODO: make it a little more helpful!
    echo "Usage: $0 [OPTION] specfile"
    echo "Options:"
    echo -e "\t--help\t\tthis help"
    echo -e "\t--specfile [FILE]\tthe specfile to be used"
    echo -e "\t--update-git\tthe source should be updated using git"
    echo -e "\t--update-svn\tthe source should be updated using svn"
    echo -e "\t--srcdir [DIR]\twhere the source file is located"
    echo -e "\t\t\tthe source will be located in srcdir/name"
    echo -e "\t--destdir [DIR]\twhere the built pkgs go"
    echo -e "\t--sound [FILE]\tplay sound when finished"
}

# options
while [ "$#" -gt 0 ]; do
    case $1 in
        --update-svn)
            # it is updated
            update=1
            update_svn=1
            update_git=0
            compress=1  # it should be compressed
        ;;
        --update-git)
            update=1
            update_git=1
            update_svn=0
            compress=1
        ;;
        --specfile)
            shift
            SPECNAME=$1
        ;;
        --no-compress)
            compress=0
        ;;
        --compress)
            compress=1
        ;;
        --refresh)
            refresh=1
        ;;
        --srcdir)
            shift
            SRCDIR=$1
        ;;
        --destdir)
            shift
            DESTDIR=$1
        ;;
        --server)
            shift
            SERVER=$1
        ;;
        --user)
            shift
            USER=$1
        ;;
        --send)
            SEND=1
        ;;
        --help)
            help
            exit 1
        ;;
        --sound)
            shift
            SOUND=$1
        ;;
        *)
            SPECNAME=$1
        ;;
    esac
    
    shift

done

if [ "$SPECNAME" = "" ]; then
    help
    exit 1
fi

# parsing name
NAME=`parse_spec.py --name $SPECNAME`
VERSION=`parse_spec.py --version $SPECNAME`
RELEASE=`parse_spec.py --release $SPECNAME`

if [ "$DESTDIR" = "" ]; then
    DESTDIR=$HOME/builtpkgs/$NAME
else
    DESTDIR=$DESTDIR/$NAME
fi

if [ "$NAME" = "" ]; then echo "Error: NAME could not be parsed"; exit 1; fi
if [ "$VERSION" = "" ]; then echo "Error: VERSION could not be parsed"; exit 1; fi
if [ "$RELEASE" = "" ]; then echo "Error: RELEASE could not be parsed"; exit 1; fi

# preparation
echo "preparing build environment"
cd $HOME

for i in ${configs[@]}; do
    mkdir -p $DESTDIR/$i
    cd $DESTDIR/$i
    if [ "$refresh" = 1 ]; then
        rm -rf *
    else
        rm -rf *.log
    fi
    cd ..
done



if [ $update = 1 ]; then
    cd $SRCDIR/$NAME
    if [ "$?" -ne "0" ]; then exit; fi
    if [ $update_git = 1 ]; then
        echo "updating source using git"
        git pull &> $DESTDIR/git.log
        if [ "$?" -ne "0" ]; then echo "Error: Could not update source using git"; exit 1; fi
    elif [ $update_svn = 1 ]; then
        echo "updating source using svn"
        if [ ! -d ".svn" ]; then echo "Error: Could not update $SRCDIR using svn"; exit 1; fi
        svn up &> $DESTDIR/svn.log
        if [ "$?" -ne "0" ]; then echo "Error: Could not update $SRCDIR using svn"; exit 1; fi
    fi
    cd $SRCDIR
fi

# at this moment, the source is checked out

if [ $compress = 1 ]; then
    # making it a tarball now
    echo "packing source"
    cd $SRCDIR
    cp -r $NAME $HOME/rpmbuild/$NAME-$VERSION
    cd $HOME/rpmbuild
    tar -czf $NAME-$VERSION.tar.gz $NAME-$VERSION
    mv $NAME-$VERSION.tar.gz $HOME/rpmbuild/SOURCES
    rm -rf $HOME/rpmbuild/$NAME-$VERSION
fi

# now the source is packed and is in
# proper place.  We assume either the
# packager, or the script put it in
# the right place -> ~/rpmbuild/SOURCES
#
# patches have to be put in place separately

echo "creating src.rpm"
rpmbuild -bs SPECS/$SPECNAME.spec > /dev/null

if [ "$?" -ne "0" ]; then echo "Error: $SPECNAME does not exist"; exit 1; fi

for config in ${configs[@]}; do
    echo -n "building $NAME $config..."
    mock -r $config $HOME/rpmbuild/SRPMS/$NAME-$VERSION-$RELEASE.src.rpm &> $DESTDIR/$config/mock.log
    if [ "$?" -ne "0" ]; then
        cp /var/lib/mock/$config/result/*.log $DESTDIR/$config/
        echo "failed"
        echo "Please refer to $DESTDIR/$config/mock.log to know why"
        exit 1;
    fi
    cp /var/lib/mock/$config/result/*.rpm $DESTDIR/$config

    if [ "$SEND" = 1 ]; then
        sftp $USER@$SERVER <<EOF
put $DESTDIR/$config/*.rpm public_html/rpms
bye
EOF
    fi

    echo "done"
done

if [ ! "$SOUND" = "" ]; then
    play $SOUND &> /dev/null
fi