#!/usr/bin/perl -w

system "wget http://fedoraproject.org/wiki/Packaging/ReviewGuidelines -q -P /tmp/";

open REVIEW, "/tmp/ReviewGuidelines" or die "Can't open ReviewGuidelines: $!";

while (<REVIEW>) {
    if (m{<li> <b>(MUST|SHOULD)</b>: (.*)}) {
	my $condition = "$1: $2\n";
	$condition =~ s/<.*?>//g;
	$condition =~ s/&nbsp;/ /g;
	$condition =~ s/\[.*?\]//g;
	print "[] $condition";
    }
}

unlink "/tmp/ReviewGuidelines";
