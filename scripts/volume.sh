#!/bin/bash
vol=`amixer get Master | grep 'Mono: Playback' | grep -Eo '[0-9]+%'`
mute=`amixer get Master | grep 'Mono: Playback' | grep -Eo '\[(on|off)]'`

if [[ "$mute" == "[off]" ]]; then
   echo "mute"
else
    echo $vol
fi
