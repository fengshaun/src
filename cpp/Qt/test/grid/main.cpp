#include <iostream>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QWidget>
#include <QObject>
#include <QVBoxLayout>
#include <QGridLayout>

class Board : public QWidget
{
	public:
		Board( QWidget * = 0 );
};

Board::Board( QWidget *parent ) : QWidget( parent ) {
	QPushButton *quit = new QPushButton( tr( "Quit" ) );
	connect( quit, SIGNAL( clicked() ), qApp, SLOT( quit() ) );

	QGridLayout *grid = new QGridLayout;
	for( int row = 0; row < 3; row++ ) {
		for( int column = 0; column < 3; column++ ) {
			QPushButton *button = new QPushButton( "XO" );
			grid->addWidget( button, row, column );
		}
	}

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget( quit );
	layout->addLayout( grid );
	setLayout( layout );
	setWindowTitle( "Grid Layout" );
}

int main( int argc, char *argv[] ) {
	QApplication app( argc, argv );

	Board board;
	board.show();
	return app.exec();
}
