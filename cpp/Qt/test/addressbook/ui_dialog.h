/********************************************************************************
** Form generated from reading ui file 'dialog.ui'
**
** Created: Fri Jul 25 14:35:29 2008
**      by: Qt User Interface Compiler version 4.4.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *nameLine;
    QLineEdit *phoneLine;
    QPushButton *addBtn;
    QPushButton *removeBtn;
    QPushButton *searchBtn;

    void setupUi(QDialog *Dialog)
    {
    if (Dialog->objectName().isEmpty())
        Dialog->setObjectName(QString::fromUtf8("Dialog"));
    Dialog->resize(428, 146);
    label = new QLabel(Dialog);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(20, 20, 51, 18));
    label_2 = new QLabel(Dialog);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setGeometry(QRect(20, 60, 61, 18));
    nameLine = new QLineEdit(Dialog);
    nameLine->setObjectName(QString::fromUtf8("nameLine"));
    nameLine->setGeometry(QRect(80, 20, 211, 29));
    phoneLine = new QLineEdit(Dialog);
    phoneLine->setObjectName(QString::fromUtf8("phoneLine"));
    phoneLine->setGeometry(QRect(80, 60, 211, 29));
    addBtn = new QPushButton(Dialog);
    addBtn->setObjectName(QString::fromUtf8("addBtn"));
    addBtn->setGeometry(QRect(310, 20, 101, 28));
    removeBtn = new QPushButton(Dialog);
    removeBtn->setObjectName(QString::fromUtf8("removeBtn"));
    removeBtn->setGeometry(QRect(310, 60, 101, 28));
    searchBtn = new QPushButton(Dialog);
    searchBtn->setObjectName(QString::fromUtf8("searchBtn"));
    searchBtn->setGeometry(QRect(310, 100, 101, 28));

    retranslateUi(Dialog);

    QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
    Dialog->setWindowTitle(QApplication::translate("Dialog", "Address book", 0, QApplication::UnicodeUTF8));
    label->setText(QApplication::translate("Dialog", "Name:", 0, QApplication::UnicodeUTF8));
    label_2->setText(QApplication::translate("Dialog", "Phone #:", 0, QApplication::UnicodeUTF8));
    nameLine->setText(QString());
    phoneLine->setText(QString());
    addBtn->setText(QApplication::translate("Dialog", "Add", 0, QApplication::UnicodeUTF8));
    removeBtn->setText(QApplication::translate("Dialog", "Remove", 0, QApplication::UnicodeUTF8));
    searchBtn->setText(QApplication::translate("Dialog", "Search", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(Dialog);
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
