/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include "mainwindow.h"

MainWindow::MainWindow( QWidget * parent ) : QDialog( parent ) {
	setupUi( this );

	connect( addBtn, SIGNAL( clicked() ), this, SLOT( add() ) );
	connect( removeBtn, SIGNAL( clicked() ), this, SLOT( remove() ) );
	connect( searchBtn, SIGNAL( clicked() ), this, SLOT( search() ) );
}

MainWindow::~MainWindow() { }

void MainWindow::add() {
	std::cout << "Add button pressed" << std::endl;
	QString name = nameLine->text();
	QString phone = phoneLine->text();

	Contact contact( name, phone );
	disk.push_back( &contact );
}

void MainWindow::search() {
	std::cout << "Search button pressed" << std::endl;
	QString name = nameLine->text();

	for ( unsigned int i = 0; i < disk.size(); i++ ) {
		if ( disk[ i ]->name() == name ) {
			std::cout << "contact found" << std::endl;
		}
	}
}

void MainWindow::remove() {
	std::cout << "Remove button pressed" << std::endl
			<< "has to be implemented" << std::endl;
}

void MainWindow::getName() {
}

void MainWindow::getNumber() {
}