/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include "mainwindow.h"

MainWindow::MainWindow() : QDialog( 0 ) {
	setupUi( this );
	
	connect( goBtn, SIGNAL( clicked() ), this, SLOT( go() ) );
	connect( backBtn, SIGNAL( clicked() ), this, SLOT( back() ) );
	connect( forwardBtn, SIGNAL( clicked() ), this, SLOT( forward() ) );
	connect( stopBtn, SIGNAL( clicked() ), this, SLOT( stop() ) );
	connect( homeBtn, SIGNAL( clicked() ), this, SLOT( home() ) );
	connect( reloadBtn, SIGNAL( clicked() ), this, SLOT( reload() ) );
	connect( gsearchBtn, SIGNAL( clicked() ), this, SLOT( gsearch() ) );
	connect( greaderBtn, SIGNAL( clicked() ), this, SLOT( greader() ) );
	connect( gmailBtn, SIGNAL( clicked() ), this, SLOT( gmail() ) );
	connect( facebookBtn, SIGNAL( clicked() ), this, SLOT( facebook() ) );

	connect( webView, SIGNAL( titleChanged( const QString & ) ), this, SLOT( on_titleChanged( const QString & ) ) );

	this->setWindowTitle( "Web browser" );
	addressLine->setText( "http://" );
}

MainWindow::~MainWindow() { }

void MainWindow::go() {
	url = addressLine->text();
	webView->load( url );
}

void MainWindow::back() {
	webView->back();
}

void MainWindow::forward() {
	webView->forward();
}

void MainWindow::stop() {
	webView->stop();
}

void MainWindow::home() {
	url = "http://www.google.com";
	webView->load( url );
}

void MainWindow::gsearch() {
	url = "http://www.google.com";
	webView->load( url );
}

void MainWindow::greader() {
	url = "http://reader.google.com";
	webView->load( url );
}

void MainWindow::gmail() {
	url = "http://mail.google.com";
	webView->load( url );
}

void MainWindow::facebook() {
	url = "http://www.new.facebook.com";
	webView->load( url );
}

void MainWindow::reload() {
	webView->reload();
}

void MainWindow::on_titleChanged( const QString &title) {
	this->setWindowTitle( "Web browser - " + title );
}