/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Sat Jul 26 16:35:01 2008
**      by: Qt User Interface Compiler version 4.4.0
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtWebKit/QWebView>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWebView *webView;
    QLineEdit *addressLine;
    QPushButton *goBtn;
    QPushButton *backBtn;
    QPushButton *forwardBtn;
    QPushButton *homeBtn;
    QPushButton *stopBtn;
    QPushButton *gsearchBtn;
    QPushButton *greaderBtn;
    QPushButton *gmailBtn;
    QPushButton *facebookBtn;
    QPushButton *reloadBtn;

    void setupUi(QDialog *MainWindow)
    {
    if (MainWindow->objectName().isEmpty())
        MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
    MainWindow->resize(932, 613);
    webView = new QWebView(MainWindow);
    webView->setObjectName(QString::fromUtf8("webView"));
    webView->setGeometry(QRect(0, 0, 931, 541));
    webView->setUrl(QUrl("about:blank"));
    addressLine = new QLineEdit(MainWindow);
    addressLine->setObjectName(QString::fromUtf8("addressLine"));
    addressLine->setGeometry(QRect(170, 550, 671, 31));
    goBtn = new QPushButton(MainWindow);
    goBtn->setObjectName(QString::fromUtf8("goBtn"));
    goBtn->setGeometry(QRect(850, 550, 71, 28));
    backBtn = new QPushButton(MainWindow);
    backBtn->setObjectName(QString::fromUtf8("backBtn"));
    backBtn->setGeometry(QRect(0, 550, 81, 28));
    forwardBtn = new QPushButton(MainWindow);
    forwardBtn->setObjectName(QString::fromUtf8("forwardBtn"));
    forwardBtn->setGeometry(QRect(80, 550, 81, 28));
    homeBtn = new QPushButton(MainWindow);
    homeBtn->setObjectName(QString::fromUtf8("homeBtn"));
    homeBtn->setGeometry(QRect(0, 580, 81, 28));
    stopBtn = new QPushButton(MainWindow);
    stopBtn->setObjectName(QString::fromUtf8("stopBtn"));
    stopBtn->setGeometry(QRect(850, 580, 71, 28));
    gsearchBtn = new QPushButton(MainWindow);
    gsearchBtn->setObjectName(QString::fromUtf8("gsearchBtn"));
    gsearchBtn->setGeometry(QRect(170, 580, 161, 28));
    greaderBtn = new QPushButton(MainWindow);
    greaderBtn->setObjectName(QString::fromUtf8("greaderBtn"));
    greaderBtn->setGeometry(QRect(340, 580, 161, 28));
    gmailBtn = new QPushButton(MainWindow);
    gmailBtn->setObjectName(QString::fromUtf8("gmailBtn"));
    gmailBtn->setGeometry(QRect(510, 580, 161, 28));
    facebookBtn = new QPushButton(MainWindow);
    facebookBtn->setObjectName(QString::fromUtf8("facebookBtn"));
    facebookBtn->setGeometry(QRect(680, 580, 161, 28));
    reloadBtn = new QPushButton(MainWindow);
    reloadBtn->setObjectName(QString::fromUtf8("reloadBtn"));
    reloadBtn->setGeometry(QRect(80, 580, 81, 28));

    retranslateUi(MainWindow);

    QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
    MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Dialog", 0, QApplication::UnicodeUTF8));
    goBtn->setText(QApplication::translate("MainWindow", "Go", 0, QApplication::UnicodeUTF8));
    backBtn->setText(QApplication::translate("MainWindow", "Back", 0, QApplication::UnicodeUTF8));
    forwardBtn->setText(QApplication::translate("MainWindow", "Forward", 0, QApplication::UnicodeUTF8));
    homeBtn->setText(QApplication::translate("MainWindow", "Home", 0, QApplication::UnicodeUTF8));
    stopBtn->setText(QApplication::translate("MainWindow", "Stop", 0, QApplication::UnicodeUTF8));
    gsearchBtn->setText(QApplication::translate("MainWindow", "G-Search", 0, QApplication::UnicodeUTF8));
    greaderBtn->setText(QApplication::translate("MainWindow", "G-Reader", 0, QApplication::UnicodeUTF8));
    gmailBtn->setText(QApplication::translate("MainWindow", "Gmail", 0, QApplication::UnicodeUTF8));
    facebookBtn->setText(QApplication::translate("MainWindow", "Facebook", 0, QApplication::UnicodeUTF8));
    reloadBtn->setText(QApplication::translate("MainWindow", "Reload", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(MainWindow);
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
