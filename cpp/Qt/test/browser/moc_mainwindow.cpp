/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Sat Jul 26 16:35:02 2008
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      17,   11,   11,   11, 0x0a,
      24,   11,   11,   11, 0x0a,
      34,   11,   11,   11, 0x0a,
      41,   11,   11,   11, 0x0a,
      50,   11,   11,   11, 0x0a,
      57,   11,   11,   11, 0x0a,
      67,   11,   11,   11, 0x0a,
      77,   11,   11,   11, 0x0a,
      85,   11,   11,   11, 0x0a,
      96,   11,   11,   11, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0go()\0back()\0forward()\0"
    "stop()\0reload()\0home()\0gsearch()\0"
    "greader()\0gmail()\0facebook()\0"
    "on_titleChanged(QString)\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

const QMetaObject *MainWindow::metaObject() const
{
    return &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
	return static_cast<void*>(const_cast< MainWindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: go(); break;
        case 1: back(); break;
        case 2: forward(); break;
        case 3: stop(); break;
        case 4: reload(); break;
        case 5: home(); break;
        case 6: gsearch(); break;
        case 7: greader(); break;
        case 8: gmail(); break;
        case 9: facebook(); break;
        case 10: on_titleChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        }
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
