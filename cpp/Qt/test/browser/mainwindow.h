/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <iostream>

#include <QDialog>
#include <QUrl>
#include <QWebView>
#include <QPushButton>

#include "ui_mainwindow.h"

class MainWindow : public QDialog, private Ui::MainWindow
{
	Q_OBJECT

	public:
		MainWindow();
		virtual ~MainWindow();

	public slots:
		void go();
		void back();
		void forward();
		void stop();
		void reload();
		void home();
		void gsearch();
		void greader();
		void gmail();
		void facebook();

	protected slots:
		void on_titleChanged( const QString & );
		
	private:
		QUrl url;
		QString title;
};

#endif //MAINWINDOW_H
