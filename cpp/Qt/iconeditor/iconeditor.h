/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef ICONEDITOR_H
#define ICONEDITOR_H

#include <QColor>
#include <QImage>
#include <QWidget>

class IconEditor : public QWidget
{
	Q_OBJECT
	Q_PROPERTY(QColor penColor READ penColor WRITE setPenColor)
	Q_PROPERTY(QImage iconImage READ iconImage WRITE setIconImage)
	Q_PROPERTY(int zoomFactor READ zoomFactor WRITE setZoomFactor)

	public:
		IconEditor(QWidget * = 0);

		void setPenColor(const QColor &);
		QColor penColor() const;
		void setZoomFactor(int);
		int zoomFactor() const;
		void setIconImage(const QImage &);
		QImage iconImage() const;
		virtual QSize sizeHint() const;

	protected:
		virtual void mousePressEvent(QMouseEvent *);
		virtual void mouseMoveEvent(QMouseEvent *);
		virtual void paintEvent(QPaintEvent *);

	private:
		void setImagePixel(const QPoint &, bool);
		QRect pixelRect(int, int) const;

		QColor curColor;
		QImage image;
		int zoom;
};

#endif //ICONEDITOR_H