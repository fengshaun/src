/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <QDataStream>
#include <QByteArray>
#include <QIODevice>
#include <QApplication>
#include <QTimer>
#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <QPushButton>

#include "socket.h"

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	QPushButton button("Quit");
	
	Socket socket;
	socket.connectToHost(QHostAddress::LocalHost, 6150);
	
	QTimer timer;
	timer.start(3 * 1000); //5 seconds
	
	QObject::connect(&timer, SIGNAL(timeout()), &socket, SLOT(send()));
	QObject::connect(&button, SIGNAL(clicked()), &app, SLOT(quit()));
	
	button.resize(250, 200);
	button.show();
	return app.exec();
}
	