/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <iostream>
#include <cstdlib>

#include <QByteArray>
#include <QDataStream>

#include "socket.h"

Socket::Socket(QObject *parent)
	: QTcpSocket(parent)
{
	std::srand(time(0));
	m_data = 10;
	connect(this, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error()));
}

void Socket::send() {
	flush();
	m_data = qint8(std::rand() % 9);
	
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_4_4);
	
	out << quint16(0);
	out << m_data;
	out.device()->seek(0);
	out << quint16(block.size() - sizeof(quint16));
	
	write(block);
	std::cout << "sending " << int(quint16(block.size() - sizeof(quint16))) << " >> " 
			  << int(m_data) << std::endl;
}

void Socket::error() {
	std::cout << "Error: " << qPrintable(this->errorString()) << std::endl;
	this->close();
}