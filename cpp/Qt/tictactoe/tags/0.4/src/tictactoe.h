/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QString>
#include <QColor>
#include <QImage>
#include <QWidget>

class TicTacToe : public QWidget
{
	Q_OBJECT
	
	public:
		TicTacToe(QString = QString("---------"), QWidget * = 0);

		QString state();
		QChar state(int);
		int player();

		void setGridColor(QColor);
		void setPenColor(QColor);
		void setCrossColor(QColor);
		
		QColor gridColor() const;
		QColor penColor() const;
		QColor crossColor() const;

		void setXOnly(bool = true);
		void setOOnly(bool = true);
		
		virtual QSize sizeHint();

	public slots:
		bool setState(QChar, const QPoint &);
		void setState(const QString);
		bool setState(int, QChar);
		bool setState_suppress(int, QChar);
		void setColors(QColor, QColor, QColor);
		void checkState();
		void clear();
		void clear_suppress();

	signals:
		void stateChanged(QString);
		void stateUpdated(int);
		void filled();
		void xWon(int, int, int);
		void oWon(int, int, int);
		void playerChanged(int);

	protected:
		virtual void mousePressEvent(QMouseEvent *);
		virtual void paintEvent(QPaintEvent *);
		virtual void keyPressEvent(QKeyEvent *);

		void drawState();
		void drawX(const QRect &);
		void drawO(const QRect &);

		void printState();

	protected slots:
		void drawCross(int, int, int);
		
	private:
		bool statesAreEqual(int, int, int, char);
		bool finished();
		
		QRect pixelRect(const int, const int);
		QRect pixelRect(const int);
		
		QColor m_penColor;
		QColor m_gridColor;
		QColor m_crossColor;
		QImage m_image;
		QString m_state;
		int m_counter;
		bool m_done;
		bool m_xOnly;
		bool m_oOnly;
		bool m_crossIsDrawn;
		int m_crossPoints[3];
};

#endif //TICTACTOE_H
