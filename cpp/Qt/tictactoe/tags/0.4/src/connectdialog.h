/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>
#include <QString>

class QLabel;
class QDialogButtonBox;
class QLineEdit;
class QGridLayout;
class QVBoxLayout;
class QSpinBox;

class ConnectDialog : public QDialog
{
	Q_OBJECT

	public:
		ConnectDialog(QWidget * = 0);

		QString host();
		quint16 port();

	protected:
		virtual void setupUi();

	private:
		QDialogButtonBox *m_buttonBox;
		
		QLabel *m_ipLabel;
		QLabel *m_portLabel;
		
		QLineEdit *m_ipLineEdit;
		QSpinBox *m_portSpinBox;

		QGridLayout *m_topLayout;
		QVBoxLayout *m_mainLayout;

		QString m_host;
		quint16 m_port;

	private slots:
		void setData();
};

#endif
