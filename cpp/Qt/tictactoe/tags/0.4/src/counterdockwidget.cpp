/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <QtGui>

#include "counterdockwidget.h"
#include "counter.h"

CounterDockWidget::CounterDockWidget(const QString &title, QWidget *parent)
	: QDockWidget(title, parent)
{
	m_counter = new Counter(this);
	setWidget(m_counter);
}

void CounterDockWidget::resizeEvent(QResizeEvent *event) {
	m_oldSize = event->oldSize();
	event->accept();
}

QSize CounterDockWidget::oldSize() {
	return m_oldSize;
}

Counter *CounterDockWidget::counter() {
	return m_counter;
}