/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QColor>
#include <QString>

#include "ui_settingsdialog.h"

class SettingsDialog : public QDialog, private Ui::SettingsDialog
{
	Q_OBJECT

	public:
		SettingsDialog(QWidget * = 0);

		QColor gridColor() const;
		QColor penColor() const;
		QColor crossColor() const;

		bool saveColors() const;
		bool saveServer() const;
		bool saveConnection() const;
		bool restoreSize() const;
		bool restorePosition() const;
		bool restoreCounter() const;

		QString connectionIp() const;
		int connectionPort() const;
		int serverPort() const;
		bool serverPortLoaded() const;

		static QColor defaultColor;

	protected:
		void setConnectionOptions();
		void setServerOptions();
		void setColorOptions();
		
	protected slots:
		void setOptions();
		void saveSettings();
		void readSettings();
		void resetDefaults();

	private:
		QColor m_colors[9];
		
		QColor m_gridColor;
		QColor m_penColor;
		QColor m_crossColor;
		bool m_saveColors;

		QString m_connectIp;
		int m_connectPort;
		bool m_alwaysConnect;

		int m_serverPort;
		bool m_alwaysCreate;
};

#endif //SETTINGSDIALOG_H
