/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <iostream>

#include <QtGui>

#include "settingsdialog.h"

QColor SettingsDialog::defaultColor = QApplication::palette().foreground().color();

SettingsDialog::SettingsDialog(QWidget *parent)
	: QDialog(parent)
{
	setupUi(this);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(setOptions()));
	connect(resetButton, SIGNAL(clicked()), this, SLOT(resetDefaults()));

	m_colors[0] = defaultColor;
	m_colors[1] = Qt::red;
	m_colors[2] = Qt::blue;
	m_colors[3] = Qt::green;
	m_colors[4] = Qt::black;
	m_colors[5] = Qt::white;
	m_colors[6] = Qt::yellow;
	m_colors[7] = Qt::cyan;
	m_colors[8] = Qt::magenta;
	m_colors[9] = Qt::transparent;

	setWindowTitle(tr("Preferences"));
	readSettings();
}

void SettingsDialog::setOptions() {
	setConnectionOptions();
	setServerOptions();
	setColorOptions();
	saveSettings();
}

void SettingsDialog::setConnectionOptions() {
	m_connectIp = ipLineEdit->text();
	m_connectPort = connectionPortSpinBox->value();

	if (connectionCheckBox->isChecked())
		m_alwaysConnect = true;
}

void SettingsDialog::setServerOptions() {
	m_serverPort = serverPortSpinBox->value();

	if (serverCheckBox->isChecked())
		m_alwaysCreate = true;
}

void SettingsDialog::setColorOptions() {
	m_gridColor = m_colors[gridComboBox->currentIndex()];
	m_penColor = m_colors[penComboBox->currentIndex()];
	m_crossColor = m_colors[crossComboBox->currentIndex()];

	if (saveColorCheckBox->isChecked())
		m_saveColors = true;
}

QColor SettingsDialog::gridColor() const {
	return m_gridColor;
}

QColor SettingsDialog::penColor() const {
	return m_penColor;
}

QColor SettingsDialog::crossColor() const {
	return m_crossColor;
}

bool SettingsDialog::saveColors() const {
	return m_saveColors;
}

bool SettingsDialog::saveServer() const {
	return m_alwaysCreate;
}

bool SettingsDialog::saveConnection() const {
	return m_alwaysConnect;
}

QString SettingsDialog::connectionIp() const {
	return m_connectIp;
}

int SettingsDialog::connectionPort() const {
	return m_connectPort;
}

int SettingsDialog::serverPort() const {
	return m_serverPort;
}

bool SettingsDialog::serverPortLoaded() const {
	if (m_alwaysCreate)
		return true;
	else
		return false;
}

void SettingsDialog::saveSettings() {
	QSettings settings("Armin Moradi", "Tic-Tac-Toe");

	settings.setValue("SettingsDialog/General/restoreSize", restoreSizeCheckBox->isChecked());
	settings.setValue("SettingsDialog/General/restorePosition", restorePositionCheckBox->isChecked());
	settings.setValue("SettingsDialog/General/restoreCounter", restoreCounterCheckBox->isChecked());
	
	settings.setValue("SettingsDialog/Colors/gridColorIndex", gridComboBox->currentIndex());
	settings.setValue("SettingsDialog/Colors/penColorIndex", penComboBox->currentIndex());
	settings.setValue("SettingsDialog/Colors/crossColorIndex", crossComboBox->currentIndex());
	settings.setValue("SettingsDialog/Colors/saveColors", m_saveColors);

	settings.setValue("SettingsDialog/Multiplayer/serverPort", m_serverPort);
	settings.setValue("SettingsDialog/Multiplayer/saveServer", m_alwaysCreate);

	settings.setValue("SettingsDialog/Multiplayer/connectionIp", m_connectIp);
	settings.setValue("SettingsDialog/Multiplayer/connectionPort", m_connectPort);
	settings.setValue("SettingsDialog/Multiplayer/saveConnection", m_alwaysConnect);
}

void SettingsDialog::readSettings() {
	QSettings settings("Armin Moradi", "Tic-Tac-Toe");

	restoreSizeCheckBox->setChecked(settings.value("SettingsDialog/General/restoreSize", false).toBool());
	restorePositionCheckBox->setChecked(settings.value("SettingsDialog/General/restorePosition", false).toBool());
	restoreCounterCheckBox->setChecked(settings.value("SettingsDialog/General/restoreCounter", false).toBool());

	gridComboBox->setCurrentIndex(settings.value("SettingsDialog/Colors/gridColorIndex", 4).toInt());
	penComboBox->setCurrentIndex(settings.value("SettingsDialog/Colors/penColorIndex", 4).toInt());
	crossComboBox->setCurrentIndex(settings.value("SettingsDialog/Colors/crossColorIndex", 6).toInt());
	m_saveColors = settings.value("SettingsDialog/Colors/saveColors", false).toBool();
	saveColorCheckBox->setChecked(m_saveColors);

	m_serverPort = quint16(settings.value("SettingsDialog/Multiplayer/serverPort", 0).toInt());
	serverPortSpinBox->setValue(m_serverPort);
	m_alwaysCreate = settings.value("SettingsDialog/Multiplayer/saveServer", false).toBool();
	serverCheckBox->setChecked(m_alwaysCreate);

	m_connectIp = settings.value("SettingsDialog/Multiplayer/connectionIp", QString("")).toString();
	ipLineEdit->setText(m_connectIp);
	m_connectPort = quint16(settings.value("SettingsDialog/Multiplayer/connectionPort", 0).toInt());
	connectionPortSpinBox->setValue(m_connectPort);
	m_alwaysConnect = settings.value("SettingsDialog/Multiplayer/saveConnection", false).toBool();
	connectionCheckBox->setChecked(m_alwaysConnect);
}

void SettingsDialog::resetDefaults() {
	QSettings settings("Armin Moradi", "Tic-Tac-Toe");
	settings.clear();
	readSettings();
}

bool SettingsDialog::restoreSize() const {
	return restoreSizeCheckBox->isChecked();
}

bool SettingsDialog::restorePosition() const {
	return restorePositionCheckBox->isChecked();
}

bool SettingsDialog::restoreCounter() const {
	return restoreCounterCheckBox->isChecked();
}