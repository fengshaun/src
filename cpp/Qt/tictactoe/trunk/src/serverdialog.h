/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef CREATESERVERDIALOG_H
#define CREATESERVERDIALOG_H

#include <QDialog>

class QDialogButtonBox;
class QSpinBox;
class QLabel;
class QLineEdit;
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;

class ServerDialog : public QDialog
{
	Q_OBJECT
	
	public:
		ServerDialog(QWidget * = 0);

		quint16 port();

	protected:
		virtual void setupUi();

	protected slots:
		void setPort(int);

	private:
		QDialogButtonBox *buttonBox;
		QPushButton *createButton;

		QLabel *portLabel;
		QSpinBox *portSpinBox;

		QHBoxLayout *portLayout;
		QVBoxLayout *mainLayout;

		quint16 m_port;
};

#endif
