============================================================
=================== TIC-TAC-TOE HANDBOOK ===================
============================================================

LOCAL MULTIPLAYER:

If you want to play with someone whom you are sitting on the
same computer with, just start the game and you can start
playing straight away.


NETWORK MULTIPLAYER:

To create a network game, follow these instructions:

- Go to Multiplayer menu, and then click on Create a server.

- A dialog will appear asking you for the port you want
  to use while hosting the game.  If you don't know what
  ports are available, set it to zero and click 'Create'.
  When setting the port number to 0(zero), the program will
  automatically choose an appropriate port for you.
  
- Now you should see 'Awaiting connections on port #' on the
  status bar.  The program is waiting for someone to join.
  
- Find out your IP (explained later) and tell the person you
  want to play with, your IP address and the port you are
  using.
  
- Wait for the person to join, if anyone joins your game, the
  status bar will inform you that it's connected to another
  player.
  
- Now you can start playing.

To join a network game, follow these instructions:

- Go to Multiplayer menu, and click on 'Join...'

- you will be greeted with a dialog asking you about the
  server's IP address and port, you have to get these information
  from the person who has created the game.
  
- After filling in the information click 'Ok'

- Now you are connected and you can start playing.


KEYBOARD SHORTCUTS:

As of version 0.3, Tic-Tac-Toe includes keyboard shortcuts.
Keyboard shortcuts in this game are mapped to numpad keys,
and as they are aligned in a grid, it precisely matches the
grid in the Tic-Tac-Toe.  You can use them to play your turn.
NOTE: number 0 clears the board.


FINDING YOUR IP ADDRESS:

Linux:
- Fire up a terminal, and as root type:
# ifconfig

- Now look for 'inet addr' in eth0 (or eth1 etc., whichever
  has an address OTHER THAN 127.0.0.1) section.

- The number (in the form of 00.000.00.00) is your IP address

Windows:
- Go to Start > Run... (Shortcut: Win+R) and type 'cmd' (without quotes)

- In the command prompt, type:
> ipconfig

- And look for 'IPv4 Address'

An alternative and easy way to find out about your IP
address is to go to this website from your webbrowser.

http://www.whatismyip.com
