/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Sun Sep 14 15:48:33 2008
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "src/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      25,   11,   11,   11, 0x0a,
      43,   11,   11,   11, 0x0a,
      58,   11,   11,   11, 0x0a,
      67,   11,   11,   11, 0x0a,
      85,   11,   11,   11, 0x0a,
     102,   11,   11,   11, 0x0a,
     109,   11,   11,   11, 0x0a,
     116,   11,   11,   11, 0x09,
     137,   11,   11,   11, 0x08,
     147,   11,   11,   11, 0x08,
     155,   11,   11,   11, 0x08,
     167,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0setOptions()\0connectToServer()\0"
    "createServer()\0center()\0updateStatusBar()\0"
    "clearStatusBar()\0xWon()\0oWon()\0"
    "updateTicTacToe(int)\0newGame()\0about()\0"
    "connected()\0reportBugs()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

const QMetaObject *MainWindow::metaObject() const
{
    return &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: setOptions(); break;
        case 1: connectToServer(); break;
        case 2: createServer(); break;
        case 3: center(); break;
        case 4: updateStatusBar(); break;
        case 5: clearStatusBar(); break;
        case 6: xWon(); break;
        case 7: oWon(); break;
        case 8: updateTicTacToe((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: newGame(); break;
        case 10: about(); break;
        case 11: connected(); break;
        case 12: reportBugs(); break;
        }
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
