/****************************************************************************
** Meta object code from reading C++ file 'tictactoe.h'
**
** Created: Sun Sep 14 16:15:36 2008
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "src/tictactoe.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tictactoe.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TicTacToe[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      33,   10,   10,   10, 0x05,
      51,   10,   10,   10, 0x05,
      63,   60,   10,   10, 0x05,
      81,   60,   10,   10, 0x05,
      99,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
     118,   10,   10,   10, 0x0a,
     140,   10,   10,   10, 0x0a,
     164,  162,  157,   10, 0x0a,
     187,   10,   10,   10, 0x0a,
     205,  162,  157,   10, 0x0a,
     225,  162,  157,   10, 0x0a,
     254,   60,   10,   10, 0x0a,
     286,   10,   10,   10, 0x0a,
     299,   10,   10,   10, 0x0a,
     307,   10,   10,   10, 0x0a,
     324,   60,   10,   10, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_TicTacToe[] = {
    "TicTacToe\0\0stateChanged(QString)\0"
    "stateUpdated(int)\0filled()\0,,\0"
    "xWon(int,int,int)\0oWon(int,int,int)\0"
    "playerChanged(int)\0setSinglePlayer(bool)\0"
    "putRandom(QChar)\0bool\0,\0setState(QChar,QPoint)\0"
    "setState(QString)\0setState(int,QChar)\0"
    "setState_suppress(int,QChar)\0"
    "setColors(QColor,QColor,QColor)\0"
    "checkState()\0clear()\0clear_suppress()\0"
    "drawCross(int,int,int)\0"
};

const QMetaObject TicTacToe::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TicTacToe,
      qt_meta_data_TicTacToe, 0 }
};

const QMetaObject *TicTacToe::metaObject() const
{
    return &staticMetaObject;
}

void *TicTacToe::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TicTacToe))
        return static_cast<void*>(const_cast< TicTacToe*>(this));
    return QWidget::qt_metacast(_clname);
}

int TicTacToe::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: stateChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: stateUpdated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: filled(); break;
        case 3: xWon((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: oWon((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: playerChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: setSinglePlayer((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: putRandom((*reinterpret_cast< QChar(*)>(_a[1]))); break;
        case 8: { bool _r = setState((*reinterpret_cast< QChar(*)>(_a[1])),(*reinterpret_cast< const QPoint(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: setState((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: { bool _r = setState((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QChar(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { bool _r = setState_suppress((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QChar(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: setColors((*reinterpret_cast< QColor(*)>(_a[1])),(*reinterpret_cast< QColor(*)>(_a[2])),(*reinterpret_cast< QColor(*)>(_a[3]))); break;
        case 13: checkState(); break;
        case 14: clear(); break;
        case 15: clear_suppress(); break;
        case 16: drawCross((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        }
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void TicTacToe::stateChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TicTacToe::stateUpdated(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TicTacToe::filled()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void TicTacToe::xWon(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TicTacToe::oWon(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TicTacToe::playerChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
