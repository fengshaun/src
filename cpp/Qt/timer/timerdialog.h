/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef TIMER_H
#define TIMER_H

#include <QDialog>

class QPushButton;
class QLCDNumber;
class QTimer;
class QVBoxLayout;

class TimerDialog : public QDialog
{
	Q_OBJECT

	public:
		TimerDialog(QWidget * = 0);

	protected slots:
		void start();
		
	private:
		int m_minutes;
		QTimer *m_timer;
		QLCDNumber *m_lcd;

		int m_time;

		QPushButton *m_startButton;
		QVBoxLayout *m_mainLayout;

	private slots:
		void done();
		void updateLcd();
};

#endif