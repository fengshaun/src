/****************************************************************************
** Meta object code from reading C++ file 'timerdialog.h'
**
** Created: Wed Sep 17 00:41:25 2008
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "timerdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'timerdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TimerDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x09,
      21,   12,   12,   12, 0x08,
      28,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TimerDialog[] = {
    "TimerDialog\0\0start()\0done()\0updateLcd()\0"
};

const QMetaObject TimerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TimerDialog,
      qt_meta_data_TimerDialog, 0 }
};

const QMetaObject *TimerDialog::metaObject() const
{
    return &staticMetaObject;
}

void *TimerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TimerDialog))
        return static_cast<void*>(const_cast< TimerDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int TimerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: start(); break;
        case 1: done(); break;
        case 2: updateLcd(); break;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
