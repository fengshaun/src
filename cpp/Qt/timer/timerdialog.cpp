/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <iostream>

#include <QtGui>

#include "timerdialog.h"

TimerDialog::TimerDialog(QWidget *parent)
	: QDialog(parent)
{
	m_timer = new QTimer;
	m_timer->setInterval(1000);

	m_lcd = new QLCDNumber(2);
	m_lcd->setSegmentStyle(QLCDNumber::Filled);

	m_startButton = new QPushButton(tr("Start"));

	connect(m_startButton, SIGNAL(clicked()), this, SLOT(start()));
	connect(m_timer, SIGNAL(timeout()), this, SLOT(updateLcd()));

	m_mainLayout = new QVBoxLayout;
	m_mainLayout->addWidget(m_lcd);
	m_mainLayout->addWidget(m_startButton);

	setLayout(m_mainLayout);
}

void TimerDialog::start() {
	m_time = 61;
	updateLcd();
	m_timer->start();
}

void TimerDialog::updateLcd() {
	if (--m_time <= 0)
		done();
	m_lcd->display(m_time);
}

void TimerDialog::done() {
	QApplication::beep();
	m_timer->stop();
}