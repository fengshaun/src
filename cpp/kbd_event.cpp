#include <iostream>
#include <fstream>
#include <string>

int main() {
    std::ifstream file("/dev/input/event3");

    while (!file.eof()) {
        file.sync();
        std::string data;
        getline(file, data);
        if (data != "") {
            std::cout << data << std::endl;
        }
        data = std::string("");
    }

    return 0;
}
