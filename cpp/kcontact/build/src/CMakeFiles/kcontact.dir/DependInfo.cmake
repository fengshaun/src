# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/armin/kcontact/kcontact/src/contactdatabase.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/contactdatabase.o"
  "/home/armin/kcontact/kcontact/src/contactdialog.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/contactdialog.o"
  "/home/armin/kcontact/kcontact/build/src/kcontact_automoc.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/kcontact_automoc.o"
  "/home/armin/kcontact/kcontact/src/main.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/main.o"
  "/home/armin/kcontact/kcontact/src/mainwidget.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/mainwidget.o"
  "/home/armin/kcontact/kcontact/src/mainwindow.cpp" "/home/armin/kcontact/kcontact/build/src/CMakeFiles/kcontact.dir/mainwindow.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "_BSD_SOURCE"
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  "QT_DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
