/*
    This file is part of KContact.

    KContact is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KContact is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KCantact.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "contactdialog.h"

#include <KDebug>

#include <QItemDelegate>

#include "contactdatabase.h"

ContactDialogWidget::ContactDialogWidget() {
	firstNameEdit = new KLineEdit;
	lastNameEdit = new KLineEdit;
	
	firstNameLabel = new QLabel("First name:");
	lastNameLabel = new QLabel("Last name:");
	
	layout = new QGridLayout;
	layout->addWidget(firstNameLabel, 0, 0);
	layout->addWidget(firstNameEdit, 0, 1);
	layout->addWidget(lastNameLabel, 1, 0);
	layout->addWidget(lastNameEdit, 1, 1);
	
	setLayout(layout);
}

ContactDialog::ContactDialog(QWidget *parent) : KDialog(parent) {
	m_widget = new ContactDialogWidget;
	m_widget->firstNameEdit->setText(m_firstName);
	m_widget->lastNameEdit->setText(m_lastName);
	
	setMainWidget(m_widget);
	setButtons(KDialog::Close);
}

ContactDialog::~ContactDialog() {
	m_mapper->submit();
}

void ContactDialog::setFirstName(QString firstName) {
	m_firstName = firstName;
}

void ContactDialog::setLastName(QString lastName) {
	m_lastName = lastName;
}

int ContactDialog::exec() {
	kDebug() << "";
	
	populateWidgets();
	return KDialog::exec();
}

void ContactDialog::populateWidgets() {
	kDebug() << "";

	m_model = new QSqlTableModel(this);
	m_model->setTable("contacts");
	m_model->setFilter("firstname = '" + m_firstName + "'");
	m_model->select();
	
	m_mapper = new QDataWidgetMapper(this);
	m_mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
	m_mapper->setModel(m_model);
	m_mapper->addMapping(m_widget->firstNameEdit, ContactDatabase::FirstName);
	m_mapper->addMapping(m_widget->lastNameEdit, ContactDatabase::LastName);
	m_mapper->toFirst();
}

