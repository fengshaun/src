/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef COUNTER_H
#define COUNTER_H

#include <QWidget>

class QLCDNumber;
class QLabel;
class QHBoxLayout;
class QVBoxLayout;
class QBoxLayout;

class Counter : public QWidget
{
	Q_OBJECT

	public:
		explicit Counter(QWidget * = 0);

		int playerOneWins();
		int playerTwoWins();

		void reset();
		void setNetworkPlay(bool, int);

	public slots:
		void playerOneWon();
		void playerTwoWon();

		void changeLayout(Qt::DockWidgetArea);

	signals:
		void playerOneWinsChanged(int);
		void playerTwoWinsChanged(int);

	protected:
		void setupUi();

	private:
		QLCDNumber *m_oneLcd;
		QLCDNumber *m_twoLcd;

		QLabel *m_oneLabel;
		QLabel *m_twoLabel;

		int m_oneValue;
		int m_twoValue;

		QVBoxLayout *m_leftLayout;
		QVBoxLayout *m_rightLayout;
		QBoxLayout *m_mainLayout;
};

#endif //COUNTER_H
