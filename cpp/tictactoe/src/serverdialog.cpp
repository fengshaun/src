/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <QtGui>

#include "serverdialog.h"

ServerDialog::ServerDialog(QWidget *parent)
	: QDialog(parent)
{
	setupUi();
	setPort(portSpinBox->value());

	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(portSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setPort(int)));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void ServerDialog::setupUi() {
	buttonBox = new QDialogButtonBox;
	createButton = new QPushButton(tr("C&reate"));

	buttonBox->addButton(createButton, QDialogButtonBox::AcceptRole);
	buttonBox->addButton(QDialogButtonBox::Cancel);

	portLabel = new QLabel(tr("Port:"));
	portSpinBox = new QSpinBox;
	portSpinBox->setRange(0, 65535);
	portSpinBox->setValue(0);

	portLayout = new QHBoxLayout;
	portLayout->addWidget(portLabel);
	portLayout->addWidget(portSpinBox);

	mainLayout = new QVBoxLayout;
	mainLayout->addLayout(portLayout);
	mainLayout->addWidget(buttonBox);

	setLayout(mainLayout);
}

void ServerDialog::setPort(int port) {
	m_port = port;
}

quint16 ServerDialog::port() {
	return m_port;
}
