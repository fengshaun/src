/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#include <QtGui>

#include "connectdialog.h"

ConnectDialog::ConnectDialog(QWidget *parent)
	: QDialog(parent)
{
	setupUi();
	connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(setData()));
	connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void ConnectDialog::setupUi() {
	m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok |
									 QDialogButtonBox::Cancel);

	m_ipLabel = new QLabel("IP:");
	m_portLabel = new QLabel("Port:");

	m_ipLineEdit = new QLineEdit("127.0.0.1");
	m_portSpinBox = new QSpinBox;
	m_portSpinBox->setRange(1, 65535);
	m_portSpinBox->setValue(1);

	m_topLayout = new QGridLayout;
	m_topLayout->addWidget(m_ipLabel, 0, 0);
	m_topLayout->addWidget(m_ipLineEdit, 0, 1);
	m_topLayout->addWidget(m_portLabel, 1, 0);
	m_topLayout->addWidget(m_portSpinBox, 1, 1);

	m_mainLayout = new QVBoxLayout;
	m_mainLayout->addLayout(m_topLayout);
	m_mainLayout->addWidget(m_buttonBox);

	setLayout(m_mainLayout);
}

void ConnectDialog::setData() {
	m_host = m_ipLineEdit->text();
	m_port = quint16(m_portSpinBox->value());
}

QString ConnectDialog::host() {
	return m_host;
}

quint16 ConnectDialog::port() {
	return m_port;
}
