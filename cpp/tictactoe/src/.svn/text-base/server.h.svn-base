/***************************************************************************
 * General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>  *
 * http://fengshaun.wordpress.com                                          *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program; if not, write to the Free Software             *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US  *
 ***************************************************************************/

#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QString>

class QTcpSocket;

class Server : public QTcpServer
{
	Q_OBJECT

	public:
		Server(QObject * = 0);
		Server(quint16, QObject * = 0);

	public slots:
		void send(int);

	signals:
		void readDone(int);

	protected:
		virtual void incomingConnection(int);
		
	protected slots:
		void read();
		virtual void initialize();
		virtual void error(QAbstractSocket::SocketError);

	private:
		QTcpSocket *m_socket;
		quint8 m_data;
		quint16 m_blockSize;
};

#endif
