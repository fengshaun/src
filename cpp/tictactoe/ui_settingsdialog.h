/********************************************************************************
** Form generated from reading ui file 'settingsdialog.ui'
**
** Created: Mon Dec 8 18:18:04 2008
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QListWidget *leftPane;
    QStackedWidget *stackedWidget;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *startupGroupBox;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *restoreSizeCheckBox;
    QCheckBox *restorePositionCheckBox;
    QCheckBox *restoreCounterCheckBox;
    QSpacerItem *verticalSpacer_2;
    QWidget *page;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *connectionGroupBox;
    QGridLayout *gridLayout_2;
    QLabel *ipLabel;
    QLineEdit *ipLineEdit;
    QLabel *portLabel;
    QSpinBox *connectionPortSpinBox;
    QCheckBox *connectionCheckBox;
    QGroupBox *serverGroupBox;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *serverPortLabel;
    QSpinBox *serverPortSpinBox;
    QCheckBox *serverCheckBox;
    QWidget *colorsPage;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *penLabel;
    QComboBox *penComboBox;
    QLabel *crossColor;
    QComboBox *crossComboBox;
    QLabel *gridLabel;
    QComboBox *gridComboBox;
    QCheckBox *saveColorCheckBox;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *resetButton;
    QSpacerItem *horizontalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SettingsDialog)
    {
    if (SettingsDialog->objectName().isEmpty())
        SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
    SettingsDialog->resize(393, 289);
    verticalLayout_4 = new QVBoxLayout(SettingsDialog);
    verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
    verticalLayout_2 = new QVBoxLayout();
    verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    leftPane = new QListWidget(SettingsDialog);
    new QListWidgetItem(leftPane);
    new QListWidgetItem(leftPane);
    new QListWidgetItem(leftPane);
    leftPane->setObjectName(QString::fromUtf8("leftPane"));
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(leftPane->sizePolicy().hasHeightForWidth());
    leftPane->setSizePolicy(sizePolicy);
    leftPane->setMaximumSize(QSize(91, 16777215));

    horizontalLayout->addWidget(leftPane);

    stackedWidget = new QStackedWidget(SettingsDialog);
    stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
    page_2 = new QWidget();
    page_2->setObjectName(QString::fromUtf8("page_2"));
    verticalLayout_7 = new QVBoxLayout(page_2);
    verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
    startupGroupBox = new QGroupBox(page_2);
    startupGroupBox->setObjectName(QString::fromUtf8("startupGroupBox"));
    verticalLayout_6 = new QVBoxLayout(startupGroupBox);
    verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
    verticalLayout_5 = new QVBoxLayout();
    verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
    restoreSizeCheckBox = new QCheckBox(startupGroupBox);
    restoreSizeCheckBox->setObjectName(QString::fromUtf8("restoreSizeCheckBox"));

    verticalLayout_5->addWidget(restoreSizeCheckBox);

    restorePositionCheckBox = new QCheckBox(startupGroupBox);
    restorePositionCheckBox->setObjectName(QString::fromUtf8("restorePositionCheckBox"));

    verticalLayout_5->addWidget(restorePositionCheckBox);

    restoreCounterCheckBox = new QCheckBox(startupGroupBox);
    restoreCounterCheckBox->setObjectName(QString::fromUtf8("restoreCounterCheckBox"));

    verticalLayout_5->addWidget(restoreCounterCheckBox);


    verticalLayout_6->addLayout(verticalLayout_5);


    verticalLayout_7->addWidget(startupGroupBox);

    verticalSpacer_2 = new QSpacerItem(20, 106, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout_7->addItem(verticalSpacer_2);

    stackedWidget->addWidget(page_2);
    page = new QWidget();
    page->setObjectName(QString::fromUtf8("page"));
    verticalLayout_3 = new QVBoxLayout(page);
    verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
    connectionGroupBox = new QGroupBox(page);
    connectionGroupBox->setObjectName(QString::fromUtf8("connectionGroupBox"));
    gridLayout_2 = new QGridLayout(connectionGroupBox);
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    ipLabel = new QLabel(connectionGroupBox);
    ipLabel->setObjectName(QString::fromUtf8("ipLabel"));

    gridLayout_2->addWidget(ipLabel, 0, 0, 1, 1);

    ipLineEdit = new QLineEdit(connectionGroupBox);
    ipLineEdit->setObjectName(QString::fromUtf8("ipLineEdit"));

    gridLayout_2->addWidget(ipLineEdit, 0, 1, 1, 1);

    portLabel = new QLabel(connectionGroupBox);
    portLabel->setObjectName(QString::fromUtf8("portLabel"));

    gridLayout_2->addWidget(portLabel, 1, 0, 1, 1);

    connectionPortSpinBox = new QSpinBox(connectionGroupBox);
    connectionPortSpinBox->setObjectName(QString::fromUtf8("connectionPortSpinBox"));
    connectionPortSpinBox->setMinimum(1);
    connectionPortSpinBox->setMaximum(65535);

    gridLayout_2->addWidget(connectionPortSpinBox, 1, 1, 1, 1);

    connectionCheckBox = new QCheckBox(connectionGroupBox);
    connectionCheckBox->setObjectName(QString::fromUtf8("connectionCheckBox"));

    gridLayout_2->addWidget(connectionCheckBox, 2, 0, 1, 2);


    verticalLayout_3->addWidget(connectionGroupBox);

    serverGroupBox = new QGroupBox(page);
    serverGroupBox->setObjectName(QString::fromUtf8("serverGroupBox"));
    gridLayout_3 = new QGridLayout(serverGroupBox);
    gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    serverPortLabel = new QLabel(serverGroupBox);
    serverPortLabel->setObjectName(QString::fromUtf8("serverPortLabel"));

    horizontalLayout_2->addWidget(serverPortLabel);

    serverPortSpinBox = new QSpinBox(serverGroupBox);
    serverPortSpinBox->setObjectName(QString::fromUtf8("serverPortSpinBox"));
    serverPortSpinBox->setMaximum(65535);

    horizontalLayout_2->addWidget(serverPortSpinBox);


    gridLayout_3->addLayout(horizontalLayout_2, 0, 0, 1, 1);

    serverCheckBox = new QCheckBox(serverGroupBox);
    serverCheckBox->setObjectName(QString::fromUtf8("serverCheckBox"));

    gridLayout_3->addWidget(serverCheckBox, 1, 0, 1, 1);


    verticalLayout_3->addWidget(serverGroupBox);

    stackedWidget->addWidget(page);
    colorsPage = new QWidget();
    colorsPage->setObjectName(QString::fromUtf8("colorsPage"));
    verticalLayout = new QVBoxLayout(colorsPage);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    groupBox = new QGroupBox(colorsPage);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    gridLayout = new QGridLayout(groupBox);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    penLabel = new QLabel(groupBox);
    penLabel->setObjectName(QString::fromUtf8("penLabel"));
    sizePolicy.setHeightForWidth(penLabel->sizePolicy().hasHeightForWidth());
    penLabel->setSizePolicy(sizePolicy);

    gridLayout->addWidget(penLabel, 0, 0, 1, 1);

    penComboBox = new QComboBox(groupBox);
    penComboBox->setObjectName(QString::fromUtf8("penComboBox"));

    gridLayout->addWidget(penComboBox, 0, 1, 1, 1);

    crossColor = new QLabel(groupBox);
    crossColor->setObjectName(QString::fromUtf8("crossColor"));

    gridLayout->addWidget(crossColor, 1, 0, 1, 1);

    crossComboBox = new QComboBox(groupBox);
    crossComboBox->setObjectName(QString::fromUtf8("crossComboBox"));

    gridLayout->addWidget(crossComboBox, 1, 1, 1, 1);

    gridLabel = new QLabel(groupBox);
    gridLabel->setObjectName(QString::fromUtf8("gridLabel"));

    gridLayout->addWidget(gridLabel, 2, 0, 1, 1);

    gridComboBox = new QComboBox(groupBox);
    gridComboBox->setObjectName(QString::fromUtf8("gridComboBox"));

    gridLayout->addWidget(gridComboBox, 2, 1, 1, 1);

    saveColorCheckBox = new QCheckBox(groupBox);
    saveColorCheckBox->setObjectName(QString::fromUtf8("saveColorCheckBox"));

    gridLayout->addWidget(saveColorCheckBox, 3, 0, 1, 1);


    verticalLayout->addWidget(groupBox);

    verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer);

    stackedWidget->addWidget(colorsPage);

    horizontalLayout->addWidget(stackedWidget);


    verticalLayout_2->addLayout(horizontalLayout);

    horizontalLayout_3 = new QHBoxLayout();
    horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
    resetButton = new QPushButton(SettingsDialog);
    resetButton->setObjectName(QString::fromUtf8("resetButton"));
    resetButton->setFocusPolicy(Qt::ClickFocus);

    horizontalLayout_3->addWidget(resetButton);

    horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer);

    buttonBox = new QDialogButtonBox(SettingsDialog);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    horizontalLayout_3->addWidget(buttonBox);


    verticalLayout_2->addLayout(horizontalLayout_3);


    verticalLayout_4->addLayout(verticalLayout_2);

    QWidget::setTabOrder(ipLineEdit, connectionPortSpinBox);
    QWidget::setTabOrder(connectionPortSpinBox, connectionCheckBox);
    QWidget::setTabOrder(connectionCheckBox, serverPortSpinBox);
    QWidget::setTabOrder(serverPortSpinBox, serverCheckBox);
    QWidget::setTabOrder(serverCheckBox, resetButton);
    QWidget::setTabOrder(resetButton, leftPane);
    QWidget::setTabOrder(leftPane, penComboBox);
    QWidget::setTabOrder(penComboBox, crossComboBox);
    QWidget::setTabOrder(crossComboBox, gridComboBox);
    QWidget::setTabOrder(gridComboBox, saveColorCheckBox);
    QWidget::setTabOrder(saveColorCheckBox, buttonBox);

    retranslateUi(SettingsDialog);
    QObject::connect(leftPane, SIGNAL(currentRowChanged(int)), stackedWidget, SLOT(setCurrentIndex(int)));
    QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialog, SLOT(reject()));

    stackedWidget->setCurrentIndex(0);


    QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
    SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Dialog", 0, QApplication::UnicodeUTF8));

    const bool __sortingEnabled = leftPane->isSortingEnabled();
    leftPane->setSortingEnabled(false);
    leftPane->item(0)->setText(QApplication::translate("SettingsDialog", "General", 0, QApplication::UnicodeUTF8));
    leftPane->item(1)->setText(QApplication::translate("SettingsDialog", "Multiplayer", 0, QApplication::UnicodeUTF8));
    leftPane->item(2)->setText(QApplication::translate("SettingsDialog", "Colors", 0, QApplication::UnicodeUTF8));

    leftPane->setSortingEnabled(__sortingEnabled);
    startupGroupBox->setTitle(QApplication::translate("SettingsDialog", "Startup", 0, QApplication::UnicodeUTF8));
    restoreSizeCheckBox->setText(QApplication::translate("SettingsDialog", "Restore size on startup", 0, QApplication::UnicodeUTF8));
    restorePositionCheckBox->setText(QApplication::translate("SettingsDialog", "Restore position on startup", 0, QApplication::UnicodeUTF8));
    restoreCounterCheckBox->setText(QApplication::translate("SettingsDialog", "Restore counter on startup", 0, QApplication::UnicodeUTF8));
    connectionGroupBox->setTitle(QApplication::translate("SettingsDialog", "Connection", 0, QApplication::UnicodeUTF8));
    ipLabel->setText(QApplication::translate("SettingsDialog", "IP:", 0, QApplication::UnicodeUTF8));
    portLabel->setText(QApplication::translate("SettingsDialog", "Port:", 0, QApplication::UnicodeUTF8));
    connectionCheckBox->setText(QApplication::translate("SettingsDialog", "Always connect to the same computer", 0, QApplication::UnicodeUTF8));
    serverGroupBox->setTitle(QApplication::translate("SettingsDialog", "Server", 0, QApplication::UnicodeUTF8));
    serverPortLabel->setText(QApplication::translate("SettingsDialog", "Port:", 0, QApplication::UnicodeUTF8));
    serverCheckBox->setText(QApplication::translate("SettingsDialog", "Always create server on this port", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("SettingsDialog", "Colors", 0, QApplication::UnicodeUTF8));
    penLabel->setText(QApplication::translate("SettingsDialog", "X/O color:", 0, QApplication::UnicodeUTF8));
    penComboBox->clear();
    penComboBox->insertItems(0, QStringList()
     << QApplication::translate("SettingsDialog", "Default", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Red", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Blue", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Green", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Black", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "White", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Yellow", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Cyan", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Magenta", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Transparent", 0, QApplication::UnicodeUTF8)
    );
    crossColor->setText(QApplication::translate("SettingsDialog", "Crossing color:", 0, QApplication::UnicodeUTF8));
    crossComboBox->clear();
    crossComboBox->insertItems(0, QStringList()
     << QApplication::translate("SettingsDialog", "Default", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Red", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Blue", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Green", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Black", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "White", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Yellow", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Cyan", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Magenta", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Transparent", 0, QApplication::UnicodeUTF8)
    );
    gridLabel->setText(QApplication::translate("SettingsDialog", "Grid color:", 0, QApplication::UnicodeUTF8));
    gridComboBox->clear();
    gridComboBox->insertItems(0, QStringList()
     << QApplication::translate("SettingsDialog", "Default", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Red", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Blue", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Green", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Black", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "White", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Yellow", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Cyan", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Magenta", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("SettingsDialog", "Transparent", 0, QApplication::UnicodeUTF8)
    );
    saveColorCheckBox->setText(QApplication::translate("SettingsDialog", "Save colors on exit", 0, QApplication::UnicodeUTF8));
    resetButton->setText(QApplication::translate("SettingsDialog", "Reset to defaults", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(SettingsDialog);
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
