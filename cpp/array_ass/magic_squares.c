/* Magic Squares assignment
 * Matthew Whittake ITCS 1212
 */

#include<stdio.h>

int is_seen(int n, int *array, int size) {
    for (int i = 0; i < size; i++) {
        if (n == *(array + i)) {
            return 0;
        }
    }
    return 1;
}

void print_array(int **array, int rows, int columns) {
    for (int i = 0; i < rows; i++) {
        printf("[ ");
        for (int j = 0; j < columns; j++) {
            printf("%d ", (int) *(array + i*columns + j));
        }
        printf("]\n");
    }
}

int is_magic(int **square, int d) {
    int table[d + d + 2]; // a table to hold the sum of all rows + columns + diagonals
    int table_pos = 0;

    // sum ALL the rows!
    for (int row = 0; row < d; row++) {
        int sum = 0;
        for (int col = 0; col < d; col++) {
            sum += (int) *(square + row * d + col);
        }
        table[table_pos] = sum;
        table_pos++;
    }

    // sum ALL the columns
    for (int row = 0; row < d; row++) {
        int sum = 0;
        for (int col = 0; col < d; col++) {
            sum += (int) *(square + col * d + row);
        }
        table[table_pos] = sum;
        table_pos++;
    }

    // L-to-R diagonal
    {
        int sum = 0;
        for (int i = 0; i < d; i++) {
            sum += (int) *(square + i*d + i);
        }
        table[table_pos] = sum;
        table_pos++;
    }

    // R-to-L diagonal
    {
        int sum = 0;
        for (int i = 0; i < d; i++) {
            sum += (int) *(square + i*d + (d - 1 - i));
        }
        table[table_pos] = sum;
        table_pos++;
    }


    printf("debug ---------------\n");
    printf("sums: ");
    for (int i = 0; i < table_pos; i++) {
        printf("%d ", table[i]);
    }
    printf("\n---------------------\n");

    // check if this f'n square is really magical!
    for (int i = 1; i < table_pos; i++) {
        if (table[i] != table[0]) {
            return 0;
        }
    }
    return 1;

    // epic long function is epic!
}

int main(int argc, char **argv) {
    printf("Enter dimension: ");
    int d; scanf("%d", &d);

    printf("the array will be %dx%d\n", d, d);

    int squares[d][d];
    int seen[d * d];
    int seen_pos = 0; // keep track of the number of numbers (numberception)!

    for (int i = 0; i < d; i++) {
        for (int j = 0; j < d; j++) {
            printf("Enter num: ");
            int n; scanf("%d", &n);

            while (!is_seen(n, seen, seen_pos)) { // check only up to where seen[] is filled
                printf("already seen.  Enter num: ");
                scanf("%d", &n);
            }

            squares[i][j] = n;
            seen[seen_pos] = n;
            seen_pos++;
        }
        seen_pos++;
    }

    printf("here's the array:\n");
    print_array((int**)squares, d, d);
    if(is_magic((int**)squares, d)) {
        printf("'Tis magic brah!\n");
    } else {
        printf("'Tis not magic brah!\n");
    }

    // BOOM!  Done! :)
}
