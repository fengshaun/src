#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>

int main() {
    std::ifstream f("/dev/input/event3", std::ios::binary);

    std::vector<char> v;
    while (!f.eof()) {
        char a;
        f >> a;
        v.push_back(a);

        if (v.size() == 144) {
            std::cout << std::endl;
            int counter = 0;
            for (int i = 0; i < v.size(); i++) {
                if (++counter % 24 == 0) {
                    std::cout << std::endl;
                }
                std::cout << std::hex << std::showbase
                          << std::setw(4) << std::setfill(' ')
                          << std::left << int(v[i]) << ' ' << std::flush;
            }
            v.clear();
            std::cout << std::endl;
        }
    }
}
