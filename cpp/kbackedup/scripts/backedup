#!/bin/bash
#
##########################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com> #
# http://fengshaun.wordpress.com                                         #
#                                                                        #
# This program is free software; you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation; either version 3 of the License, or      #
# any later version.                                                     #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program; if not, write to the Free Software            #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US #
##########################################################################

#go to home directory  -->  I just love to start from home
cd

########################### CONFIGURATION ########################## NOTE:change these to your likings

#directories to be backed up.  Separated by spaces NOTE: should be absolute path
#you should add and remove directories to your likings inside the brackets
DIRECTORIES=() 

#the backup folder which all the backup files would go in there
DIRECTORY=$TMP/bkup

#if it's a local backup, where do you want to put the resulting archive
LOCALDEST=$HOME/Desktop

#if it's a local backup, should it be decompressed after archiving (silly option)
DECOMPRESS=0

#whether to reveal hidden files (by removing the '.' from the filenames)
NOHIDDEN=0

#whether to delete '.swp' files (e.g. Vim swap files)
REMOVESWAP=0

#excluding a folder or a file from being backed up
EXCLUDE=0
EXCLUDEDIR=

#temporary folder
TMP=/tmp

#resulting archive name
ARCHIVENAME=backup.tar.gz

#whether a local backup or a remote backup
LOCATION=machine

#if it's a remote backup, the ip address of the remote machine (should support ssh)
IP=127.0.0.1

#remote username
UN=

#remote folder which the resulting archive would be sent
REMOTEDEST=/cygdrive/c/system/backup

#if you want to completely renew your backup directory
RENEW=0

####### Be careful with these options
TAROPTIONS=-czf
UNTAROPTIONS=-xzf
NOHIDDENONLY=0
SYNCONLY=0
REMOVESWAPONLY=0



#####################################################################


#check if there is any arguments
if [ "$#" = "0" ] || [ "$1" = "--help" ]; then
	echo "Usage: $0 [ OPTION ]"
	echo ""
	echo "OPTIONS: "
	echo -e "  --default\t\t\tusing default settings"
	echo -e "\t\t\t\t(using your settings in the script)"
	echo -e "  --decompress\t\t\tdecompress the backup archive"
	echo -e "\t\t\t\ton the local folder (only for local backups)"
	echo -e "  --local [DIRECTORY]\t\tcreate a local backup"
	echo -e "  --machine\t\t\tcreate a backup on a remote machine"
	echo -e "  --dir [DIRECTORY]\t\tlocal directory to be backed up"
	echo -e "  --remote-dir [DIRECTORY]\tremote directory that "
	echo -e "\t\t\t\tthe backup archive will be put in"
	echo -e "  --ip [ IP ADDRESS ]\t\tthe ip address of remote machine"
	echo -e "  --user [ USERNAME ]\t\tremote username used to log into"
	echo -e "\t\t\t\tthe remote computer"
	echo -e "  --sync-only\t\t\texits when finished syncing source with"
	echo -e "\t\t\t\tthe backup directory"
	echo -e "  --no-hidden\t\t\treveals hidden files in the resulting archive"
	echo -e "\t\t\t\tby removing the '.' in front of the filenames"
	echo -e "  --no-hidden-only\t\tsame as --no-hidden by exits when done"
	echo -e "  --rswapfiles\t\t\tremoves '.swp' files from the resulting archive"
	echo -e "  --rswapfiles-only\t\tsame as --rswapfiles but exits when done"
	echo -e "  --exclude\t\t\texcludes specified folder from the resulting archive"
	echo -e "  --noexclude\t\t\tdoes not exclude any folder from the resulting archive."
	echo -e "\t\t\t\tUse when you have set EXCLUDE to 1 in the script"
	echo ""
	echo "Options for this machine are:"
	echo "  --$LOCATION"
	echo "  --ip $IP"
	echo "  --user $UN"
	echo "  --site $SITE"
	echo "  --remote-dir $REMOTEDEST"
	echo "  --dir $DIRECTORIES[@]}"
	echo ""
	echo "  archive name: $ARCHIVENAME"
	echo "  directories to be backed up:"
	for i in ${DIRECTORIES[@]}; do
		echo "    $i"
	done
	echo ""
	echo "If you want to use the default settings,"
	echo "use '--default' or '-' as an option"
	echo "example  $ backup -    $ backup --default"
	echo ""
	echo "You can also change all of the options above"
	echo "directly in the script to make it even easier to backup"
	exit 1;
fi


##################### START CONFIGURATION #####################

while [ "$#" -gt "0" ]; do
	case $1 in
		--ip)
			shift
			IP=$1
		;;
		--user)
			shift
			UN=$1
		;;
		--dir)
			shift
			DIRECTORIES[${#DIRECTORIES[@]}]=$1
		;;
		--decompress)
			DECOMPRESS=1
		;;
		--remote-dir|--remotedir)
			shift
			REMOTEDEST=$1
		;;
		--local)
			shift
			LOCATION=local
			LOCALDEST=$1
		;;
		--server)
			LOCATION=server
			shift
			if [ "$1" = "" ]; then
				SITE=ftp.ypu.com
			else
				SITE=$1
			fi
		;;
		--machine)
			LOCATION=machine
		;;
		--sync-only|--synconly)
			SYNC=1
			SYNCONLY=1
		;;
		--no-hidden|--nohidden)
			NOHIDDEN=1
		;;
		--no-hidden-only|--nohiddenonly)
			NOHIDDEN=1
			NOHIDDENONLY=1
		;;
		--rswapfile|--rswap)
			REMOVESWAP=1
		;;
		--norswapfile|--norswap)
			REMOVESWAP=0
		;;
		--rswapfile-only|--rswaponly)
			REMOVESWAP=1
			REMOVESWAPONLY=1
		;;
		--exclude)
			EXCLUDE=1
			shift
			EXCLUDEDIR=$1
			EXCLUDEDIR=`echo $EXCLUDEDIR | sed s:$HOME:$DIRECTORY:`
		;;
		--noexclude)
			EXCLUDE=0
		;;
		--renew)
			RENEW=1
		;;
		--default|-)
			#do nothing, for with the default options
		;;
		*)
			echo "bad option $1"
			exit 0
		;;

	esac

	shift

	if [ "$#" = "0" ]; then
		break
	fi
done
###############END CONFIGURATION ##################


#### RENEW ####
if [ "$RENEW" = "1" ]; then
	echo "Renewing backup directory"
	rm -rf $DIRECTORY
fi

if [ "$EXCLUDE" = "1" ]; then
	echo "Excluding $EXCLUDEDIR"
	rm -rf $EXCLUDEDIR
	echo "EXCLUDE DONE!"
fi

if [ "$NOHIDDEN" = "1" ]; then
	echo "Revealing hidden files and folders"
	cd $DIRECTORY
	for i in `find -name ".*"`; do
		if [ "$i" != "." ]; then
			j=`echo $i | sed 's/\.\/\.//'`

			#if the directory (file replaces) exists, remove it!! it will interfere!
			if [ -d "$j" ]; then
				rm -rfv "$j"
			fi

			#and then move it safely!
			mv -v "$i" "$j"
		fi
	done
	
	echo "REVEAL DONE!"

	if [ "$NOHIDDENONLY" = "1" ]; then
		exit
	fi
fi

if [ "$REMOVESWAP" = "1" ]; then
	echo "Removing Vim swap files"
	cd $DIRECTORY
	for i in `find -name "*.swp"`; do
		rm -v $i
	done
	
	echo "REMOVE DONE!"

	if [ "$REMOVESWAPONLY" = "1" ]; then
		exit
	fi
fi


#### SETTING UP THE ENVIRONMENT ####
echo "Backing up ${DIRECTORIES[@]}"
mkdir $DIRECTORY

# Putting it all in one directory
for i in ${DIRECTORIES[@]}; do
	cp -ru $i $DIRECTORY
done

if [ "$SYNCONLY" = "1" ]; then
		exit 0
fi

cd $DIRECTORY
tar $TAROPTIONS $ARCHIVENAME *

#### PUTTING IT INTO $TMP ####
mv $ARCHIVENAME $TMP

#### THEN MOVE IT TO THE APPROPRIATE LOCATION ####
if [ "$LOCATION" = "local" ]; then
	#if it's a local backup, then it would go to $LOCALDEST
	cp $TMP/$ARCHIVENAME $LOCALDEST
elif [ "$LOCATION" = "machine" ]; then
	#if it's a remote backup on another machine, send the archive!
	echo "sending $ARCHIVENAME to $IP"
	if [ "$UN" = "" ]; then
		sftp $IP <<!
		cd '$REMOTEDEST'
		put $TMP/$ARCHIVENAME
		bye
!
	else
		sftp $UN@$IP <<!
		cd '$REMOTEDEST'
		put $TMP/$ARCHIVENAME
		bye
!
	fi
elif [ "$LOCATION" = server ]; then
	#if it's an ftp server, then we upload it!
	echo "sending $ARCHIVENAME to $SITE"
	ftp $SITE <<!
	put $TMP/$ARCHIVENAME
	bye
!
fi

### DECOMPRESS ###
if [ "$DECOMPRESS" = "1" ]; then
	if [ "$LOCATION" = "local" ]; then
		cd $LOCALDEST
		tar $UNTAROPTIONS $ARCHIVENAME
		rm -f $ARCHIVENAME
	fi

	if [ "$LOCATION" = machine ]; then

		#it should be decompressed now, IF there is an option of it being decompress
		echo "decompressing $ARCHIVENAME on $IP..."
		ssh $IP <<!
		cd '$REMOTEDEST'
		tar $UNTAROPTIONS $ARCHIVENAME
		rm -f $ARCHIVENAME
		exit
!
	fi
fi

#### REMOVING TRACES ####
echo "removing traces..."
cd $TMP
rm -vf $ARCHIVENAME

#### ECHOING WHAT JUST HAPPENED ####
echo "DONE!"
if [ "$LOCATION" = local ]; then
	echo "backup files were written to $LOCALDEST"
elif [ "$LOCATION" = machine ]; then
	echo "backup files were written to $IP $REMOTEDEST"
elif [ "$LOCATION" = server ]; then
	echo "backup files were written to $SITE"
fi
