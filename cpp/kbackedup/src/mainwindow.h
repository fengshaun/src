#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Solid/Device>

#include <QProcess>

#include "kbackupwidget.h"
#include "storagefinder.h"
#include "backupserver.h"

class MainWindow : public KXmlGuiWindow
{
	Q_OBJECT
	
	public:
		MainWindow();
		virtual ~MainWindow();
		
	protected:
		virtual void populateTargets();
		
	protected slots:
		virtual void startBackup();
		void addFolder();
		void removeFolder();
		
		void backupStarted();
		void backupFinished();
		
		QList<QString> sources();
		
	private:
		KBackupWidget *widget;
		BackupServer *server;
		
		QList<QString> udiList;
		QList<QString> labelList;
		QList<Solid::Device> deviceList;
};

#endif // MAINWINDOW_H