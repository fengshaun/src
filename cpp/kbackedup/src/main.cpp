#include <KAboutData>
#include <KApplication>
#include <KCmdLineArgs>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
	KAboutData aboutData("kbackedup", 0,
					ki18n("KBackedUp"),
					"1.0",
					ki18n("A Front-end for the backed-up script"),
					KAboutData::License_GPL_V2,
					ki18n("Copyright (c) 2008"),
					ki18n("A program for backing up important\n"
						  "data and settings."),
					"http://fengshaun.wordpress.com",
					"amoradi@fedoraproject.org");
					
	KCmdLineArgs::init(argc, argv, &aboutData);
	
	KApplication app;
	
	MainWindow *mw = new MainWindow;
	mw->show();
	
	return app.exec();
}
