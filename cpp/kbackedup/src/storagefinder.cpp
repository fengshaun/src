/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "storagefinder.h"

#include <Solid/Device>

#include <KDebug>

StorageFinder::StorageFinder() {
}

StorageFinder::~StorageFinder() {
}

void StorageFinder::setCriteria(Solid::DeviceInterface::Type type, Solid::StorageDrive::Bus bus) {
	m_type = type;
	m_bus = bus;
}

void StorageFinder::find() {
	QList<Solid::Device> list = Solid::Device::listFromType(Solid::DeviceInterface::StorageAccess, QString());
	
	for (int i = 0; i < list.count(); i++) {
		Solid::Device device = list[i];
		if (device.isValid()) {
			if (device.is<Solid::StorageAccess>()) {
				if (Solid::StorageAccess *sa = device.as<Solid::StorageAccess>()) {
					if (Solid::StorageVolume *sv = device.as<Solid::StorageVolume>()) {
						m_labels.append(sv->label());
						m_udis.append(device.udi());
						m_devices.append(device);
					}
				}
			}
		}
	}
}

QList<QString> StorageFinder::udis() {
	return m_udis;
}

QList<QString> StorageFinder::labels() {
	return m_labels;
}

QList<Solid::Device> StorageFinder::devices() {
	return m_devices;
}