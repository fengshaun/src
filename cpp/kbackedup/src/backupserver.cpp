/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "backupserver.h"

#include <iostream>

BackupServer::BackupServer(QList<QString> sources, Solid::StorageAccess *dev) {
	m_sources = sources;
	m_target = dev;
}

BackupServer::~BackupServer() {
}

void BackupServer::startBackup() {
	emit started();
	if (mountDevice()) {
		connect(m_target, SIGNAL(setupDone(Solid::ErrorType, QVariant, const QString &)),
				this, SLOT(runScript(Solid::ErrorType, QVariant, const QString &)));
	} else {
		runScript();
	}
}

bool BackupServer::mountDevice() {
	if (!m_target->isAccessible()) {
		m_target->setup();
		return true;
	} else {
		return false;
	}
}

void BackupServer::runScript(Solid::ErrorType, QVariant, const QString &) {
	runScript();
}

void BackupServer::runScript() {
	backedup = new QProcess;
	QStringList options;
	
	for (int i = 0; i < m_sources.count(); i++) {
		options << "--dir" << m_sources[i];
	}
	
	options << "--local" << m_target->filePath();
	
	std::cout << "starting the backup process" << std::endl;
	//backedup->setWorkingDirectory("/home/armin/kbackedup/bin");
	backedup->start("./backedup", options);
	
	connect(backedup, SIGNAL(error(QProcess::ProcessError)),
			this, SLOT(error(QProcess::ProcessError)));
	connect(backedup, SIGNAL(finished(int, QProcess::ExitStatus)),
			this, SLOT(backupFinished(int, QProcess::ExitStatus)));
	connect(backedup, SIGNAL(readyRead()),
			this, SLOT(readOutput()));
}

void BackupServer::readOutput() {
	QString message = QString(backedup->readAllStandardOutput());
	std::cout << qPrintable(message);
}

void BackupServer::backupFinished(int exitCode, QProcess::ExitStatus status) {
	delete backedup;
	emit finished();
}

void BackupServer::error(QProcess::ProcessError err) {
	if (err == QProcess::FailedToStart) {
		std::cout << "Failed to execute the backedup script" << std::endl;
		std::cout << "Check to see if the backedup script is executable" << std::endl;
		std::cout << "You can make it executable using 'chmod +x FILE'" << std::endl;
	}
	
	delete backedup;
	emit finished();
}
