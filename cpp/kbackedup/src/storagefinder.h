/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef STORAGEFINDER_H
#define STORAGEFINDER_H

#include <Solid/Device>
#include <Solid/StorageAccess>
#include <Solid/StorageDrive>
#include <Solid/StorageVolume>

#include <QList>
#include <QString>

class StorageFinder
{
	public:
		StorageFinder();
		virtual ~StorageFinder();
		
		virtual void setCriteria(Solid::DeviceInterface::Type, Solid::StorageDrive::Bus);
		virtual void find();
		
		QList<Solid::Device> devices();
		QList<QString> labels();
		QList<QString> udis();
		
	private:		
		QList<Solid::Device> m_devices;
		QList<QString> m_labels;
		QList<QString> m_udis;
		
		Solid::DeviceInterface::Type m_type;
		Solid::StorageDrive::Bus m_bus;
};

#endif // STORAGEFINDER_H
