/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef KBACKUPWIDGET_H
#define KBACKUPWIDGET_H

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

#include <KXmlGuiWindow>
#include <KListWidget>
#include <KPushButton>
#include <KComboBox>
#include <KGuiItem>

class KBackupWidget : public QWidget
{
	public:
		KBackupWidget();
		~KBackupWidget();
		
		void deactivateInterface();
		void activateInterface();
		
		// Widgets
		QLabel *targetLabel;
		KListWidget *listWidget;
		KComboBox *targetCombo;
		KPushButton *addButton;
		KPushButton *removeButton;
		KPushButton *backupButton;
		
		// Layout
		QHBoxLayout *topLayout;
		QHBoxLayout *middleLayout;
		QVBoxLayout *rightButtonLayout;
		QVBoxLayout *mainLayout;
		
	private:
		void layoutWidgets();
};

#endif // KBACKUPWIDGET_H
