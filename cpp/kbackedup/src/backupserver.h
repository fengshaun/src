/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef BACKUPSERVER_H
#define BACKUPSERVER_H

#include <QObject>
#include <QProcess>

#include <Solid/StorageAccess>

class BackupServer : public QObject
{
	Q_OBJECT
	
	public:
		BackupServer(QList<QString>, Solid::StorageAccess *);
		~BackupServer();
		
		void startBackup();
		
	signals:
		void finished();
		void started();
		
	protected slots:
		void runScript();
		void runScript(Solid::ErrorType, QVariant, const QString &);
		
		virtual void backupFinished(int, QProcess::ExitStatus);
		virtual void readOutput();
		void error(QProcess::ProcessError);
		
		bool mountDevice();
		
	private:
		Solid::StorageAccess *m_target;
		QList<QString> m_sources;
		
		QProcess *backedup;
		
};

#endif // BACKUPSERVER_H
