#include "mainwindow.h"

#include <QList>
#include <QString>
#include <QProcess>
#include <QStringList>

#include <KIcon>
#include <KDebug>
#include <KFileDialog>

#include <Solid/Device>
#include <Solid/StorageAccess>
#include <Solid/StorageDrive>

#include <iostream>

MainWindow::MainWindow() : KXmlGuiWindow(0) {
	widget = new KBackupWidget;
	setCentralWidget(widget);
	
	populateTargets();
	
	connect(widget->addButton, SIGNAL(clicked()),
			this, SLOT(addFolder()));
	connect(widget->removeButton, SIGNAL(clicked()),
			this, SLOT(removeFolder()));
	connect(widget->backupButton, SIGNAL(clicked()),
			this, SLOT(startBackup()));
}

MainWindow::~MainWindow() {
}

void MainWindow::populateTargets() {
	StorageFinder finder;
	
	finder.find();
	udiList = finder.udis();
	labelList = finder.labels();
	deviceList = finder.devices();
	
	for (int i = 0; i < labelList.count(); i++) {
		widget->targetCombo->addItem(labelList[i]);
	}
}

void MainWindow::addFolder() {
	QString folderPath = KFileDialog::getExistingDirectory(KUrl(), this, QString("KBackedUp"));
	widget->listWidget->addItem(folderPath);
}

void MainWindow::removeFolder() {
	QListWidgetItem *item = widget->listWidget->currentItem();
	delete item;
}

void MainWindow::startBackup() {
	QList<QString> srcs = sources();
	Solid::StorageAccess *target = (deviceList[widget->targetCombo->currentIndex()]).as<Solid::StorageAccess>();
	
	server = new BackupServer(srcs, target);
	
	connect(server, SIGNAL(started()), this, SLOT(backupStarted()));
	connect(server, SIGNAL(finished()), this, SLOT(backupFinished()));
	
	server->startBackup();
}

QList<QString> MainWindow::sources() {
	QList<QString> srcs;
	for (int i = 0; i < widget->listWidget->count(); i++) {
		srcs.append(widget->listWidget->item(i)->text());
	}
	
	return srcs;
}

void MainWindow::backupStarted() {
	widget->deactivateInterface();
}

void MainWindow::backupFinished() {
	delete server;
	widget->activateInterface();
}