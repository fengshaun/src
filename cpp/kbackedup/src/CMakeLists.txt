include_directories(${QT_INCLUDES} 
					${KDE4_INCLUDES})

set(kbackedup_SRCS
	mainwindow.cpp
	main.cpp
	kbackupwidget.cpp
	storagefinder.cpp
	backupserver.cpp)

kde4_add_executable(kbackedup ${kbackedup_SRCS})
target_link_libraries(kbackedup	${KDE4_KDEUI_LIBS}
								${KDE4_SOLID_LIBS}
								${KDE4_KIO_LIBS})

install(TARGETS kbackedup
		DESTINATION ${BIN_INSTALL_DIR})