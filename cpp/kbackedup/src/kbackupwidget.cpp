/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "./kbackupwidget.h"

KBackupWidget::KBackupWidget() {
	layoutWidgets();
}

KBackupWidget::~KBackupWidget() {
}

void KBackupWidget::layoutWidgets() {
	// Widgets
	targetLabel = new QLabel("Target:");
	listWidget = new KListWidget;
	targetCombo = new KComboBox;
	targetCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	addButton = new KPushButton;
	removeButton = new KPushButton;
	backupButton = new KPushButton("Backup");
	
	// Layout
	topLayout = new QHBoxLayout;
	middleLayout = new QHBoxLayout;
	rightButtonLayout = new QVBoxLayout;
	mainLayout = new QVBoxLayout;
	
	// Widget properties
	addButton->setIcon(KIcon("list-add"));
	removeButton->setIcon(KIcon("list-remove"));
	
	// Laying out
	topLayout->addWidget(targetLabel);
	topLayout->addWidget(targetCombo);
	
	rightButtonLayout->addWidget(addButton);
	rightButtonLayout->addWidget(removeButton);
	rightButtonLayout->addStretch();
	
	middleLayout->addWidget(listWidget);
	middleLayout->addLayout(rightButtonLayout);
	
	mainLayout->addLayout(topLayout);
	mainLayout->addLayout(middleLayout);
	mainLayout->addWidget(backupButton);
	
	setLayout(mainLayout);
}

void KBackupWidget::deactivateInterface() {
	targetCombo->setDisabled(true);
	listWidget->setDisabled(true);
	addButton->setDisabled(true);
	removeButton->setDisabled(true);
	backupButton->setDisabled(true);
	targetLabel->setDisabled(true);
}

void KBackupWidget::activateInterface() {
	targetCombo->setDisabled(false);
	listWidget->setDisabled(false);
	addButton->setDisabled(false);
	removeButton->setDisabled(false);
	backupButton->setDisabled(false);
	targetLabel->setDisabled(false);
}