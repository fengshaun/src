#include <vector>
#include <iostream>
#include <cmath>

bool is_prime(long num) {
    if (num < 2) {
        return false;
    } else if (num < 4) {
        return true;
    } else if (num % 2 == 0) {
        return false;
    } else if (num < 9) {
        return true;
    } else if (num % 3 == 0) {
        return false;
    } else {
        long max = std::sqrt(num);
        for (int i = 5; i < max + 1; i += 6) {
            if (num % i == 0) return false;
            else if (num % (i + 2) == 0) return false;
        }
    }
    
    return true;
}

int main() {
    int counter = 0;
    for (long i = 1; i <= 1000000; i++) {
        if (is_prime(i)) counter++;
    }

    std::cout << counter << std::endl;
}

