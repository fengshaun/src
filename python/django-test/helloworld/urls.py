from django.conf.urls.defaults import *
from helloworld.views import hello, current_datetime
from helloworld.views import hours_ahead

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       (r'^hello/$', hello),
                       (r'^time/$', current_datetime),
                       (r'^time/plus/(\d{1,2})/$', hours_ahead),
                       (r'^admin/(.*)', admin.site.root),
)
