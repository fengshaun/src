from django.http import HttpResponse, Http404
from django import template
from django.template.loader import get_template
from django import shortcuts
import datetime

def hello(request):
    return HttpResponse('Hello Django world!')

def current_datetime(request):
    time = datetime.datetime.now()
    return shortcuts.render_to_response('current_datetime.html', locals())

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    time = datetime.datetime.now() + datetime.timedelta(hours=offset)
    return shortcuts.render_to_response('hours_ahead.html', locals())
