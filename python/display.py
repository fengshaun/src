############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>
# http://fengshaun.wordpress.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US
############################################################################

from Tkinter import *
import tkMessageBox

class Application:
	def __init__( self, master ):
		frame = Frame( master )
		frame.pack()

		self.text = Label( frame, text="Enter:" )
		self.text.grid( row = 0, column = 0 )

		self.txtField = Entry( frame )
		self.txtField.grid( row = 0, column = 1, columnspan = 3 )

		self.btn = Button( frame, text = "display", command = self.display )
		self.btn.grid( row = 0, column = 4 )

	def display( self ):
		tkMessageBox.showinfo( 'Text', 'You typed: %s ' % self.txtField.get() )


root = Tk()
root.title( "Display" )
app = Application( root )
root.mainloop()
