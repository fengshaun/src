############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>
# http://fengshaun.wordpress.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US
############################################################################

import wx

class Application( wx.Frame ):
	def __init__( self, parent, id, title ):
		wx.Frame.__init__( self, parent, id, title, size = ( 250, 100 ) )

		self.text = wx.StaticText( self, -1, '0', ( 150, 50 ) )
		
		self.addBtn = wx.Button( self, -1, '+', ( 10, 10 ) )
		self.subBtn = wx.Button( self, -1, '-', ( 10, 60 ) )

		self.Bind( wx.EVT_BUTTON, self.onPlus, id = self.addBtn.GetId() )
		self.Bind( wx.EVT_BUTTON, self.onMinus, id = self.subBtn.GetId() )

	def onPlus( self, event ):
		value = int( self.text.GetLabel() )
		value += 1
		self.text.SetLabel( str( value ) )

	def onMinus( self, event ):
		value = int( self.text.GetLabel() )
		value -= 1
		self.text.SetLabel( str( value ) )

app = wx.App()
frame = Application( None, -1, 'Communications' )
frame.Show()
app.MainLoop()
