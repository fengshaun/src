import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        
        self.scene = QGraphicsScene()
        self.view = QGraphicsView(self.scene)

        self.layout = QHBoxLayout()
        self.layout.addWidget(self.view)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    w = MainWindow()
    w.show()

    sys.exit(app.exec_())
