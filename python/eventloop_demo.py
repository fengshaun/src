import threading
import os
import Queue
import time
import sys

Q = Queue.Queue()

class EventGenerator(threading.Thread):
    def __init__(self, func, id):
        self.func = func
        self.id = id
        threading.Thread.__init__(self)

    def run(self):
        for i in range(10):
            Q.put((self.func, self.id))
            time.sleep(1)

class EventHandler(object):
    def handleEvents(self, *args, **kargs):
        try:
            func = Q.get()
            func[0](func[1])
            Q.task_done()
        except Queue.Empty:
            pass

def event(id):
    print 'I am an event from %d' % id

if __name__ == '__main__':
    gens = []

    for i in range(5):
        thread = EventGenerator(event, i)
        thread.start()
        gens.append(thread)

    handler = EventHandler()

    while 1:
        handler.handleEvents()
        for t in gens:
            if t.isAlive: 
                print '%d is alive' % t.id
                break
        else: sys.exit(0)
        time.sleep(0.001)
    
