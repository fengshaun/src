import math

def xcombinations(items, n):
    '''xcombinations(items, choose) -> generator
    generates the different possible combinations of 'items'

    example:
        xcombinations([1, 2, 3], 2) -> [1, 2]
                                       [1, 3]
                                       [2, 3]
    '''

    if n < 1: yield []
    else:
        for i in xrange(len(items)):
            for j in xcombinations(items[i + 1:], n - 1):
                yield [items[i]] + j

def xpermutations(items, n=None):
    '''xpermutations(items) -> generator
    generates the different possible permutations of 'items'

    example:
        xpermutations([1, 2]) -> [1, 2] [2, 1]
    '''
    if n is None: n = len(items)
    if n < 1: yield []
    else:
        for i in xrange(len(items)):
            for j in xpermutations(items[:i] + items[i + 1:], n - 1):
                yield [items[i]] + j

def gcd(a, b):
    '''gcd(a, b) -> int
    returns the greatest common divisor of 'a' and 'b'

    example:
        gcd(4, 6) -> 2
        gcd(5, 6) -> 1
    '''
    while b:
        a, b = b, a % b
    return a

def gcd_many(l):
    '''gcd_many(numbers) -> int
    returns the greatest common divisor of 'numbers'

    example:
        gcd_many([4, 6, 10]) ->
    '''
    return reduce(gcd, l)

def lcm(a, b):
    '''lcm(a, b) -> int
    returns the least common multiple of 'a' and 'b'

    example:
        lcm(10, 8) -> 2
    '''
    for i in range(1, min((a, b)) + 1):
        if a % i == 0 and b % i == 0:
            return i

    return a * b
 
def xrotations(items):
    '''xrotations(items) -> generator
    generates the rotations of a number

    example:
        xrotations([1, 2, 3]) -> [1, 2, 3]
                                 [2, 3, 1]
                                 [3, 1, 2]
    '''
    yield items

    # len(items) - 1 because we don't want the
    # original number to be repeated
    for i in range(len(items) - 1):
        items = items[1:] + [items.pop(0)]
        yield items

def is_pandigital(l, n=None):
    '''is_pandigital(number, digits) -> bool
    returns True if 'number' is pandigital of 'digits'

    example:
        is_pandigital([1, 4, 5, 2, 3], 5) -> True
    '''
    l = digits(l)
    if n is None: n = len(l)
    if len(l) != n: return False
    l = sorted(l)
    for i in range(1, n):
        try:
            if l[i - 1] != i:
                return False
        except IndexError:
            return False
    return True

def is_palindromic(num):
    '''is_palindromic(number) -> bool
    returns True if 'number' is palindromic

    example:
        is_palindromic(321123) -> True
    '''
    l = digits(num)

    if not len(l) % 2 == 0:
        l1 = l[:len(l) / 2 + 1]
    else:
        l1 = l[:len(l) / 2]
    l2 = l[len(l) / 2:]
    l2.reverse()

    for i in range(len(l1)):
        if not l1[i] == l2[i]:
            return False

    return True

_hex_codes = {"0":"0000", "1":"0001", "2":"0010", "3":"0011",
              "4":"0100", "5":"0101", "6":"0110", "7":"0111",
              "8":"1000", "9":"1001", "A":"1010", "B":"1011",
              "C":"1100", "D":"1101", "E":"1110", "F":"1111",
              "-":"-"}

def to_binary(number):
    '''to_binary(number) -> string
    converts 'number' to binary

    example:
        to_binary(300) -> '100101100'
    '''
    if number == 0:
        return '0'
    result = [_hex_codes[code] for code in '%X' % number]
    return (''.join(result)).lstrip('0')

def truncate(n, t=1, mode='right'):
    '''truncate(number, digits=1, mode='right') -> int
    truncates the rightmost 'digits' digit or leftmost 'digit' digits of 'number'
    mode can be 'right' or 'left'

    example:
        truncate(5362, 'right') -> 536
        truncate(5362, 2) -> 53
    '''
    l = digits(n)

    if mode == 'right':
        for a in range(t): l.pop()
    elif mode == 'left':
        for a in range(t): l.pop(0)
    return int(''.join([str(x) for x in l]))

def divisors(n):
    '''divisors(number) -> list
    returns a list of 'number's divisors

    example:
        divisors(12) -> 1, 2, 3, 4, 6
    '''
    div = [1]
    high_bound = int(math.sqrt(n))
    if high_bound * high_bound == n:
        # perfect square
        div.append(high_bound)
        high_bound -= 1

    for i in range(2, high_bound + 1):
        if n % i == 0:
            div.append(i)
            div.append(n // i)
    return div

def digits(n):
    return [int(i) for i in str(n)]

def is_permutation(n1, n2):
    if sorted(digits(n1)) == sorted(digits(n2)):
        return True
    else:
        return False

def next_pentagonal_num():
    n = 1

    while 1:
        yield n * (3 * n - 1) / 2
        n += 1

def is_pentagonal(num):
    n = (0.5 + math.sqrt(0.25 + 6 * num)) / 3
    if n - int(n) == 0: return True
    else: return False

pentagonal = lambda num: num * (3 * num - 1) / 2

def triangle_nums(max):
    n = []
    for i in next_triangle_num():
        if i > max:
            break
        n.append(i)

    return n

def next_triangle_num():
    '''next_trangle_num() -> int
    A generator function that Calculates the next
    trianglular number.
    '''
    num = 1
    
    while 1:
        yield n * (n + 1) / 2
        n += 1

def is_triangle(num):
    n = math.sqrt(8.0 * num + 1)
    if n - int(n) == 0: return True
    else: return False

def next_hexagonal_num():
    num = 1

    while 1:
        yield n * (2 * n - 1)
        n += 1

hexagonal = lambda x: x * (2 * x - 1)

def is_hexagonal(num):
    n = (math.sqrt(8 * num + 1) + 1) / 4.0
    if n - int(n) == 0: return True
    else: return False

