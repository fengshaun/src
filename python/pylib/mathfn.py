import math
from pylib import prime
import time

def factorial(n):
    '''factorial(number) -> int
    Returns the factorial of a number

    example:
        factorial(4) -> 24
    '''
    result = 1
    for i in range(n, 1, -1):
        result *= i
    return result

def xfibonacci():
    '''xfibonacci() -> generator
    generates the fibonacci sequence

    example:
        xfibonacci() -> 1, 1, 2, 3, 5, ...
    '''
    a = 1
    b = 1
    yield a
    yield b
    while 1:
        c = a + b
        a = b
        b = c
        yield c


def combination(n, k):
    '''combination(total, choose) -> int
    Calculates the combination in the form:
    XCX where X is a number.

    example:
        combination(7, 4) -> 35
    '''
    if k > n or n == k:
        return 0
    return factorial(n) / (factorial(k) * factorial(n - k))

def permutation(n, k):
    '''permutation(total, choose) -> int
    Calculates the permutation in the form:
    XPX where X is a number.

    example:
        permutation(7, 4) -> 840
    '''
    if k > n or n == k:
        return 0
    return factorial(n) / factorial(n - k)

def triangle_nums(max):
    n = []
    for i in next_triangle_num():
        if i > max:
            break
        n.append(i)

    return n

def next_triangle_num():
    '''next_trangle_num() -> int
    A generator function that Calculates the next
    trianglular number.
    '''
    num = 1
    add_num = 2

    while 1:
        yield num
        num += add_num
        add_num += 1

def possible_factors(pf):
    '''possible_factors(prime_factors) -> int
    calculates the number of possible factors for
    a given list of prime factors which might contain
    duplicates.

    example:
        possible_factors([2, 2, 7]) -> 5
    '''
    # it uses the formula (a_1+1) * (a_2+1) * ...
    # where p_1^a_1 * p_2^a_2 * ...
    # using a nested list works
    possible_fac = 1

    l = []

    try:
        num = pf[0]
    except IndexError:
        return 0

    l_of_l = []
    l_of_l.append(num)

    for i in range(1, len(pf)):
        if pf[i] == num:
            l_of_l.append(pf[i])
        else:
            l.append(l_of_l)
            l_of_l = []
            num = pf[i]
            l_of_l.append(num)

    l.append(l_of_l)

    for i in l:
        possible_fac *= (len(i) + 1)

    return possible_fac

if __name__ == '__main__':
    print 'Factorial of 7 is:', fac(7)
    print '4C7:', comb(4, 7)
    print '4P7:', perm(4, 7)
    print 'Possible factors of [2, 2, 3]', possible_factors([2, 2, 3])
    print 'First 20 trianglular numbers:\n', [next_triangle_num() for i in range(20)]
