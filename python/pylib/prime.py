import math

def is_prime(num):
    '''is_prime(number) -> bool
    Determines if a number is prime or not

    example:
        is_prime(7) -> True
    '''
    if num < 2:         return False
    elif num < 4:       return True
    elif not num % 2:   return False
    elif num < 9:       return True
    elif not num % 3:   return False
    else:
        for n in range(5, int(math.sqrt(num) + 1), 6):
            if not num % n:
                return False
            elif not num % (n + 2):
                return False

    return True

def primes(min, max):
    min = min - 1 if min % 2 == 0 else min

    for i in xrange(min, max, 2):
        if is_prime(i): yield i

def primes_max(max):
    '''primes_max(maximum) -> generator
    generates primes up to 'maximum'

    example:
        primes_max(5) -> 2, 3
    '''
    yield 2
    for n in range(3, max, 2):
        if is_prime(n):
            yield n

def next_prime():
    '''next_prime() -> generator
    generates primes up to an undefined upperbound

    example:
        next_prime() -> 2, 3, 5, ...
    '''
    yield 2
    n = 3
    while 1:
        if is_prime(n):
            yield n
        n += 2

def primes_count(count):
    '''primes_count(count) -> generator
    generates the first 'count' primes

    example:
        primes_count(4) -> 2, 3, 5, 7
    '''
    counter = 1
    num = 3

    yield 2

    while counter < count:
        if is_prime(num):
            yield num
            counter += 1
        num += 2

def prime_factors(num):
    factors = []
    if num % 2 == 0:
        factors.append(2)
        while num % 2 == 0:
            num /= 2

    factor = 3
    max_factor = int(math.sqrt(num))
    while num > 1 and factor <= max_factor:
        if num % factor == 0:
            factors.append(factor)
            while num % factor == 0:
                num /= factor

            max_factor = int(math.sqrt(num))
        factor += 2

    if num != 1:
        factors.append(num)

    return factors

# unit test 
if __name__ == '__main__':
    print 'printing prime numbers up to 60:'
    for i in primes_max(60):
        print i,

    print '\nprinting the first 20 prime numbers:'
    for i in primes_count(20):
        print i,
