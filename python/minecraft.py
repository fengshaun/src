#!/usr/bin/python

# requires python >= 3.3.0a3

import sys, os
import urllib.request
import urllib.error
import time
import re
import lzma
import zipfile
import xml.etree.ElementTree as ET

#####################################################
################### CONFIGURATION ###################
#####################################################

baseUrl = "http://207.171.163.225:80"

dotlinux = os.path.join(os.path.expanduser("~"), ".minecraft")
dotwindows = os.path.join(os.path.expanduser("~"), "AppData", "Roaming", ".minecraft")
dotmacosx = os.path.join("/Library", "Application Support", "minecraft")

config = {}

if sys.platform.startswith("linux"):
    config["dotminecraft"] = dotlinux
    config["OS"] = "linux"
elif sys.platform.startswith("win"):
    config["dotminecraft"] = dotwindows
    config["OS"] = "windows"
elif sys.platform.startswith("darwin"):
    config["dotminecraft"] = dotmacosx
    config["OS"] = "macosx"
else:
    print("Sorry, the location of .minecraft could not be determined.")

config["files"] = ["%s/MinecraftDownload/%s" % (baseUrl, i) for i in ["lwjgl.jar",
                                                                   "lwjgl_util.jar",
                                                                   "jinput.jar",
                                                                   "minecraft.jar",
                                                                  ]]
config["files"] += ["%s/MinecraftDownload/%s_natives.jar.lzma" % (baseUrl, config["OS"])]
config["resources"] = "%s/MinecraftResources" % baseUrl
config["launcher"] = "%s/MinecraftDownload/launcher/minecraft.jar" % baseUrl

#####################################################
####################### MAIN ########################
#####################################################

def help():
    print("""Usage: %s " % sys.argv[0]
This program is lisenced under GPLv3.  It will download the latest minecraft to
an appropriate location for you to enjoy.

Please make sure you have removed all traces of .minecraft directory from any
previous installation.""")
    exit(1)

if __name__ == "__main__":
    if len(sys.argv) > 1 and (sys.argv[1] == "--help" or sys.argv[1] == "-h"):
        help()

    saved_path = os.getcwd()

    ## prepare directories/environment
    print("Preparing Environment")
    print("Creating %s" % config["dotminecraft"])
    os.mkdir(config["dotminecraft"])

    os.mkdir("%s/bin" % config["dotminecraft"])
    os.mkdir("%s/bin/natives" % config["dotminecraft"])
    os.mkdir("%s/resources" % config["dotminecraft"])

    ## download needed files
    print("Downloading libraries")
    for url in config["files"]:
        print("Downloading %s" % url.split("/")[-1])
        urllib.request.urlretrieve(url, "%s/bin/%s" % (config["dotminecraft"], url.split("/")[-1]))

    print("Downloading launcher")
    urllib.request.urlretrieve(config["launcher"], "%s/MinecraftLauncher.jar" % config["dotminecraft"])
    print("Downloading resources file")
    urllib.request.urlretrieve(config["resources"], "%s/resources.xml" % config["dotminecraft"])
    time.sleep(1)

    ## parse and download resources
    print("Setting up resources")
    print("Parsing resources.xml")
    tree = ET.parse("%s/resources.xml" % config["dotminecraft"])

    for elem in tree.iter():
        if elem.tag.endswith("Key"):
            f = "%s/MinecraftResources/%s" % (baseUrl, elem.text)
            name = re.search(".*MinecraftResources/(.*)", f).group(1)
            if name.endswith("/"):
                os.mkdir("%s/resources/%s" % (config["dotminecraft"], name))
            else:
                try:
                    print("Downloading %s" % f)
                    urllib.request.urlretrieve(f, "%s/resources/%s" % (config["dotminecraft"], name))
                except urllib.error.HTTPError as e:
                    print("Error:" + str(e))
                except FileNotFoundError as e:
                    print("File not found: %s" % f)


    ## write version file
    ## minecraft needs this file, but the contents don't matter much
    ## as long as there are 15 bytes and the first one is \x00
    print("Writing version file")
    f = open(os.path.join(config["dotminecraft"], "bin", "version"), "wb")
    f.write(b"\x00\x02\x03\x04\x05\x06\x07\x08\x09\x10\x11\x12\x13\x14\x15")
    f.close()

    ## decompress native files
    print("Decompressing native files")
    native_lzma = "%s/bin/%s_natives.jar.lzma" % (config["dotminecraft"], config["OS"])
    with lzma.LZMAFile(native_lzma) as f:
        with open("%s/bin/natives/%s_natives.jar" % (config["dotminecraft"], config["OS"]), "wb") as o:
            o.write(f.read())

    os.chdir("%s/bin/natives/" % config["dotminecraft"])
    f = zipfile.ZipFile("%s/bin/natives/%s_natives.jar" % (config["dotminecraft"], config["OS"]))
    f.extractall()
    f.close()
