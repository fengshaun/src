#!/usr/bin/python2

# pop.mail.yahoo.com
# port 995
# +SSL

# TODO
# connect to FTP and send it directly to my comp

import poplib as pop
import string
import getpass
import os
import sys
import random

# making output unbuffered
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

user = "feng.shaun@yahoo.com"
server = pop.POP3_SSL("pop.mail.yahoo.com", 995)

try:
    server.user(user)
    print "Please enter your password for " + user
    print "P.S. Thank you.  It is immensely appreciated. :)"
    print "Please note that even though you type your password, nothing is shown in the terminal."
    server.pass_(getpass.getpass())
except pop.error_proto as e:
    print "Oh noes!  Something went wrong!"
    print "Contact me at amoradi@fedoraproject.org with the follow info:"
    print
    print e
    sys.exit(1)

filename = list(string.letters + string.digits)
random.shuffle(filename)
filename = "".join(filename)[:8]

# we'll leave this buffered for performance reasons
f = open(filename, "w")
separator = "\n\n" + "=" * 50 + "\n" + "=" * 50 + "\n\n"

total_size = server.list()[2]

print "Please be patient and don't close this window.\nThank you!"
print
print "Working...0%\r",

msgInfo = server.list()[1]

for msg in msgInfo:
    msgNum = int(string.split(msg, " ")[0])
    print "Working...%s%%\r" % (round(float(msgNum) / total_size * 100, 1)),

    message = string.join(server.retr(msgNum)[1], "\n")
    f.write(message)
    f.write(separator)

f.close()

t2 = time.time()

print
print "time: " + str(t2 - t1)
print "Done.  Again, thank you very much."
print "downloaded to " + filename
