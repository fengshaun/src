import gtk
import webkit
import gobject

gobject.threads_init()

w = gtk.Window()
w.set_title('webtest.py')
w.connect('destroy', gtk.main_quit)

v = webkit.WebView()
v.open('https://talkgadget.google.com/talkgadget/client')

w.add(v)
w.show_all()

gtk.main()
