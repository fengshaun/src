import gtk
import re
import gobject
import urllib
import urllib2

w = gtk.Window()
w.connect('destroy', gtk.main_quit)

def on_insert_text(entry, text, tlen, pos):
    if re.match('^https?://[^ ]+', text) and tlen > 20:
        apiurl = 'http://is.gd/api.php?' + urllib.urlencode(dict(longurl=text))
        shorturl = urllib2.urlopen(apiurl).read()
        entry.insert_text(shorturl, entry.get_position())
        entry.stop_emission('insert-text')
        gobject.idle_add(entry.set_position, entry.get_position() + len(shorturl))

e = gtk.Entry()
e.connect('insert-text', on_insert_text)

w.add(e)
w.show_all()

gtk.main()
