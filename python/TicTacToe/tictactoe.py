#!/usr/bin/python
#
############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>   #
# http://fengshaun.wordpress.com                                           #
#                                                                          #
# This program is free software; you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation; either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program; if not, write to the Free Software              #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US   #
############################################################################

import wx

class MainWindow( wx.Frame ):
	def __init__( self ):
		wx.Frame.__init__( self, None, -1, title = 'TicTacToe' )
		self.panel = wx.Panel( self, -1 )

		self.counter = 0

		self.initWidgets()
		self.initMenus()

		self.Center()
		self.Show()

	def initWidgets( self ):
		self.sb = self.CreateStatusBar()
		self.sb.SetStatusText( 'A game of tic tac toe' )

		self.b1 = wx.Button( self.panel, 1, '-' )
		self.b2 = wx.Button( self.panel, 2, '-' )
		self.b3 = wx.Button( self.panel, 3, '-' )
		self.b4 = wx.Button( self.panel, 4, '-' )
		self.b5 = wx.Button( self.panel, 5, '-' )
		self.b6 = wx.Button( self.panel, 6, '-' )
		self.b7 = wx.Button( self.panel, 7, '-' )
		self.b8 = wx.Button( self.panel, 8, '-' )
		self.b9 = wx.Button( self.panel, 9, '-' )

		self.resetButton = wx.Button( self.panel, 10, 'Reset' )

		self.b1.Bind( wx.EVT_BUTTON, self.onB1 )
		self.b2.Bind( wx.EVT_BUTTON, self.onB2 )
		self.b3.Bind( wx.EVT_BUTTON, self.onB3 )
		self.b4.Bind( wx.EVT_BUTTON, self.onB4 )
		self.b5.Bind( wx.EVT_BUTTON, self.onB5 )
		self.b6.Bind( wx.EVT_BUTTON, self.onB6 )
		self.b7.Bind( wx.EVT_BUTTON, self.onB7 )
		self.b8.Bind( wx.EVT_BUTTON, self.onB8 )
		self.b9.Bind( wx.EVT_BUTTON, self.onB9 )

		self.resetButton.Bind( wx.EVT_BUTTON, self.reset )

		self.grid = wx.GridSizer( 3, 3, 5, 5 )
		self.grid.AddMany( [ ( self.b1, 0, wx.EXPAND ),
				( self.b2, 0, wx.EXPAND ),
				( self.b3, 0, wx.EXPAND ),
				( self.b4, 0, wx.EXPAND ),
				( self.b5, 0, wx.EXPAND ),
				( self.b6, 0, wx.EXPAND ),
				( self.b7, 0, wx.EXPAND ),
				( self.b8, 0, wx.EXPAND ),
				( self.b9, 0, wx.EXPAND ) ] )
		
		self.vbox = wx.BoxSizer( wx.VERTICAL )
		self.vbox.Add( self.resetButton, 0, wx.EXPAND )
		self.vbox.AddSizer( self.grid, 1, wx.EXPAND )

		self.panel.SetSizer( self.vbox )

	def initMenus( self ):
		self.mb = wx.MenuBar()
		self.fileM = wx.Menu()
		self.helpM = wx.Menu()

		self.quitMI = wx.MenuItem( self.fileM, 1, '&Quit\tCtrl+Q', 'Quit the game' )
		self.quitMI.SetBitmap( wx.Bitmap( 'icons/exit.png' ) )
		self.aboutMI = wx.MenuItem( self.helpM, 2, '&About...', 'Learn about this software' )

		self.fileM.AppendItem( self.quitMI )
		self.helpM.AppendItem( self.aboutMI )

		self.mb.Append( self.fileM, '&File' )
		self.mb.Append( self.helpM, '&Help' )

		self.SetMenuBar( self.mb )

		self.Bind( wx.EVT_MENU, self.onQuit, self.quitMI, id = 1 )
		self.Bind( wx.EVT_MENU, self.onAbout, self.aboutMI, id = 2 )

	def onB1( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b1.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b1.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB2( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b2.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b2.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB3( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b3.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b3.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB4( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b4.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b4.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB5( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b5.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b5.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB6( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b6.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b6.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB7( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b7.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b7.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB8( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b8.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b8.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def onB9( self, event ):
		self.showPlayer()

		if self.counter % 2 == 0:
			self.b9.SetLabel( 'X' )
		elif self.counter % 2 != 0:
			self.b9.SetLabel( 'O' )

		self.counter += 1

		self.check()

		event.Skip()

	def isFull( self ):
		if self.b1.Label != '-':
			if self.b2.Label != '-':
				if self.b3.Label != '-':
					if self.b4.Label != '-':
						if self.b5.Label != '-':
							if self.b6.Label != '-':
								if self.b7.Label != '-':
									if self.b8.Label != '-':
										if self.b9.Label != '-':
											return True
										else: return False
									else: return False
								else: return False
							else: return False
						else: return False
					else: return False
				else: return False
			else: return False
		else: return False

	def reset( self, event ):
		for i in ( self.b1, self.b2, self.b3, self.b4, self.b5, self.b6,
				self.b7, self.b8, self.b9 ):
			i.SetLabel( '-' )

		self.counter = 0
		self.sb.SetStatusText( 'A game of tic tac toe' )

		event.Skip()

	def check( self ):
		if self.xWon():
			wx.MessageBox( 'X has won', 'X won' )
			self.sb.SetStatusText( 'X has won the game' )
		elif self.oWon():
			wx.MessageBox( 'O has won', 'O won' )
			self.sb.SetStatusText( 'O has won the game' )
		elif self.isFull():
			wx.MessageBox( 'Board is full, press reset to restart the game', 'board is full' )
			self.sb.SetStatusText( 'Board is full, reset to restart' )

	def xWon( self ):
		if self.b1.Label == 'X':
			if self.b2.Label == 'X':
				if self.b3.Label == 'X':
					return True
		
		if self.b4.Label == 'X':
			if self.b5.Label == 'X':
				if self.b6.Label == 'X':
					return True

		if self.b7.Label == 'X':
			if self.b8.Label == 'X':
				if self.b9.Label == 'X':
					return True

		if self.b1.Label == 'X':
			if self.b4.Label == 'X':
				if self.b7.Label == 'X':
					return True

		if self.b2.Label == 'X':
			if self.b5.Label == 'X':
				if self.b8.Label == 'X':
					return True

		if self.b3.Label == 'X':
			if self.b6.Label == 'X':
				if self.b9.Label == 'X':
					return True

		if self.b1.Label == 'X':
			if self.b5.Label == 'X':
				if self.b9.Label == 'X':
					return True

		if self.b3.Label == 'X':
			if self.b5.Label == 'X':
				if self.b7.Label == 'X':
					return True

		return False

	def oWon( self ):
		if self.b1.Label == 'O':
			if self.b2.Label == 'O':
				if self.b3.Label == 'O':
					return True
		
		if self.b4.Label == 'O':
			if self.b5.Label == 'O':
				if self.b6.Label == 'O':
					return True

		if self.b7.Label == 'O':
			if self.b8.Label == 'O':
				if self.b9.Label == 'O':
					return True

		if self.b1.Label == 'O':
			if self.b4.Label == 'O':
				if self.b7.Label == 'O':
					return True

		if self.b2.Label == 'O':
			if self.b5.Label == 'O':
				if self.b8.Label == 'O':
					return True

		if self.b3.Label == 'O':
			if self.b6.Label == 'O':
				if self.b9.Label == 'O':
					return True

		if self.b1.Label == 'O':
			if self.b5.Label == 'O':
				if self.b9.Label == 'O':
					return True

		if self.b3.Label == 'O':
			if self.b5.Label == 'O':
				if self.b7.Label == 'O':
					return True

		return False

	def showPlayer( self ):
		if self.counter % 2 == 0:
			self.sb.SetStatusText( 'Player O' )
		elif self.counter % 2 != 0:
			self.sb.SetStatusText( 'Player X' )

	def onQuit( self, event ):
		self.Close()

	def onAbout( self, event ):
		description = '''TicTacToe is a simple two player fun game of tic tac toe or XO'''

		license = '''TicTacToe is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US'''

 		info = wx.AboutDialogInfo()

		info.SetName( 'TicTacToe' )
		info.SetVersion( '0.0.1' )
		info.SetDescription( description )
		info.SetCopyright( '(C) 2008 Armin Moradi' )
		info.SetWebSite( 'http://fengshaun.wordpress.com' )
		info.SetLicense( license )
		info.AddDeveloper( 'Armin Moradi' )
		info.AddDocWriter( 'Armin Moradi' )

		wx.AboutBox( info )


app = wx.App()
frame = MainWindow()
app.MainLoop()
