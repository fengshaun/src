import goocanvas
import gtk
import sys

def on_rect_button_press(view, target, event, data=None):
    print 'rect_item received button press event'
    return True

def on_delete_event(window, event, data=None):
    gtk.main_quit()

def main():
    window = gtk.Window()
    scrolled_win = gtk.ScrolledWindow()
    canvas = goocanvas.Canvas()

    root = canvas.get_root_item()

    window.set_default_size(640, 600)
    window.show()
    window.connect('delete-event', on_delete_event)

    scrolled_win.set_shadow_type(gtk.SHADOW_IN)
    scrolled_win.show()

    window.add(scrolled_win)

    canvas.set_size_request(600, 450)
    canvas.set_bounds(0, 0, 1000, 1000)
    canvas.show()
    scrolled_win.add(canvas)

    # Add a few simple items
    for i in range(5):
        goocanvas.Rect(parent=root,
                       x=150,
                       y=200 + (i*50),
                       height=20,
                       width=40+(i*40),
                       radius_x=10,
                       radius_y=10,
                       stroke_color='black',
                       fill_color='darkgreen'
                       )
    gtk.main()

if __name__ == '__main__':
    main()
