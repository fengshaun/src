import os, sys
import curses
import textwrap
import commands
import facebook

CONFIG_FILE = os.path.join(os.path.expanduser("~"), ".fbcurse")
API_KEY     = "19a90d1cf69eaab57d82d54f3bcaba44"
SECRET      = "5e55410f8380f1e6e2a1d876c40be37a"

# global variables used later on.
fb = None
scr = None

def beginCurses():
    global scr
    scr = curses.initscr()
    curses.start_color()
    curses.use_default_colors()
    curses.noecho()
    curses.cbreak()
    scr.keypad(1)

def endCurses():
    curses.echo()
    curses.nocbreak()
    scr.keypad(0)
    curses.endwin()

def configToken():
    with open(CONFIG_FILE, "r") as cf:
        for line in cf.readlines():
            line = line.strip().split("=")
            if line[0] == "token":
                return line[1]
    return ""

def saveCredentials():
    with open(CONFIG_FILE, "a") as cf:
        cf.write("session_key=%s\n" % fb.session_key)
        cf.write("secret=%s\n" % fb.secret)

def login():
    global fb

    print "logging in"
    d = {"session_key": "", "secret": ""}
    with open(CONFIG_FILE, "r") as cf:
        for line in cf.readlines():
            line = line.strip().split("=")
            if line[0] == "session_key":
                d["session_key"] = line[1]
                print "session_key found: %s" % d["session_key"]
            elif line[0] == "secret":
                d["secret"] = line[1]
                print "secret found: %s" % d["secret"]
    
    if not (d["session_key"] or d["secret"]):
        print "session_key/secret not found, looking for token"
        token = configToken()
        
        if not token:
            print "token not found"
            print ("Please go to http://www.facebook.com/code_gen.php?v=1.0&api_key=19a90d1cf69eaab57d82d54f3bcaba44\n" +
                   "and click 'Generate' to create a login token.  Then copy that token in ~/.fbcurse like so:\n" +
                   "token=TOKEN\n" +
                   "where TOKEN is the token you got from facebook.  Then rerun this application.")
            return False

        print "token: %s" % token
        fb = facebook.Facebook(API_KEY, SECRET, token)
        fb.auth.getSession()
        saveCredentials()

        if not fb.users.hasAppPermission(ext_perm='read_stream', uid=fb.uid):
            print ("Please go to http://www.facebook.com/connect/prompt_permissions.php?v=1.0&api_key=19a90d1cf69eaab57d82d54f3bcaba44&ext_perm=read_stream&extern=1\n" +
                   "and click 'Allow' to give this application the required permission.")
            return False

        return fb

    else:
        fb = facebook.Facebook(API_KEY, SECRET)
        fb.session_key = d["session_key"]
        fb.secret = d["secret"]
        fb.uid = d["session_key"].split("-")[1]
        return fb

def posts():
    stream = fb.stream.get(limit=20)
    p = []

    for post in stream["posts"]:
        for pro in stream["profiles"]:
            if post["actor_id"] == pro["id"]:
                # find the name of the poster
                message = "%s" % pro["name"]

                # is it a wall-to-wall post?
                if "target_id" in post:
                    for pro in stream["profiles"]:
                        if post["target_id"] == pro["id"]:
                            message += " -> %s " % pro["name"]

                # cut the whole thing down to 15 characters
                if len(message) > 15:
                    message = message[0:15]

                # add the message in a cool tabulated format
                message += "\t| "
                message += "\n\t\t| ".join(textwrap.wrap(post["message"], scr.getmaxyx()[1] - 22)) if post["message"] else "(no message attached)"
                message += "\n"

                # if "attribution", it's from an app and if "attachment", it has some sort of cool link
                if post["attribution"]:
                    message += "\t\t| from '%s' app\n" % post["attribution"]
                elif "attachment" in post and "media" in post["attachment"]:
                    for m in post["attachment"]["media"]:
                        name = post["attachment"]["name"] if "name" in post["attachment"] else "<name of attachment could not be retrieved>"
                        name = name[:scr.getmaxyx()[1] - 26]
                        message += "\t\t| %s: %s\n\t\t|\t%s\n" % (m["type"], name,m["href"])

                message += "\t\t| (%s) comments, (%s) likes\n" % (post["comments"]["count"] if "count" in post["comments"] else "-",
                                                                  post["likes"]["count"] if "count" in post["likes"] else "-")
                message += "-" * 16 + "+" + "-" * (scr.getmaxyx()[1] - 17)

                p.append(message)

    return p

def header():
    return "FbCurse -- %s: (%s) messages\n\n" % (fb.users.getInfo([fb.uid], ["name"])[0]["name"],
                                                 fb.notifications.get()["messages"]["unread"])

def update():
    scr.clear()
    scr.addstr(header())

    for post in posts():
        try:
            scr.addstr(post)
        except curses.error:
            # it's printing past screen
            break
        except:
            pass

    scr.refresh()

if __name__ == "__main__":
    beginCurses()

    fb = login()

    if not fb:
        endCurses()

    update()

    while True:
        try:
            c = chr(scr.getch())
        except ValueError:
            c = ""
    
        if c == "q":
            break
        elif c == "u":
            update()

    endCurses()

