import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class SampleWindow(QMainWindow):
    
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.button = QPushButton('Hello PyQt4', self)
        self.setWindowTitle('Sample PyQt4 Window')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = SampleWindow()
    w.show()
    sys.exit(app.exec_())
