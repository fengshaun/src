import math

LENGTH = 1000000
primes = [0 for k in range(LENGTH)]  # initializing a list to 0 (False)

def primes_sieve2():
    primes[2] = 1
    primes[3] = 1
    primes[5] = 1
    primes[7] = 1

    for i in range(11, LENGTH, 6):
        primes[i] = 1
        primes[i + 2] = 1

    for i in range(5, int(math.sqrt(LENGTH)), 2):
        if (primes[i]):
            for j in range(i * i, LENGTH, 2 * i):
                primes[i] = 0

if __name__ == '__main__':
    primes_sieve2()

    # for the sake of making sure the number is correct
    acc = 0
    for i in range(len(primes)):
        if primes[i]:
            acc += 1
    print acc
