#!/usr/bin/python

############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>
# http://fengshaun.wordpress.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US
############################################################################


import pickle

D = { }
name = ''
number = 0

try:
	file = open( 'adb', 'r' )
except:
	print "ERROR: Cannot load file \"adb\"\nPlease make sure that \"adb\" is in the same directory as the script"
	exit()

try:
	D = pickle.load( file ) #loading the dictionary
except:
	pass #adb is empty

file = open( 'adb', 'r+w' )

print "For searching a particular phone# press 1"
print "For adding an entry press 2"

opt = raw_input()

if opt == '1':
	print "enter the desired name: "
	name = raw_input()
	
	for key in sorted( D ): 
		if key == name:
			number = D[ key ]
			print number
			exit()

	print "no entry found"
	exit()

elif opt == '2':
	print "enter a name and a number ( 0 to exit ): "
	s = raw_input()

	while s != '0':
		l = s.split()
		name = l[ 0 ]
		number = l[ 1 ]
		D[ name ] = number

		s = raw_input()

	try:
		pickle.dump( D, file )
	except:
		print 'ERROR: Cannot save recent entries'

	exit()

else:
	print "inappropriate option"
	exit()
