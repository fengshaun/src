############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>
# http://fengshaun.wordpress.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US
############################################################################

from Tkinter import *
import tkMessageBox
import pickle

class Application:
	def __init__( self, master ):
		#To keep the records
		self.D = {}

		#where else to save data?
		try:
			print 'opening database'
			self.file = open( 'phonebookdb', 'rb' )
			print 'open successful'
	
			print 'loading database'
			if not self.checkFile():
				print 'database empty'
				self.file.close()
			else:
				self.file.seek( 0 )
				self.D = pickle.load( self.file )
				print 'loading successful'
				self.file.close()
				self.printDatabase()
	
		except:
			print 'Error occurred while trying to load database'
			exit()
	
		#now we have the database in the memory, we can reset the file (inefficient but works)
		self.file = open( 'phonebookdb', 'wb' )

		frame = Frame( master )
		frame.pack()

		self.nameLabel = Label( frame, text = 'Name:' )
		self.nameLabel.grid( row = 0, column = 0 )
		
		self.phoneLabel = Label( frame, text = 'Phone #:' )
		self.phoneLabel.grid( row = 1, column = 0 )

		self.nameEntry = Entry( frame )
		self.nameEntry.grid( row = 0, column = 1, columnspan = 3 )
		
		self.phoneEntry = Entry( frame )
		self.phoneEntry.grid( row = 1, column = 1, columnspan = 3 )

		self.searchBtn = Button( frame, text = 'Search', command = self.search )
		self.searchBtn.grid( row = 0, column = 4 )

		self.addBtn = Button( frame, text = 'Add', command = self.add )
		self.addBtn.grid( row = 1, column = 4 )

		self.removeBtn = Button( frame, text = 'Remove', command = self.remove )
		self.removeBtn.grid( row = 2, column = 4 )

	def add( self ):
		self.name = self.nameEntry.get()
		self.number = self.phoneEntry.get()
		print 'adding contact %s %s' % ( self.name, self.number ),

		if self.name not in self.D:
			self.D[ self.name ] = self.number

			print 'succeeded'
			tkMessageBox.showinfo( title = 'Contact added', message = 'Contact %s with phone number %s successfully added' % ( self.name, self.number ) )
		else:
			print 'failed'
			tkMessageBox.showerror( title = 'Error occurred', message = 'Contact %s with phone number %s could not be added\nThere is an existing contact with the same name' % ( self.name, self.number ) )

	def remove( self ):
		self.name = self.nameEntry.get()

		if self.name in self.D:
			print 'removing %s %s' % ( self.name, self.D[ self.name ] )
			self.D.pop( self.name )
			tkMessageBox.showinfo( title = 'Contact removed', message = 'Contact %s is successfully removed' % self.name )
		else:
			print 'contact not in database'
			tkMessageBox.showerror( title = 'Error occurred', message = 'No contact with name %s exists in phonebook' % self.name )

	def search( self ):
		self.name = self.nameEntry.get()

		if self.name in self.D:
			print 'contact found'
			self.number = self.D[ self.name ]
			tkMessageBox.showinfo( title = 'Contact found', message = 'Name: %s\nPhone #: %s' % ( self.name, self.number ) )
		else:
			print 'contact not found'
			tkMessageBox.showinfo( title = 'Contact not found', message = 'Name %s not found in the database' % self.name )

	def save( self ):
		print 'saving state'
		try:
			pickle.dump( self.D, self.file )
		except:
			print 'Error occurred while saving the database'
			exit()

	def checkFile( self ):
		rl = self.file.readline()
		if rl == '': #empty
			return 0
		else:
			return 1

	def printDatabase( self ):
		print 'Database contents:'
		for key in self.D:
			print key, self.D[ key ]
