import sys
import inspect

from gtk import gdk
import goocanvas
import gtk
import gobject
import cairo

DEBUG=1
DEBUG_PAINT=1

def debug(message):
    if DEBUG:
        print '(%s, %s): %s' % (inspect.stack()[1][1],
                                inspect.stack()[1][2],
                                message)

def debug_paint(*message):
    if DEBUG_PAINT:
        print '(%s, %s): %s' % (inspect.stack()[1][1],
                                inspect.stack()[1][2],
                                message)

class PegStyle(goocanvas.Style):
    def __init__(self):
        goocanvas.Style.__init__(self)

    def set_fill_options(self, cr):
        debug_paint('set_fill_options')
        cr.set_source_rgb(1, 1, 1)

    def set_stroke_options(self, cr):
        debug_paint('set_fill_options')
        cr.set_dash(0)
        cr.set_line_cap(cairo.LINE_CAP_ROUND)
        cr.set_line_width(10)
        
class Peg(goocanvas.ItemSimple):
    def __init__(self, x, y, width, height=25, parent=None):
        goocanvas.ItemSimple.__init__(self, parent=parent)

        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.__is_selected = False

        #self.set_style(PegStyle())
        
        self.connect('button-press-event', self.on_press)
        self.connect('motion-notify-event', self.on_motion_notify)
        self.connect('button-release-event', self.on_release)

        # a tuple of (x, y) of the center point of this Peg
        self.center = (self.x + self.width / 2.0,
                       self.y + self.height / 2.0)

    def update(self, entire_tree, cr):
        debug_paint('UPDATING %s' % self)
            
        self.bounds.x1 = self.x
        self.bounds.y1 = self.y
        self.bounds.x2 = self.x + self.width
        self.bounds.y2 = self.y + self.height

        return goocanvas.Bounds(x1, y1, x2, y2)
    
    def paint(self, cr, bounds, scale):
        debug_paint('PAINTING %s' % self)
            
        cr.move_to(bounds.x1 + self.width / 2, bounds.y1)
        cr.set_line_width(10)
        cr.line_to(self.x + self.width, self.y + self.height)
        cr.line_to(self.x, self.y + self.height)
        cr.close_path()

        self.simple_data.style.set_fill_options(cr)
        self.simple_data.style.set_stroke_options(cr)

        cr.stroke()
        cr.fill()

    def is_item_at(self, x, y, cr, is_pointer_event):
        debug_paint('IS ITEM AT %s' % self)
        
        if x >= self.x and x <= self.x + self.width:
            if y >= self.y and y <= self.y + self.height:
                return True
        return False
    
    '''
    def paint_path(self, cr):
        debug_paint('PAINTING PATH %s' % self)
    
    def create_path(self, cr):
        debug_paint('CREATING PATH %s' % self)

        cr.rectangle(self.x, self.y, self.width, self.height)

        self.changed(True)
    '''
    def set_selected(self, yes=True):
        debug('%s %s SELECTED' % (self, 'IS' if yes else 'IS NOT'))
            
        self.__is_selected = bool(yes)

    def is_selected(self):
        return self.__is_selected
        
    def on_press(self, widget, target, event):
        # we start the dragging process here,
        # as soon as the user clicks on a peg
        # we set the drag center to the event
        # point.  Then, we use the different of
        # the later motion-notify-event points
        # and this drag center point to move
        # the peg.
        
        debug('-' * 40)
        debug('ON PRESSED')
        debug(self)
        debug(widget)
        debug(target)
        debug(event)
        debug('-' * 40)

        self.center = (event.x, event.y)
        self.set_selected(True)

    def on_release(self, widget, target, event):
        debug('-' * 40)
        debug('ON PRESSED')
        debug(self)
        debug(widget)
        debug(target)
        debug(event)
        debug('-' * 40)

        # set the peg to not selected so on_motion_notify
        # so that it doesn't move pegs out of the blue
        self.set_selected(False)

    def on_motion_notify(self, widget, target, event):
        debug('-' * 40)
        debug('ON PRESSED')
        debug(self)
        debug(widget)
        debug(target)
        debug(event)
        debug('-' * 40)
            
        if self.is_selected() and (event.state & gdk.BUTTON1_MASK):
            self.translate(event.x - self.center[0],
                           event.y - self.center[1])
        else:
            debug("%s CAN'T MOVE: OBJECT IS NOT SELECTED" % self)

class HanoiWindow(gtk.Window):
    def __init__(self):
        debug('Setting up main window')
            
        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)
        
        self.set_title('The tower of Hanoi')
        self.set_default_size(700, 480)
        self.connect('delete-event', gtk.main_quit)

        debug('Setting up canvas')
        
        self.canvas = goocanvas.Canvas()
        self.canvas.set_size_request(700, 480)
        self.canvas.set_bounds(0, 0, 700, 480)
        
        self.root_item = self.canvas.get_root_item()

        debug('Adding root items to canvas')

        for i in range(5):
            Peg(parent=self.root_item,
                x=150,
                y=200 + (i * 35),
                width=40 + (i * 40))

        self.add(self.canvas)

        debug('Done.')
        
        self.show_all()

if __name__ == '__main__':
    w = HanoiWindow()
    gtk.main()
