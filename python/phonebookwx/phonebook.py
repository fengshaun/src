#!/usr/bin/env python
#
############################################################################
# General Ledger, Copyright (C) 2008 Armin Moradi <feng.shaun@gmail.com>
# http://fengshaun.wordpress.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US
############################################################################

import pickle
import wx

class Phonebook:
	def __init__( self ):
		self.frame = wx.Frame( None, -1, title = 'Phone book', size = ( 320, 150 ) )
		self.panel = wx.Panel( self.frame, 1 )

		print 'opening database'
		self.file = open( 'phdb', 'r' )
		self.db = {}

		try:
			print 'loading database'
			self.db = pickle.load( self.file )
		except:
			print 'database is empty'

		print 'loading GUI'
		self.initWidgets()
		print 'loading menues'
		self.initMenus()

		self.frame.Center()
		self.frame.Show()
		print 'everything successfully loaded'

	def initWidgets( self ):
		self.nameLabel = wx.StaticText( self.panel, -1, 'Name:' )
		self.phoneLabel = wx.StaticText( self.panel, -1, 'Phone:' )

		self.addButton = wx.Button( self.panel, 1, 'Add' )
		self.removeButton = wx.Button( self.panel, 2, 'Remove' )
		self.searchButton = wx.Button( self.panel, 3, 'Search' )

		self.nameEntry = wx.TextCtrl( self.panel, -1 )
		self.phoneEntry = wx.TextCtrl( self.panel, -1 )
		
		#orienting them properly using layouts
		self.vbox = wx.BoxSizer( wx.VERTICAL )
		
		self.hbox1 = wx.BoxSizer( wx.HORIZONTAL )
		self.hbox1.Add( self.nameLabel, 0, wx.RIGHT, 10 )
		self.hbox1.Add( self.nameEntry, 1 )

		self.vbox.Add( self.hbox1, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10 )

		self.hbox2 = wx.BoxSizer( wx.HORIZONTAL )
		self.hbox2.Add( self.phoneLabel, 0, wx.RIGHT, 10 )
		self.hbox2.Add( self.phoneEntry, 1 )

		self.vbox.Add( self.hbox2, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10 )

		self.hbox3 = wx.BoxSizer( wx.HORIZONTAL )
		self.hbox3.Add( self.addButton, 1, wx.RIGHT, 10 )
		self.hbox3.Add( self.removeButton, 1, wx.ALIGN_CENTER_HORIZONTAL, 10 )
		self.hbox3.Add( self.searchButton, 1, wx.LEFT, 10 )

		self.vbox.Add( ( -1, 10 ) )
		self.vbox.Add( self.hbox3, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.BOTTOM | wx.ALIGN_BOTTOM, 10 )

		self.panel.SetSizer( self.vbox )

		self.frame.sb = self.frame.CreateStatusBar()

		#Event handling
		self.addButton.Bind( wx.EVT_ENTER_WINDOW, self.enterAdd, id = 1 )
		self.removeButton.Bind( wx.EVT_ENTER_WINDOW, self.enterRemove, id = 2 )
		self.searchButton.Bind( wx.EVT_ENTER_WINDOW, self.enterSearch, id = 3 )
		#self.panel.Bind( wx.EVT_ENTER_WINDOW, self.enterPanel, id = 1 )

		self.frame.Bind( wx.EVT_BUTTON, self.onAdd, id = 1 )
		self.frame.Bind( wx.EVT_BUTTON, self.onRemove, id = 2 )
		self.frame.Bind( wx.EVT_BUTTON, self.onSearch, id = 3 )

		self.frame.Bind( wx.EVT_MENU, self.onAdd, id = 1 )
		self.frame.Bind( wx.EVT_MENU, self.onRemove, id = 2 )
		self.frame.Bind( wx.EVT_MENU, self.onSearch, id = 3 )

		self.frame.Bind( wx.EVT_MENU, self.onAbout, id = 4 )
		
	def initMenus( self ):
		self.menubar = wx.MenuBar()
		
		self.fileMenu = wx.Menu()
		self.addMenuItem = wx.MenuItem( self.fileMenu, 1, '&Add contact', 'Adds a new contact' )
		self.removeMenuItem = wx.MenuItem( self.fileMenu, 2, '&Remove contact', 'Removes an existing contact' )
		self.searchMenuItem = wx.MenuItem( self.fileMenu, 3, '&Search', 'Searches for specified contact' )
		self.fileMenu.AppendItem( self.addMenuItem )
		self.fileMenu.AppendItem( self.removeMenuItem )
		self.fileMenu.AppendItem( self.searchMenuItem )

		self.helpMenu = wx.Menu()
		self.aboutMenuItem = wx.MenuItem( self.helpMenu, 4, 'A&bout this program...', 'Information about this program' )
		self.helpMenu.AppendItem( self.aboutMenuItem )

		self.menubar.Append( self.fileMenu, '&File' )
		self.menubar.Append( self.helpMenu, '&Help' )
		self.frame.SetMenuBar( self.menubar )

	def onAdd( self, event ):
		name = str( self.nameEntry.GetValue() )
		phone = str( self.phoneEntry.GetValue() )

		print 'adding contact \'%s\' %s' % ( name, phone ),

		if name == '':
			print 'FAILED: no name given'
			self.frame.sb.SetStatusText( 'ERROR: no name given' )
		elif phone == '':
			print 'FAILED: no phone given'
			self.frame.sb.SetStatusText( 'ERROR: no phone number given' )
		elif name in self.db:
			print 'FAILED: contact already exists'
			self.frame.sb.SetStatusText( 'ERROR: name already exists' )
		else:
			self.db[ name ] = phone
			print 'successful'
			self.frame.sb.SetStatusText( 'contact \'%s\' : %s added' % ( name, phone ) )
			
	def onRemove( self, event ):
		name = str( self.nameEntry.GetValue() )

		print 'removing contact \'%s\'' % name,
		
		if name in self.db:
			self.db.pop( name )
			print 'successful'
			self.frame.sb.SetStatusText( 'contact \'%s\' removed' % name )
		else:
			print 'FAILED: no such contact'
			self.frame.sb.SetStatusText( 'ERROR: no such contact' )

	def onSearch( self, event ):
		name = str( self.nameEntry.GetValue() )

		print 'searching for contact \'%s\' in database' % name

		if name in self.db:
			print 'contact found:\nname:\t\'%s\'\nphone:\t%s' % ( name, self.db[ name ] )
			wx.MessageBox( 'Contact found:\nname:\t\'%s\'\nphone:\t%s' % ( name, self.db[ name ] ), 'found' )
			self.frame.sb.SetStatusText( '\'%s\':\t\t%s' % ( name, self.db[ name ] ) )
		else:
			print 'no contact found with name \'%s\'' % name
			self.frame.sb.SetStatusText( 'Warning: no such contact' )
			wx.MessageBox( 'No contact found\n\'%s\'' % name, 'not found' )

	def onAbout( self, event ):
		description = '''Phonebook is a simple program for storing and retrieving your friends and family phone numbers.'''

		license = '''Phonebook is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US'''

 		info = wx.AboutDialogInfo()

		info.SetName( 'Phonebook' )
		info.SetVersion( '0.0.1' )
		info.SetDescription( description )
		info.SetCopyright( '(C) 2008 Armin Moradi' )
		info.SetWebSite( 'http://fengshaun.wordpress.com' )
		info.SetLicense( license )
		info.AddDeveloper( 'Armin Moradi' )
		info.AddDocWriter( 'Armin Moradi' )

		wx.AboutBox( info )

	def fileIsEmtpy( self ):
		line = self.file.readline()

		if line == '':
			return True
		else:
			return False

	def save( self ):
		print 'saving state',
		self.file = open( 'phdb', 'w' )
		pickle.dump( self.db, self.file )
		self.file.close()
		print 'successful'

	def enterAdd( self, event ):
		self.frame.sb.SetStatusText( 'Adds a new contact' ) 
		event.Skip()

	def enterRemove( self, event ):
		self.frame.sb.SetStatusText( 'Removes an existing contact' )
		event.Skip()

	def enterSearch( self, event ):
		self.frame.sb.SetStatusText( 'Searches for specified contact' )
		event.Skip()

	def enterPanel( self, event ):
		self.frame.sb.SetStatusText( '' )

if __name__ == '__main__':
	app = wx.App()
	pb = Phonebook()
	app.MainLoop()
	pb.save()
