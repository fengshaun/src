#!/usr/bin/python2

class FbUser:

    API_KEY = ''
    SECRET = ''
    
    def __init__(self, authToken=None):
        self.login(authToken)
        
        self.uid = self.fb.uid
        self.lastMessage = None
        self.friendRequestsCount = None
	
    def login(self, authToken=None):
        settings = QSettings('Armin Moradi', 'Plasbook')

        if authToken is not None:
            self.fb = facebook.Facebook(FbUser.API_KEY, FbUser.SECRET, authToken)
            data = self.fb.auth.getSession()
        
            if data and 'session_key' in data:
                debug('saving settings:')
                debug('\t%s' % data['secret'])
                debug('\t%s' % data['session_key'])
                
                settings.setValue('secret', str(data['secret']))
                settings.setValue('session_key', str(data['session_key']))
                
                if not self.fb.users.hasAppPermission('status_update', uid=self.fb.uid):
                    self.fb.request_extended_permission('status_update', popup=True)
                    self.waitLogin()
            
                if not self.fb.users.hasAppPermission('read_stream', uid=self.fb.uid):
                    self.fb.request_extended_permission('read_stream', popup=True)
                    self.waitLogin()

                if not self.fb.users.hasAppPermission('read_inbox', uid=self.fb.uid):
                    self.fb.request_extended_permission('read_inbox', popup=True)
                    self.waitLogin()
        else:
            self.fb = facebook.Facebook(FbUser.API_KEY, FbUser.SECRET)
            self.fb.session_key = str(settings.value('session_key', '').toString()).strip()
            self.fb.secret = str(settings.value('secret', '').toString()).strip()
            self.fb.uid = self.fb.session_key.split('-')[1]

            if not (self.fb.secret or self.fb.session_key):
                debug('No session_key or secret found...clearing settings')
                settings.clear()
                return None

        self.loggedIn = True

    def unreadMessageCount(self):
        return self.fb.notifications.get()['messages']['unread']

