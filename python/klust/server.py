# Klust server.  Runs a received command from a remote location
# on the local machine

import SocketServer
import time

class KlustHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        self._data = self.rfile.readline().strip()
        print "%s wrote: '%s'" % (self.client_address[0], self._data)

if __name__ == '__main__':
    HOST, PORT = "127.0.0.1", 64001
    server = SocketServer.TCPServer((HOST, PORT), KlustHandler)
    server.serve_forever()
