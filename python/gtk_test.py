#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk

class MainWindow(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

        self.table = gtk.Table(2, 2, True)
        self.add(self.table)

        self.btn1 = gtk.Button('Btn1')
        self.btn2 = gtk.Button('Btn2')
        self.btn3 = gtk.Button('Btn3')

        self.table.attach(self.btn1, 0, 1, 0, 1)
        self.table.attach(self.btn2, 1, 2, 0, 1)
        self.table.attach(self.btn3, 0, 2, 1, 2)

        self.show_all()

def main():
    gtk.main()

if __name__ == '__main__':
    w = MainWindow()
    main()
