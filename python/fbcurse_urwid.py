import os, sys
import urwid
import commands
import textwrap
import facebook

CONFIG_FILE = os.path.join(os.path.expanduser("~"), ".fbcurse")
API_KEY     = "19a90d1cf69eaab57d82d54f3bcaba44"
SECRET      = "5e55410f8380f1e6e2a1d876c40be37a"

# global variables used later on.
fb = None
content = None
frame = None

def configToken():
    with open(CONFIG_FILE, "r") as cf:
        for line in cf.readlines():
            line = line.strip().split("=")
            if line[0] == "token":
                return line[1]
    return ""

def saveCredentials():
    with open(CONFIG_FILE, "a") as cf:
        cf.write("session_key=%s\n" % fb.session_key)
        cf.write("secret=%s\n" % fb.secret)

def login():
    global fb

    print "logging in"
    d = {"session_key": "", "secret": ""}
    with open(CONFIG_FILE, "r") as cf:
        for line in cf.readlines():
            line = line.strip().split("=")
            if line[0] == "session_key":
                d["session_key"] = line[1]
                print "session_key found: %s" % d["session_key"]
            elif line[0] == "secret":
                d["secret"] = line[1]
                print "secret found: %s" % d["secret"]
    
    if not (d["session_key"] or d["secret"]):
        print "session_key/secret not found, looking for token"
        token = configToken()
        
        if not token:
            print "token not found"
            print ("Please go to http://www.facebook.com/code_gen.php?v=1.0&api_key=19a90d1cf69eaab57d82d54f3bcaba44\n" +
                   "and click 'Generate' to create a login token.  Then copy that token in ~/.fbcurse like so:\n" +
                   "token=TOKEN\n" +
                   "where TOKEN is the token you got from facebook.  Then rerun this application.")
            return False

        print "token: %s" % token
        fb = facebook.Facebook(API_KEY, SECRET, token)
        fb.auth.getSession()
        saveCredentials()

        if not fb.users.hasAppPermission(ext_perm='read_stream', uid=fb.uid):
            print ("Please go to http://www.facebook.com/connect/prompt_permissions.php?v=1.0&api_key=19a90d1cf69eaab57d82d54f3bcaba44&ext_perm=read_stream&extern=1\n" +
                   "and click 'Allow' to give this application the required permission.")
            return False

        return fb

    else:
        fb = facebook.Facebook(API_KEY, SECRET)
        fb.session_key = d["session_key"]
        fb.secret = d["secret"]
        fb.uid = d["session_key"].split("-")[1]
        return fb

def posts():
    stream = fb.stream.get(limit=10)
    p = []
    for post in stream["posts"]:
        for pro in stream["profiles"]:
            if post["actor_id"] == pro["id"]:
                d = {}
                # find the name of the poster
                d["name"] = pro["name"]

                # is it a wall-to-wall post?
                if "target_id" in post:
                    for pro in stream["profiles"]:
                        if post["target_id"] == pro["id"]:
                            d["target"] = pro["name"]

                d["message"] = post["message"] or "(no messages attached)"

                # if "attribution", it's from an app; if "attachment", it has some sort of cool link
                if post["attribution"]:
                    d["app"] = post["attribution"]
                elif "attachment" in post and "media" in post["attachment"]:
                    d["media"] = []
                    for m in post["attachment"]["media"]:
                        d["media"].append(m)

                d["comments"] = post["comments"]["count"] if "count" in post["comments"] else "0"
                d["likes"] = post["likes"]["count"] if "count" in post["likes"] else "0"

                p.append(d)

    return p

def header():
    return "FbCurse -- %s: (%s) messages" % (fb.users.getInfo([fb.uid], ["name"])[0]["name"],
                                                 fb.notifications.get()["messages"]["unread"])

def update():
    global frame, content

    c = []
    for post in posts():
        message = post["name"] if len(post["name"]) < 15 else post["name"][:15]
        while len(message) < 15: message += " "
        message += "| "
        message += post["message"]
        c.extend([urwid.AttrMap(urwid.Text(message), None, "selected"), urwid.Divider("-")])

    c.insert(0, urwid.Text(""))
    c = c[:-1]

    content = urwid.SimpleListWalker(c)
    listbox = urwid.ListBox(content)
    h = urwid.AttrMap(urwid.Text(header()), "header")

    frame = urwid.Frame(listbox, h)

    content.set_focus(1)

def process_input(input, raw):
    if "u" in input:
        update()

    elif "j" in input:
        content.set_focus(content.get_focus()[1] + 2)

    elif "k" in input:
        if content.get_focus()[1] > 1:
            content.set_focus(content.get_focus()[1] - 2)
        else:
            content.set_focus(1)

    elif "h" in input:
        return ["left"]

    elif "l" in input:
        return ["right"]

    elif "q" in input:
        raise urwid.ExitMainLoop()

    return input

if __name__ == "__main__":
    palette = [("header", "white", "black"),
               ("selected", "bold", ""),]

    fb = login()
    if not fb:
        raise urwid.ExitMainLoop()

    update()

    urwid.MainLoop(frame, palette, input_filter=process_input).run()
